package com.openfeint.example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.ui.Dashboard;

import android.app.Application;

public class OpenFeintApplication extends Application {
	static final String gameName = "loader-android";
	static final String gameID = "491454";
	static final String gameKey = "9dH7p1AORwVqX3oi6YJIg";
	static final String gameSecret = "5Dhqj2hlgoXbBd2pdwDdBK635dod5QqjWDuyuSwttA"; 
	
	public static List<Achievement> achievements = null;
	public static List<Leaderboard> leaderboards = null;
	
    @Override
    public void onCreate() {
        super.onCreate();

        Map<String, Object> options = new HashMap<String, Object>();
        options.put(OpenFeintSettings.SettingCloudStorageCompressionStrategy, OpenFeintSettings.CloudStorageCompressionStrategyDefault);
        // use the below line to set orientation
        // options.put(OpenFeintSettings.RequestedOrientation, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        OpenFeintSettings settings = new OpenFeintSettings( gameName, gameKey, gameSecret, gameID, options);
        
        OpenFeint.initialize(this, settings, new OpenFeintDelegate() { });
        
        Achievement.list(new Achievement.ListCB() {
			@Override public void onSuccess(List<Achievement> _achievements) {
				achievements = _achievements;
			}
		});
        
        Leaderboard.list(new Leaderboard.ListCB() {
			@Override public void onSuccess(List<Leaderboard> _leaderboards) {
				leaderboards = _leaderboards;
			}
		});
        Dashboard.open();
    }

}
