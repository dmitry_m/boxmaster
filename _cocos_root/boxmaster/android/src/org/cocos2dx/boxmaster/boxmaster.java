/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.boxmaster;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.Object;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxEditText;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.ui.Dashboard;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.resource.User;

import android.util.Log;
import android.widget.Toast;
import android.widget.LinearLayout;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import android.os.Bundle;

public class boxmaster extends Cocos2dxActivity{
	private static boolean FREE = false;
	private Cocos2dxGLSurfaceView mGLView;
  	private AdView adView;

  	// ������� �� C++
    private static native void setIsRecord();
    private static native void setUserLoggedIn( boolean logged );
    private static native void writeScoresFromOF( int scores, int complexity );
    
    public static List<Achievement> achievements = null;
  	public static List<Leaderboard> leaderboards = null;
  	
  	private static final String gameName = "loader-android";
  	private static final String gameID = "491454";
  	private static final String gameKey = "9dH7p1AORwVqX3oi6YJIg";
  	private static final String gameSecret = "5Dhqj2hlgoXbBd2pdwDdBK635dod5QqjWDuyuSwttA";
	
	// leaderboards
  	private static final String hardLeaderboardID   = "1158427";
  	private static final String mediumLeaderboardID = "1158437";
  	private static final String easyLeaderboardID   = "1158447";
	private static final String AD_UNIT_ID = "a14f9dcff6a21be";
	private static final String TEST_DEVICE = "001919f91bd14e";
		
	private static void showLeaderboard() {
		Dashboard.open();
	}
		
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_demo);
		String packageName = getApplication().getPackageName();
		super.setPackageName(packageName);

		//---------------------------------------------------------------
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.linearLayout);
	    
		if( FREE ) {
			// Admob
			AdView adView = new AdView(this, AdSize.BANNER, AD_UNIT_ID);
		    layout.addView(adView);
		    AdRequest adRequest = new AdRequest();    
			//adRequest.addTestDevice(TEST_DEVICE);
		    adView.loadAd(adRequest);
		}
		else {
			// Openfeint
			Map<String, Object> options = new HashMap<String, Object>();
	        options.put(OpenFeintSettings.SettingCloudStorageCompressionStrategy, OpenFeintSettings.CloudStorageCompressionStrategyDefault);
	        OpenFeintSettings settings = new OpenFeintSettings( gameName, gameKey, gameSecret, gameID, options);
			OpenFeint.initialize(this, settings, new OpenFeintDelegate() { });	 
		}
	    //---------------------------------------------------------------	    
		
        mGLView = (Cocos2dxGLSurfaceView) findViewById(R.id.game_gl_surfaceview);
        mGLView.setTextField((Cocos2dxEditText)findViewById(R.id.textField));
	}
	   
	// OPENFEINT-----------------------------------------------------------------------------------
	// ������� ��� C++
	private static void userLoggedIn() {
		if( !FREE ) {
			setUserLoggedIn( OpenFeint.isUserLoggedIn() ); 
		}		
	}
	
	private static void getScoresForPlayer() {
		if( !FREE && OpenFeint.isUserLoggedIn() ) {
			// ������			
			Leaderboard easy = new Leaderboard( easyLeaderboardID );
			easy.getUserScore( OpenFeint.getCurrentUser(), new Leaderboard.GetUserScoreCB() {				
				@Override public void onSuccess(Score score) {
					if( score != null ) {
						Log.w( "LEADERBOARD", score.toString());
						writeScoresFromOF( (int)score.score, 0 );
					}
					else { 
						writeScoresFromOF( -1, 0 );
					}
				}
			}); 
					
			// �������
			Leaderboard medium = new Leaderboard( mediumLeaderboardID );
			medium.getUserScore( OpenFeint.getCurrentUser(), new Leaderboard.GetUserScoreCB() {			
				@Override public void onSuccess(Score score) {
					if( score != null ) {
						Log.w( "LEADERBOARD", score.toString());
						writeScoresFromOF( (int)score.score, 1 );
					}
					else { 
						writeScoresFromOF( -1, 1 );
					}
				}
			}); 
			// �������
			Leaderboard hard = new Leaderboard( hardLeaderboardID );
			hard.getUserScore( OpenFeint.getCurrentUser(), new Leaderboard.GetUserScoreCB() {			
				@Override public void onSuccess(Score score) {
					if( score != null ) {
						Log.w( "LEADERBOARD", score.toString());
						writeScoresFromOF( (int)score.score, 2 );
					}
					else {
						writeScoresFromOF( -1, 2 );
					}
				}
			});  			
		}
	}
	
	private static void submitScores( int scores, int complexity ) {
		if( !FREE ) {
			String s = String.valueOf(scores);

			String leaderboardID = easyLeaderboardID;

			if( complexity == 1 ) {
				leaderboardID = mediumLeaderboardID;
			}
			else if( complexity == 2 ) {
				leaderboardID = hardLeaderboardID;
			}
			
			Score score = new Score( scores, null ); 
			Leaderboard main = new Leaderboard( leaderboardID );
			score.submitTo(main, new Score.SubmitToCB() {
			  @Override public void onSuccess(boolean newHighScore) { 
			  }
			  @Override public void onFailure(String exceptionMessage) {
			  }
			}); 
		}
	}
	
	// --------------------------------------------------------------------------------------------
	
	 @Override
	 protected void onPause() {
	     super.onPause();
	     mGLView.onPause();
	 }

	 @Override
	 protected void onResume() {
	     super.onResume();
	     mGLView.onResume();
	 }
	 
	 @Override
	 public void onDestroy() {
	     adView.destroy();
	     //super.onDestroy();
	 }

	
     static {
    	 System.loadLibrary("game");
     }
}