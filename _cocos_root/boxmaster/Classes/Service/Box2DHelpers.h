#ifndef BOX2DHELPERS_H
#define BOX2DHELPERS_H

#include "Box2D.h"
#include "Constants\CommonProtocols.h"
#include "GameObjects\Box2D\Box2DSprite.h"

bool isBodyCollidingWithObjectType( b2Body *body, GameObjectType objectType );

#endif