#ifndef FILE_OPERATION_H
#define FILE_OPERATION_H

#include <string>
#include "Configuration\ConfigurationAdapter.h"

class ConfigFile {
	static std::string getFilePath();

public:
	static void writeConfiguration( Configuration cfg );
	static Configuration readConfiguration();
};

#endif
