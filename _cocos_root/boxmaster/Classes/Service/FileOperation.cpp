#include "cocos2d.h"
#include "Constants\Constants.h"
#include <stdio.h>
#include "Service\FileOperation.h"

using namespace std;

void ConfigFile::writeConfiguration( Configuration cfg ) {
	string path = getFilePath();
	FILE *fp = fopen(path.c_str(), "w");

	if (! fp) {
		CCLOG("can not create file %s", path.c_str());
		return;
	}

	fwrite( &cfg, sizeof cfg, 1, fp );
	fclose(fp);
}

Configuration ConfigFile::readConfiguration() {
	Configuration cfg;
	cfg.maxScoresEasy = cfg.maxScoresHard = cfg.maxScoresMedium = -1;
	cfg.music = true;
	cfg.sound = true;
	cfg.orientation = 0;

	string path = getFilePath();
	FILE *fp = fopen(path.c_str(), "r");
	
	if (! fp) {
		CCLOG("can not open file %s", path.c_str());
		return cfg;
	}

	fread( &cfg, sizeof cfg, 1, fp );
	fclose(fp);
	return cfg;
}

string ConfigFile::getFilePath() {
	string path("");
#if IS_ANDROID
	path.append("/data/data/org.cocos2dx.boxmaster/");
#endif
#if	IS_WIN32
	path.append("C:/");
#endif
	path.append( CONFIG_FILENAME );
	return path;
}