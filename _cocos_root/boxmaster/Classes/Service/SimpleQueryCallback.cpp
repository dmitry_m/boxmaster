#include "SimpleQueryCallback.h"
#include "GameObjects\Box2D\Box.h"
#include "GameObjects\Box2D\Loader.h"


SimpleQueryCallback::SimpleQueryCallback( const b2Vec2 &point )
{
	pointToTest = point;
	fixtureFound = NULL;
}

bool SimpleQueryCallback::ReportFixture( b2Fixture *fixture )
{
	switch( objectToFind ) {
	case kLoaderType:
		return loaderSearch( fixture );

	case kBoxType:
	default:
		return boxSearch( fixture );
	}
}

bool SimpleQueryCallback::boxSearch( b2Fixture *fixture ) {
	b2Body* body = fixture->GetBody();
		Box2DSprite *sprite = (Box2DSprite *) body->GetUserData();
		if( sprite->getGameObjectType() == kBoxType ) {
			Box* box = (Box*)sprite;
			if( !box->getBorder() && fixture->TestPoint( pointToTest ) ) {
					fixtureFound = fixture;
					return false;
			}
		}
		return true;
}

bool SimpleQueryCallback::loaderSearch( b2Fixture *fixture ) {
	b2Body* body = fixture->GetBody();
		Box2DSprite *sprite = (Box2DSprite *) body->GetUserData();
		if( sprite->getGameObjectType() == kLoaderType ) {
			Loader* pLoader = (Loader*)sprite;
			if( fixture->TestPoint( pointToTest ) ) {
					fixtureFound = fixture;
					return false;
			}
		}
		return true;
}

