#ifndef PUSH_HANDLER_H_
#define PUSH_HANDLER_H_

#include "AbstractHandler.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"
#include "GameObjects\Box2D\Loader.h"

class Scene1ActionLayer; 

class PushHandler : public AbstractHandler {
public:
	PushHandler(Scene1ActionLayer * layer, DirectionTypes direction) 
		: m_layer(layer),
		  m_direction(direction) 
	{ }

	virtual void initHandler() {
		Coordinates endCoordinates;
		m_startPosition = endCoordinates = P2C(m_layer->getLoader()->getPosition());		
		if (m_direction == kLeft) {
			endCoordinates.x--;
		}
		else if (m_direction = kRight) {
			endCoordinates.x++;
		}
		m_endPosition = C2P(endCoordinates);
	}

	virtual bool updateWithDeltaTime(float deltaTime) {
		Loader * hero = m_layer->getLoader();
		if ( (m_direction == kLeft && !hero->getLeftIsEmpty() && m_endPosition.x < m_layer->getLoader()->getPosition().x) 
		  || (m_direction == kRight && !hero->getRightIsEmpty() &&  m_endPosition.x > m_layer->getLoader()->getPosition().x) ) 
		{
			hero->setCurrentDirection(m_direction);
			return false;
		}
		return true;
	}

private:
	Scene1ActionLayer *		m_layer;
	DirectionTypes			m_direction;
	Coordinates				m_startPosition;
	cocos2d::CCPoint		m_endPosition;
};

#endif