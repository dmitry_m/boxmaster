#ifndef JUMP_HANDLER_H_
#define JUMP_HANDLER_H_

#include "AbstractHandler.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"
#include "GameObjects\Box2D\Loader.h"

class Scene1ActionLayer; 

class JumpHandler : public AbstractHandler {
public:
	JumpHandler(Scene1ActionLayer * layer, DirectionTypes direction) 
		: m_layer(layer),
		  m_direction(direction),
		  m_jumped(false),
		  m_timeout(0)
	{ }

	virtual void initHandler() {
		Coordinates endCoordinates;
		m_startPosition = endCoordinates = P2C(m_layer->getLoader()->getPosition());		
		if (m_direction == kLeft) {
			endCoordinates.x--;
		}
		else if (m_direction = kRight) {
			endCoordinates.x++;
		}
		m_endPosition = C2P(endCoordinates);
	}

	virtual bool updateWithDeltaTime(float deltaTime) {
		if (!m_jumped) {
			m_layer->getLoader()->setJumpQuery(true);
			m_jumped = true;
		}
		else {
			m_timeout += deltaTime;
			if (m_timeout > 0.7) {
				Loader * hero = m_layer->getLoader();
				if ( !(m_direction == kLeft && hero->getLeftIsEmpty() && m_endPosition.x < m_layer->getLoader()->getPosition().x) 
				  && !(m_direction == kRight && hero->getRightIsEmpty() &&  m_endPosition.x > m_layer->getLoader()->getPosition().x) ) 
				{
					return true;
				}
				hero->setCurrentDirection(m_direction);
			}
		}
		return false;
	}

private:
	float					m_timeout;
	bool					m_jumped;
	Scene1ActionLayer *		m_layer;
	DirectionTypes			m_direction;
	Coordinates				m_startPosition;
	cocos2d::CCPoint		m_endPosition;
};

#endif