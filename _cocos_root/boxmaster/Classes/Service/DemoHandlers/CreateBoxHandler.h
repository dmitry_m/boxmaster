#ifndef CREATE_BOX_HANDLER_H_
#define CREATE_BOX_HANDLER_H_

#include "AbstractHandler.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"
#include "GameObjects\Box2D\Loader.h"

class Scene1ActionLayer; 

class CreateBoxHandler : public AbstractHandler {
public:
	CreateBoxHandler(Scene1ActionLayer * layer, int offset) 
		: m_layer(layer),
		  m_offset(offset) 
	{ }

	virtual void initHandler() {		
	}

	virtual bool updateWithDeltaTime(float deltaTime) {
		m_layer->createBoxAtPlaceWithCoordinatesWithIndicator(m_offset, 0);
		return true;
	}

private:
	Scene1ActionLayer *		m_layer;
	int						m_offset;
	Coordinates				m_startPosition;
	cocos2d::CCPoint		m_endPosition;
};

#endif