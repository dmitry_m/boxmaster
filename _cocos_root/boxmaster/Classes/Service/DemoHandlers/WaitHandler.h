#ifndef WAIT_HANDLER_H_
#define WAIT_HANDLER_H_

#include "AbstractHandler.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"

class Scene1ActionLayer; 

class WaitHandler : public AbstractHandler {
public:
	WaitHandler(Scene1ActionLayer * layer, float timeout) 
		: m_layer(layer),
		  m_timeout(timeout) 
	{ }

	virtual void initHandler() {}

	virtual bool updateWithDeltaTime(float deltaTime) {
		m_timeout -= deltaTime;
		return (m_timeout <= 0.0f);
	}

private:
	Scene1ActionLayer * m_layer;
	float m_timeout;
};

#endif