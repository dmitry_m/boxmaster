#ifndef ABSTRACT_HANDLER_H_
#define ABSTRACT_HANDLER_H_

class AbstractHandler {
public:
	virtual void initHandler() = 0;

	// returns true if handler shall be removed
	virtual bool updateWithDeltaTime(float deltaTime) = 0;
};

#endif