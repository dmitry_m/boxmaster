#ifndef SIMPLEQUERYCALLBACK_H
#define SIMPLEQUERYCALLBACK_H

#include "Constants/constants.h"
#include "Box2D.h"

class SimpleQueryCallback : public b2QueryCallback {
	CC_SYNTHESIZE( GameObjectType, objectToFind, ObjectToFind );
	bool boxSearch( b2Fixture *fixture );
	bool loaderSearch( b2Fixture *fixture );
public:
	b2Vec2 pointToTest;
	b2Fixture *fixtureFound;

	SimpleQueryCallback( const b2Vec2 &point );
	virtual bool ReportFixture( b2Fixture *fixture );

};

#endif

