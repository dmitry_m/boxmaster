#include "ServiceInfo.h"




// real-field functions ----------------------------------------------------

CCPoint field2real( CCPoint oldPos ) {
	return ccp( oldPos.x + kLeftBorder, oldPos.y + kMarginBottom ); 		
}

CCPoint real2field( CCPoint oldPos ) {	
	CCPoint retVal;
	if( oldPos.x < kLeftBorder ) {
		retVal.x = kLeftBorder;
	} else {
		retVal.x = oldPos.x - kLeftBorder;
	}

	if( oldPos.y < kMarginBottom ) {
		retVal.y = kMarginBottom;
	} else {
		retVal.y = oldPos.y - kMarginBottom;
	}
	return retVal;
}

// coordinates-position functions ------------------------------------------

CCPoint C2P( Coordinates coordinates ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	CCPoint newPos = field2real( ccp(kBoxSide * coordinates.x +kBoxSide / 2,kBoxSide * coordinates.y +kBoxSide / 2 ) );
	newPos.x += ( int )coordinates.x * kBoxSeparator;
	//newPos.y += y * 2;
	
	return newPos;
}

CCPoint C2P( int x, int y ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	CCPoint newPos = field2real( ccp(kBoxSide * x +kBoxSide / 2,kBoxSide * y +kBoxSide / 2 ) );
	newPos.x += x * kBoxSeparator;
	//newPos.y += y * 2;
	
	return newPos;
}

Coordinates P2C( CCPoint position ) {
	CCPoint pos = real2field( position );
	Coordinates coordinates;
	coordinates.x = ( int )( pos.x / (kBoxSide + kBoxSeparator ) );
	coordinates.y = ( int )( pos.y /kBoxSide );
	return coordinates;
}

Coordinates P2C( float x, float y ) {
	CCPoint pos = real2field( ccp( x, y ) );
	Coordinates coordinates;
	coordinates.x = ( int )( pos.x / (kBoxSide + kBoxSeparator ) );
	coordinates.y = ( int )( pos.y /kBoxSide );
	return coordinates;
}

// vector-position functions -----------------------------------------------

b2Vec2 P2V( CCPoint point ) {
	return b2Vec2( point.x / PTM_RATIO, point.y / PTM_RATIO );
}

b2Vec2 P2V( float x, float y ) {
	return b2Vec2( x / PTM_RATIO, y / PTM_RATIO );
}

CCPoint V2P( b2Vec2 vector ) {
	return CCPoint( vector.x * PTM_RATIO, vector.y * PTM_RATIO );
}

CCPoint V2P( float x, float y ) {
	return CCPoint( x * PTM_RATIO, y * PTM_RATIO );
}

// vector-coordinates functions --------------------------------------------

b2Vec2 C2V( Coordinates coordinates ) {	
	return P2V( C2P( coordinates ) );
}

b2Vec2 C2V( int x, int y ) {
	return P2V( C2P( x, y ) );
}

Coordinates V2C( b2Vec2 position ) {
	return P2C( V2P( position ) );
}

Coordinates V2C( float x, float y ) {
	return P2C( V2P( x, y) );
}




