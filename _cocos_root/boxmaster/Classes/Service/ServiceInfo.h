#ifndef SERVICEINFO_H
#define SERVICEINFO_H

#include "GameObjects\Box2D\Box.h"

class Coordinates
{
public:
	int x;
	int y;
	Coordinates()
		:x( 0 ), y( 0 ) {}
	Coordinates( int X, int Y )	
		:x( X ), y( Y )  {}
};

// falling info object contains object, and destination in coordinates
class FallingInfo
{
	CC_SYNTHESIZE( Box*, object, Object );
	CC_SYNTHESIZE( int, x, X );
	CC_SYNTHESIZE( int, y, Y );
public:
	FallingInfo( Box* object, int posX, int posY )	
		:object( object ), x( posX ), y( posY )  {}
};

// position transforming

CCPoint field2real( CCPoint oldPos );
CCPoint real2field( CCPoint oldPos );

// coordinates-position functions ------------------------------------------
CCPoint C2P( Coordinates coordinates );
CCPoint C2P( int x, int y );
Coordinates P2C( CCPoint position );
Coordinates P2C( float x, float y );

// vector-position functions -----------------------------------------------
b2Vec2 P2V( CCPoint point );
b2Vec2 P2V( float x, float y );
CCPoint V2P( b2Vec2 vector );
CCPoint V2P( float x, float y );

// vector-coordinates functions --------------------------------------------
b2Vec2 C2V( Coordinates coordinates );
b2Vec2 C2V( int x, int y );
Coordinates V2C( b2Vec2 position );
Coordinates V2C( float x, float y );

#endif