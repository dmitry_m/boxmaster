#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "Singletons\GameManager.h"

#include "cocos2d.h"
#include "platform/android/jni/JniHelper.h" 
#include <jni.h>

#if ( FREE == 1 )	
	#define CLASS_OPEN_NAME "org/cocos2dx/boxmasterfree/boxmaster" 
#else
	#define CLASS_OPEN_NAME "org/cocos2dx/boxmaster/boxmaster"
#endif

#ifndef RUNJAVA_H
#define RUNJAVA_H

extern "C" {
	// ��� ������� �������� Java-���
#if ( FREE == 1 )
	void Java_org_cocos2dx_boxmasterfree_boxmaster_setIsRecord( JNIEnv* env, jobject thiz );
	void Java_org_cocos2dx_boxmasterfree_boxmaster_setUserLoggedIn( JNIEnv* env, jobject thiz, jboolean logged );
	void Java_org_cocos2dx_boxmasterfree_boxmaster_writeScoresFromOF( JNIEnv* env, jobject thiz, jint scores, jint complexity );
#else
	void Java_org_cocos2dx_boxmaster_boxmaster_setIsRecord( JNIEnv* env, jobject thiz );
	void Java_org_cocos2dx_boxmaster_boxmaster_setUserLoggedIn( JNIEnv* env, jobject thiz, jboolean logged );
	void Java_org_cocos2dx_boxmaster_boxmaster_writeScoresFromOF( JNIEnv* env, jobject thiz, jint scores, jint complexity );
#endif
	
	// �������, ���������� ������� � Java
	class RunJava {
	public:
		static void openDashBoard();
		static void userLoggedIn();
		static void getScores();
		static void submitScores( int scores, int complexity );
	};
} 

#endif

#endif // CC_PLATFORM_ANDROID

