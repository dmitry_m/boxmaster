#ifndef EXITLAYER_H
#define EXITLAYER_H

#include "Constants\Constants.h"
#include "Singletons\GameManager.h"

class ExitLayer : public cocos2d::CCLayer
{
protected:
	CCLabelBMFont* levelLabelText;

	CCMenu *returnButtonMenu;	
	CCMenu *exitButtonMenu;
public:
	LAYER_NODE_FUNC( ExitLayer );
	void returnToMainMenu( CCObject* pSender );
	void exitGame( CCObject* pSender );
	void keyBackClicked();

	void displayMenu( CCObject* pSender );
	bool init();
};

#endif