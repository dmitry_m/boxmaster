#include "ExitScene.h"

bool ExitScene::init()
{
	bool pRet = false;
	if ( CCScene::init() )
	{
		ExitLayer *myLayer = ExitLayer::node();
		this->addChild( myLayer );
		pRet = true;
	}

	return pRet;
}
