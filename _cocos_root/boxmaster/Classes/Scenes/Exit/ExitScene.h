#ifndef EXITSCENE_H
#define EXITSCENE_H


#include "ExitLayer.h"

using namespace std;

class ExitScene : public CCScene
{
public:
	SCENE_NODE_FUNC( ExitScene );
	bool init();
};

#endif