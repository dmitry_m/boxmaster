#include "ExitLayer.h"

using namespace cocos2d;

bool ExitLayer::init() {
	bool pRet = false;
	if ( CCLayer::init() )
	{
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

		this->setIsTouchEnabled( false );
		this->setIsKeypadEnabled( true );

		CCSprite *background = CCSprite::spriteWithFile( IM_MENU_BACK );
		background->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background );

		// for fps visible
#if NEED_DF
			CCSprite* fpsDisplay = CCSprite::spriteWithFile( IM_SKY );
			fpsDisplay->setPosition( ccp( fpsDisplay->boundingBox().size.width * FPS_COEFF, fpsDisplay->boundingBox().size.height * FPS_COEFF) );
			this->addChild(fpsDisplay, 200 );
#endif

		levelLabelText = CCLabelBMFont::labelWithString( LABEL_EXIT, kBigFont );
		levelLabelText->setPosition( ccp( screenSize.width * 0.5, screenSize.height * 0.65f ) );
		levelLabelText->setOpacity( 0 );	


		this->addChild( levelLabelText );
		
		CCFiniteTimeAction *levelLabelTextFadeIn = CCSequence::actions( 
			CCFadeIn::actionWithDuration( FADE_TIME ), 
			NULL );

		levelLabelText->runAction( levelLabelTextFadeIn );

		displayMenu( NULL );

		pRet = true;
	}

	return pRet;
}

void ExitLayer::returnToMainMenu( CCObject* pSender ) {
	GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );
} 

void ExitLayer::exitGame( CCObject* pSender ) {
	CCDirector::sharedDirector()->end();
} 


void ExitLayer::displayMenu( CCObject* pSender ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	// return to main menu button	
	CCMenuItemImage *returnButton = CCMenuItemImage::itemFromNormalImage( 
		IM_NO,
		IM_NO_PR,
		NULL,
		this,
		menu_selector( ExitLayer::returnToMainMenu ) );

	returnButtonMenu = CCMenu::menuWithItems( returnButton, NULL );
	returnButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	returnButtonMenu->setPosition( ccp( screenSize.width * ( MENU_N_BUTTON_X_COEFFICIENT + 0.05f ) , MENU_Y_N_BUTTON_Y_COEFFICIENT * screenSize.height ) );
	returnButtonMenu->setOpacity( 0 );
	CCAction* returnButtonAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_N_BUTTON_X_COEFFICIENT, MENU_Y_N_BUTTON_Y_COEFFICIENT * screenSize.height ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	returnButtonMenu->runAction( returnButtonAction );
	this->addChild( returnButtonMenu, 0, kMainMenuTagValue );

	// exit level button	
	CCMenuItemImage *exitButton = CCMenuItemImage::itemFromNormalImage( 
		IM_YES,
		IM_YES_PR,
		NULL,
		this,
		menu_selector( ExitLayer::exitGame ) );

	exitButtonMenu = CCMenu::menuWithItems( exitButton, NULL );
	exitButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	exitButtonMenu->setPosition( ccp( screenSize.width * ( MENU_Y_BUTTON_X_COEFFICIENT - 0.05f ) , MENU_Y_N_BUTTON_Y_COEFFICIENT * screenSize.height ) );
	exitButtonMenu->setOpacity( 0 );
	CCAction* exitButtonAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_Y_BUTTON_X_COEFFICIENT, MENU_Y_N_BUTTON_Y_COEFFICIENT * screenSize.height ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	exitButtonMenu->runAction( exitButtonAction );
	this->addChild( exitButtonMenu, 0, kMainMenuTagValue );
}

void ExitLayer::keyBackClicked() {
	this->returnToMainMenu( NULL );		
}