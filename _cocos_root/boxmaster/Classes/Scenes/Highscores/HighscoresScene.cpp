#include "HighscoresScene.h"

bool HighscoresScene::init() {
	bool pRet = 0;
	if ( CCScene::init() ) {
		HighscoresLayer* myHighscoresLayer = HighscoresLayer::node();
		this->addChild( myHighscoresLayer );
		pRet = 1;
	}
	return pRet;
}

