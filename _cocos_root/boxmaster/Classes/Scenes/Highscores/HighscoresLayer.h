#ifndef HIGHSCORESLAYER_H
#define HIGHSCORESLAYER_H
#include "Singletons\GameManager.h"

using namespace cocos2d;

class HighscoresLayer : public CCLayer {
	CCMenu *returnButtonMenu;
	bool disableInput;

public:
	CCLabelBMFont *label;
	LAYER_NODE_FUNC( HighscoresLayer );
	
	void returnToMainMenu( CCObject* pSender );
	void displayMenu( CCObject* pSender );
	bool init();
	void keyBackClicked();
	void initBackgroundAndLabels();
	void getCost( char* buf, int scores );
};

#endif
