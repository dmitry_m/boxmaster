#include "HighscoresLayer.h"

bool HighscoresLayer::init() 
{
	bool pRet = 0;
	if ( CCLayer::init() ) 
	{
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize(); 
		disableInput = false;
		this->setIsTouchEnabled( false );
		this->setIsKeypadEnabled( true );		

		initBackgroundAndLabels();		

		pRet = 1;
	}
	return pRet;
}

void HighscoresLayer::returnToMainMenu( CCObject* pSender ) {
	if( !disableInput) {
		GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );
		disableInput = true;
	}
} 


void HighscoresLayer::displayMenu( CCObject* pSender ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	// return to main menu button	
	CCMenuItemImage *returnButton = CCMenuItemImage::itemFromNormalImage( 
		IM_RETURN,
		IM_RETURN_PR,
		NULL,
		this,
		menu_selector( HighscoresLayer::returnToMainMenu ) );

	returnButtonMenu = CCMenu::menuWithItems( returnButton, NULL );
	returnButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	returnButtonMenu->setPosition( ccp( 0, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT) );
	returnButtonMenu->setOpacity( 0 );

	CCAction* returnButtonAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_LEFT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	returnButtonMenu->runAction( returnButtonAction );
	this->addChild( returnButtonMenu, 5, kMainMenuTagValue );
}

void HighscoresLayer::keyBackClicked() {
	this->returnToMainMenu( NULL );		
}

void HighscoresLayer::initBackgroundAndLabels() {	
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

		char buf[128];
		float scale = 0.8;
		int scores;

		
		// ���
		CCSprite *background = CCSprite::spriteWithFile( IM_PAPER );
		background->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background, 1 );

		CCSprite *background_paper = CCSprite::spriteWithFile( IM_PAPER );
		background_paper->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background_paper, 0 );
		
		// ������ "���������"
		CCLabelBMFont* scoreLabel = CCLabelBMFont::labelWithString( LABEL_SCORES, kFont );
		scoreLabel->setPosition( ccp( screenSize.width * 0.65f, screenSize.height * 0.85f ) );
		this->addChild( scoreLabel, 10 );
		scoreLabel->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.65f, screenSize.height * 0.90f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL ) );
		CCLabelBMFont* levelLabel = CCLabelBMFont::labelWithString( LABEL_LEVEL, kFont );
		levelLabel->setPosition( ccp( screenSize.width * 0.25f, screenSize.height * 0.85f ) );
		this->addChild( levelLabel, 10 );
		levelLabel->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.25f, screenSize.height * 0.90f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL ) );
		
		// ������ "�������"
		CCLabelBMFont* hardTextLabel = CCLabelBMFont::labelWithString( LABEL_HARD, kFont );
		hardTextLabel->setPosition( ccp( screenSize.width * 0.25f, screenSize.height * 0.65f ) );
		this->addChild( hardTextLabel, 10 );
		hardTextLabel->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.25f, screenSize.height * 0.70f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL ) );
		
		scores = _CONFIGMAN->Conf.getScoresHard();
		sprintf( buf, "%d", scores >= 0 ? scores : 0 );
		CCLabelBMFont* hardScoresLabel = CCLabelBMFont::labelWithString( buf, kFont );
		hardScoresLabel->setPosition( ccp( screenSize.width * 0.65f, screenSize.height * 0.65f ) );
		this->addChild( hardScoresLabel, 10 );
		hardScoresLabel->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.65f, screenSize.height * 0.70f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL ) );
		
		getCost( buf, scores );
		CCSprite *hardMedal = CCSprite::spriteWithFile( buf );
		hardMedal->setPosition( ccp( screenSize.width * 0.92f, screenSize.height * 0.65f ) );
		this->addChild( hardMedal, 10 );
		hardMedal->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.92f, screenSize.height * 0.70f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL ) );
		
		// ������ "�������"
		CCLabelBMFont* mediumTextLabel = CCLabelBMFont::labelWithString( LABEL_MEDIUM, kFont );
		mediumTextLabel->setPosition( ccp( screenSize.width * 0.25f, screenSize.height * 0.50f ) );
		this->addChild( mediumTextLabel, 10 );
		mediumTextLabel->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.25f, screenSize.height * 0.55f ) ),
				CCFadeIn::actionWithDuration( FADE_TIME ),
				NULL ) );
		
		scores = _CONFIGMAN->Conf.getScoresMedium();
		sprintf( buf, "%d", scores >= 0 ? scores : 0 );
		CCLabelBMFont* mediumScoresLabel = CCLabelBMFont::labelWithString( buf, kFont );
		mediumScoresLabel->setPosition( ccp( screenSize.width * 0.65f, screenSize.height * 0.50f ) );
		this->addChild( mediumScoresLabel, 10 );
		mediumScoresLabel->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.65f, screenSize.height * 0.55f ) ),
				CCFadeIn::actionWithDuration( FADE_TIME ),
				NULL ) );

		getCost( buf, scores );
		CCSprite *mediumMedal = CCSprite::spriteWithFile( buf );
		mediumMedal->setPosition( ccp( screenSize.width * 0.92f, screenSize.height * 0.50f ) );
		this->addChild( mediumMedal, 10 );
		mediumMedal->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.92f, screenSize.height * 0.55f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL ) );

		// ������ "������"
		CCLabelBMFont* easyTextLabel = CCLabelBMFont::labelWithString( LABEL_EASY, kFont );
		easyTextLabel->setPosition( ccp( screenSize.width * 0.25f, screenSize.height * 0.35f ) );
		this->addChild( easyTextLabel, 10 );
		easyTextLabel->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.25f, screenSize.height * 0.40f ) ),
				CCFadeIn::actionWithDuration( FADE_TIME ),
				NULL ) );
		
		scores = _CONFIGMAN->Conf.getScoresEasy();
		sprintf( buf, "%d", scores >= 0 ? scores : 0 );
		CCLabelBMFont* easyScoresLabel = CCLabelBMFont::labelWithString( buf, kFont );
		easyScoresLabel->setPosition( ccp( screenSize.width * 0.65f, screenSize.height * 0.35f ) );
		this->addChild( easyScoresLabel, 10 );
		easyScoresLabel->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.65f, screenSize.height * 0.40f ) ),
				CCFadeIn::actionWithDuration( FADE_TIME ),
				NULL ) );

		getCost( buf, scores );
		CCSprite *easyMedal = CCSprite::spriteWithFile( buf );
		easyMedal->setPosition( ccp( screenSize.width * 0.92f, screenSize.height * 0.35f ) );
		this->addChild( easyMedal, 10 );
		easyMedal->runAction( CCSpawn::actions( CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.92f, screenSize.height * 0.40f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL ) );

		// ������� ���������
		CCSprite *hider = CCSprite::spriteWithFile( IM_PAPER );
		hider->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( hider, 20 );		
		hider->runAction( CCSpawn::actions( 
			CCFadeOut::actionWithDuration( FADE_TIME ), 
			CCCallFuncO::actionWithTarget( this, callfuncO_selector( HighscoresLayer::displayMenu ), hider ),
			NULL ) );		

		// ��� ����������� FPS
#if NEED_DF
		CCSprite* fpsDisplay = CCSprite::spriteWithFile( IM_SKY );
		fpsDisplay->setPosition( ccp( fpsDisplay->boundingBox().size.width * FPS_COEFF, fpsDisplay->boundingBox().size.height * FPS_COEFF) );
		this->addChild(fpsDisplay, 200 );
#endif
}


void HighscoresLayer::getCost( char* buf, int scores ) {
	if( scores >= FIRST_CUP_SCORES ) {
		strcpy( buf, IM_MEDAL1 );
	}
	else if( scores >= SECOND_CUP_SCORES ) {
		strcpy( buf, IM_MEDAL2 );
	}
	else if( scores >= THIRD_CUP_SCORES ) {
		strcpy( buf, IM_MEDAL3 );
	}
	else {
		strcpy( buf, IM_MEDAL0 );
	}
}
