#ifndef HIGHSCORESSCENE_H
#define HIGHSCORESSCENE_H

#include "HighscoresLayer.h"

using namespace cocos2d;

class HighscoresScene : public CCScene {
public:
	SCENE_NODE_FUNC( HighscoresScene );
	bool init();	
};

#endif
