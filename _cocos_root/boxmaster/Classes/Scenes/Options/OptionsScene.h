#ifndef OPTIONSSCENE_H
#define OPTIONSSCENE_H

#include "OptionsLayer.h"

using namespace cocos2d;

class OptionsScene : public CCScene
{
public:
	SCENE_NODE_FUNC( OptionsScene );
	bool init();
};

#endif