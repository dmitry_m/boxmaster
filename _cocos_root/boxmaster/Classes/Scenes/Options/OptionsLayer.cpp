#include "OptionsLayer.h"

OptionsLayer::OptionsLayer() {
}

void OptionsLayer::returnToMainMenu( CCObject* pSender ) {
	if( !disableInput) {
		GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );
		disableInput = true;
	}
}

void OptionsLayer::musicTogglePressed( CCObject* pSender ) {
	GameManager::sharedGameManager()->setIsMusicON( !GameManager::sharedGameManager()->getIsMusicON() );
	if( GameManager::sharedGameManager()->getIsMusicON() ) {
		PLAYSOUNDEFFECT( TURN_ON );
		GameManager::sharedGameManager()->playBackgroundTrack( BACKGROUND_MUSIC2 );
	}
	_CONFIGMAN->settingsUpdated();	
}

void OptionsLayer::SFXTogglePressed( CCObject* pSender ) {
	GameManager::sharedGameManager()->setIsSoundEffectsON( !GameManager::sharedGameManager()->getIsSoundEffectsON() );
	PLAYSOUNDEFFECT( TURN_ON );
	_CONFIGMAN->settingsUpdated();	
}

void OptionsLayer::JoystickPositionTogglePressed( CCObject* pSender ) {
	GameManager::sharedGameManager()->setOrientation( !GameManager::sharedGameManager()->getOrientation() ); 
	if( GameManager::sharedGameManager()->getOrientation() ) {	
		PLAYSOUNDEFFECT( TURN_ON );
	}
	else {
		PLAYSOUNDEFFECT( TURN_OFF );
	}
	_CONFIGMAN->settingsUpdated();	
}

bool OptionsLayer::init() {
	bool pRet = false;
	if ( CCLayer::init() ) {
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize(); 
		disableInput = false;
		
		CCSprite *background = CCSprite::spriteWithFile( IM_MAIN_MENU_BACK );
		background->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background );

		this->setIsKeypadEnabled( true );

		// for fps visible
#if NEED_DF
			CCSprite* fpsDisplay = CCSprite::spriteWithFile( IM_SKY );
			fpsDisplay->setPosition( ccp( fpsDisplay->boundingBox().size.width * FPS_COEFF, fpsDisplay->boundingBox().size.height * FPS_COEFF) );
			this->addChild(fpsDisplay, 200 );
#endif
				
		CCLabelBMFont *musicOnLabelText = CCLabelBMFont::labelWithString( LABEL_MUSIC_ON, kFont );
		musicOnLabelText->setScale( 0.8 );
		CCLabelBMFont *musicOffLabelText = CCLabelBMFont::labelWithString( LABEL_MUSIC_OFF, kFont );
		musicOffLabelText->setScale( 0.8 );
		CCLabelBMFont *SFXOnLabelText = CCLabelBMFont::labelWithString( LABEL_SFX_ON, kFont );
		SFXOnLabelText->setScale( 0.8 );
		CCLabelBMFont *SFXOffLabelText = CCLabelBMFont::labelWithString( LABEL_SFX_OFF, kFont );
		SFXOffLabelText->setScale( 0.8 );
		CCLabelBMFont *JoystickPosition1Text = CCLabelBMFont::labelWithString( LABEL_RIGHT_HANDED, kFont );
		JoystickPosition1Text->setScale( 0.8 );
		CCLabelBMFont *JoystickPosition2Text = CCLabelBMFont::labelWithString( LABEL_LEFT_HANDED, kFont );
		JoystickPosition2Text->setScale( 0.8 );

		CCMenuItemLabel *musicOnLabel = CCMenuItemLabel::itemWithLabel( musicOnLabelText, this, NULL );
		CCMenuItemLabel *musicOffLabel = CCMenuItemLabel::itemWithLabel( musicOffLabelText, this, NULL );
		CCMenuItemLabel *SFXOnLabel = CCMenuItemLabel::itemWithLabel( SFXOnLabelText, this, NULL );
		CCMenuItemLabel *SFXOffLabel = CCMenuItemLabel::itemWithLabel( SFXOffLabelText, this, NULL );
		CCMenuItemLabel *JoystickPosition1 = CCMenuItemLabel::itemWithLabel( JoystickPosition1Text, this, NULL );
		CCMenuItemLabel *JoystickPosition2 = CCMenuItemLabel::itemWithLabel( JoystickPosition2Text, this, NULL );

										 
		CCMenuItemToggle *musicToggle = CCMenuItemToggle::itemWithTarget( this,	
											menu_selector( OptionsLayer::musicTogglePressed ), 
											musicOnLabel, 
											musicOffLabel, 
											NULL );
		
		CCMenuItemToggle *SFXToggle = CCMenuItemToggle::itemWithTarget( this, 
											menu_selector( OptionsLayer::SFXTogglePressed ), 
											SFXOnLabel, 
											SFXOffLabel, 
											NULL );
		CCMenuItemToggle *JoystickPositionToggle = CCMenuItemToggle::itemWithTarget( this, 
											menu_selector( OptionsLayer::JoystickPositionTogglePressed ), 
											JoystickPosition1, 
											JoystickPosition2, 
											NULL );

		CCMenu *optionsMenu = CCMenu::menuWithItems( SFXToggle,
											musicToggle,
											JoystickPositionToggle,
											NULL );
				
		optionsMenu->alignItemsVerticallyWithPadding( 20.0f );
		optionsMenu->setPosition( ccp( screenSize.width * 0.55f, screenSize.height * 0.5f ) );
		
		CCAction* moveAction = CCSpawn::actions(
			CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.55f, screenSize.height * 0.55f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL );
		optionsMenu->runAction( moveAction );
		this->addChild( optionsMenu );

		// return to main menu button	
		CCMenuItemImage *returnButton = CCMenuItemImage::itemFromNormalImage( 
			IM_RETURN,
			IM_RETURN_PR,
			NULL,
			this,
			menu_selector( OptionsLayer::returnToMainMenu ) );

		returnButtonMenu = CCMenu::menuWithItems( returnButton, NULL );
		returnButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.259f );
		returnButtonMenu->setPosition( ccp( 0, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT) );
		returnButtonMenu->setOpacity( 0 );

		CCAction* returnButtonAction = CCSpawn::actions( 
			CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_LEFT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL );

		returnButtonMenu->runAction( returnButtonAction );
		this->addChild( returnButtonMenu, 5, kMainMenuTagValue );
        
        if ( GameManager::sharedGameManager()->getIsMusicON() == false ) 
            musicToggle->setSelectedIndex( 1 ); // Music is OFF        
		if ( GameManager::sharedGameManager()->getIsSoundEffectsON() == false ) 
            SFXToggle->setSelectedIndex( 1 ); // SFX are OFF
		if ( GameManager::sharedGameManager()->getOrientation() == true ) 
            JoystickPositionToggle->setSelectedIndex( 1 ); // ������ ���������
		
		pRet = true;
	}
	return pRet;
}


void OptionsLayer::keyBackClicked() {
	this->returnToMainMenu( NULL );		
}