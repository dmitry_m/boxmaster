#include "OptionsScene.h"

bool OptionsScene::init()
{
	bool pRet = 0;
	if( CCScene::init() )
	{
		OptionsLayer *myLayer = OptionsLayer::node();
		this->addChild( myLayer );
		pRet = 1;
	}
	return pRet;
}