#ifndef OPTIONSLAYER_H
#define OPTIONSLAYER_H

#include "Constants\Constants.h"
#include "Singletons\GameManager.h"

using namespace cocos2d;

class OptionsLayer : public CCLayer
{
	CCMenu *returnButtonMenu;
	bool disableInput;

public:
	OptionsLayer();
	LAYER_NODE_FUNC( OptionsLayer );
	void returnToMainMenu( CCObject* pSender );
	void showCredits( CCObject* pSender );
	void musicTogglePressed( CCObject* pSender );
	void SFXTogglePressed( CCObject* pSender );
	void JoystickPositionTogglePressed( CCObject* pSender );
	bool init();	
	void keyBackClicked();
};

#endif