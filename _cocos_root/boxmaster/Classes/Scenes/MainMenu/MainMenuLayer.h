#ifndef MAINMENULAYER_H
#define MAINMENULAYER_H

#include "Constants\Constants.h"
#include "Singletons\GameManager.h"

using namespace cocos2d;

class MainMenuLayer : public CCLayer {
	bool mainMenu;
	bool menuLoaded;
	bool sceneMenuUsed;
	CCLabelBMFont *captionLabel;
	CCMenu *mainMenuPlay;
	CCMenu *mainMenuSettings;
	CCMenu *mainMenuInfo;	
	CCMenu *sceneSelectMenu;
	bool displaingScenes;
	CCLabelBMFont* labelText;
	CCSprite *hider;
	
	void loaded();
	void hideMainMenuButtons( CCNode *pSender );
	void keyBackClicked();
	void ccTouchesBegan( CCSet *touches, CCEvent *event );

	// main menu	
	void showOptions( CCObject* pSender );
	void showCredits( CCObject* pSender );
	void showScenes( CCObject* pSender );
	void showExit( CCObject* pSender );
	
	// scene menu
	void playScene( CCObject* pSender ); 
	void returnToMainMenuFromScenes( CCObject* pSender );

	// menus
	void displayMainMenu( CCObject* pSender );
	void displaySceneSelection( CCObject* pSender );

	void resetSprite( CCNode* spr ); 
	
#if NEED_HS 	
	CCMenu *mainMenuHighscores;
	void showHighscores( CCObject* pSender );
#endif

#if NEED_OF
	CCMenu *mainMenuOpenFeint;	
	void ofClick( CCObject* pSender );
#endif

public:
	MainMenuLayer();
	~MainMenuLayer();
	LAYER_NODE_FUNC( MainMenuLayer );

	bool init();
};

#endif