#include "MainMenuLayer.h"

MainMenuLayer::MainMenuLayer() {
	mainMenuPlay = NULL;
	mainMenuSettings = NULL;
	mainMenuInfo = NULL;
	#if NEED_OF
		mainMenuOpenFeint = NULL;
	#endif
	sceneSelectMenu = NULL;
}

MainMenuLayer::~MainMenuLayer() {

}

bool MainMenuLayer::init() {
	bool pRet = false;
	if ( CCLayer::init() ) 	{
		if( GameManager::sharedGameManager()->getOldScene() != kOptionsScene &&
			GameManager::sharedGameManager()->getOldScene() != kCreditsScene &&
			GameManager::sharedGameManager()->getOldScene() != kHighscoresScene &&
			GameManager::sharedGameManager()->getOldScene() != kHelpScene &&
			GameManager::sharedGameManager()->getOldScene() != kExitScene ) {

			GameManager::sharedGameManager()->playBackgroundTrack( BACKGROUND_MUSIC2 );
		}
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
		
		sceneMenuUsed = false;
		mainMenu = false;
		menuLoaded = false;
		this->setIsKeypadEnabled( true );

		CCSprite *background = CCSprite::spriteWithFile( IM_MAIN_MENU_BACK );
		background->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background, 1 );

		// loader label
#if !FREE
		labelText = CCLabelBMFont::labelWithString( LABEL_GAME_NAME, kEngBigFont );
#else
		labelText = CCLabelBMFont::labelWithString( LABEL_GAME_NAME_FREE, kEngBigFont );
#endif
#if NEED_FREE
		labelText->setPosition( ccp( screenSize.width * 0.5, screenSize.height * 0.75f ) );
#else
		labelText->setPosition( ccp( screenSize.width * 0.5, screenSize.height * 0.80f ) );
#endif
		labelText->setScale( 0.8 );
		this->addChild( labelText, 5 );		
				
		// for fps visible
#if NEED_DF
			CCSprite* fpsDisplay = CCSprite::spriteWithFile( IM_SKY );
			fpsDisplay->setPosition( ccp( fpsDisplay->boundingBox().size.width * FPS_COEFF, fpsDisplay->boundingBox().size.height * FPS_COEFF) );
			this->addChild(fpsDisplay, 200 );
#endif				
#if		(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			this->setIsTouchEnabled( true );
#endif		
		
		// hide
		hider = CCSprite::spriteWithFile( IM_PAPER );
		hider->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( hider, 20 );
		
		CCFiniteTimeAction *hiderFadeOut = CCSpawn::actions( 
			CCFadeOut::actionWithDuration( FADE_TIME ), 
			CCCallFuncO::actionWithTarget( this, callfuncO_selector( MainMenuLayer::displayMainMenu ), NULL ),
			NULL );

		hider->runAction( hiderFadeOut );

#if NEED_OF
		RunJava::getScores();
#endif
		pRet = true;
	}
	return pRet;
}

void MainMenuLayer::resetSprite( CCNode* spr ) {
	if( !spr ) {
		return;
	}
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	float targetAlt = spr->getPosition().y < screenSize.height/2 ? 0.9f * screenSize.height : 0.1f * screenSize.height ;

	spr->runAction( CCSequence::actions ( 
		CCMoveTo::actionWithDuration( 0.5f, ccp( spr->getPosition().x, targetAlt ) ),
		CCCallFuncN::actionWithTarget( this, callfuncN_selector( MainMenuLayer::resetSprite ) ),
		NULL ) );
}

void MainMenuLayer::loaded() {
	menuLoaded = true;
}

//#####################################################################
// from main menu

void MainMenuLayer::showOptions( CCObject* pSender ) {
	if( !mainMenu || !menuLoaded ) {
		return;
	}
	GameManager::sharedGameManager()->runSceneWithID( kOptionsScene );
}

void MainMenuLayer::showCredits( CCObject* pSender ) {
	if( !mainMenu || !menuLoaded ) {
		return;
	}
	GameManager::sharedGameManager()->runSceneWithID( kHelpScene );
}

#if NEED_HS	
	void MainMenuLayer::showHighscores( CCObject* pSender ) {
		if( !mainMenu || !menuLoaded ) {
			return;
		}
		GameManager::sharedGameManager()->runSceneWithID( kHighscoresScene );
	}
#endif

void MainMenuLayer::showScenes( CCObject* pSender ) {
	if( !mainMenu || !menuLoaded ) {
		return;
	}
	displaySceneSelection( NULL );
}

void MainMenuLayer::showExit( CCObject* pSender ) {
	if( !mainMenu || !menuLoaded ) {
		return;
	}
	GameManager::sharedGameManager()->runSceneWithID( kExitScene );
}

#if NEED_OF 
	void MainMenuLayer::ofClick( CCObject* pSender ) {
		if( !mainMenu || !menuLoaded ) {
			return;
		}
		RunJava::openDashBoard();
	}
#endif

//#####################################################################
// from scenes menu
	
void MainMenuLayer::playScene( CCObject* pSender ) {
	if( mainMenu || sceneMenuUsed ) {
		return;
	}
	sceneMenuUsed = true;
	CCMenuItemFont* itemPassedIn = ( CCMenuItemFont* )pSender;
	if ( itemPassedIn->getTag()== 1 ){
		//
		GameManager::sharedGameManager()->setGameComplexity( kHard );
		GameManager::sharedGameManager()->runSceneWithID( kGameLevel1 );
	} 
	else if ( itemPassedIn->getTag()== 2 ) {
		GameManager::sharedGameManager()->setGameComplexity( kMedium );
		GameManager::sharedGameManager()->runSceneWithID( kGameLevel1 );
	}
	else if ( itemPassedIn->getTag()== 3 ) {
		GameManager::sharedGameManager()->setGameComplexity( kEasy );
		GameManager::sharedGameManager()->runSceneWithID( kGameLevel1 );
	}
	else {
		CCLOG( "Tag was: %d", itemPassedIn->getTag() );
		CCLOG( "Placeholder for next chapters" );
	}
}

void MainMenuLayer::returnToMainMenuFromScenes( CCObject* pSender ) {
	if( mainMenu || !menuLoaded ) {
		return;
	}
	displayMainMenu( NULL );
}

//#####################################################################
// show menu

void MainMenuLayer::displayMainMenu( CCObject* pSender ) {
	mainMenu = true;
	menuLoaded = false;

	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	if( sceneSelectMenu != NULL )
		sceneSelectMenu->removeFromParentAndCleanup( true );


	// ������ Play
	CCMenuItemImage *playGameButton = CCMenuItemImage::itemFromNormalImage( 
		IM_PLAY,
		IM_PLAY_PR,
		NULL,
		this,
		menu_selector( MainMenuLayer::showScenes ) );

	playGameButton->setTag( 1 );

	mainMenuPlay = CCMenu::menuWithItems( playGameButton, NULL );
	mainMenuPlay->setOpacity( 0 );
	mainMenuPlay->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	mainMenuPlay->setPosition( ccp( screenSize.width * 0.50f, screenSize.height * 0.48f - playGameButton->boundingBox().size.height/8 ) );
	CCAction* movePlayAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.50f, screenSize.height * 0.48f ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	mainMenuPlay->runAction( movePlayAction );
	this->addChild( mainMenuPlay, 5 );
	
	// ������ Settings
	CCMenuItemImage *optionsButton = CCMenuItemImage::itemFromNormalImage( 
		IM_SETTINGS,
		IM_SETTINGS_PR,
		NULL,
		this,
		menu_selector( MainMenuLayer::showOptions ) );

	mainMenuSettings = CCMenu::menuWithItems( optionsButton, NULL );
	mainMenuSettings->setOpacity( 0 );
	mainMenuSettings->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	mainMenuSettings->setPosition( ccp( screenSize.width, MENU_BOTTOM_BUTTON_Y_COEFFICIENT * screenSize.height ) );

	CCAction* moveSettingsAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_RIGHT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	mainMenuSettings->runAction( moveSettingsAction );
	this->addChild( mainMenuSettings, 5 );

	// ������ Info	
	CCMenuItemImage *infoButton = CCMenuItemImage::itemFromNormalImage( 
		IM_INFO,
		IM_INFO_PR,
		NULL,
		this,
		menu_selector( MainMenuLayer::showCredits ) );

	mainMenuInfo = CCMenu::menuWithItems( infoButton, NULL );
	mainMenuInfo->setOpacity( 0 );
	mainMenuInfo->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	mainMenuInfo->setPosition( ccp( 0, MENU_BOTTOM_BUTTON_Y_COEFFICIENT * screenSize.height ) );

	CCAction* moveInfoAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_LEFT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	mainMenuInfo->runAction( moveInfoAction );
	this->addChild( mainMenuInfo, 5 );

#if NEED_HS
	// ������ Highscore	
	CCMenuItemImage *highscoresButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HIGHSCORES,
		IM_HIGHSCORES_PR,
		NULL,
		this,
		menu_selector( MainMenuLayer::showHighscores ) );

	mainMenuHighscores = CCMenu::menuWithItems( highscoresButton, NULL );
	mainMenuHighscores->setOpacity( 0 );
	mainMenuHighscores->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	mainMenuHighscores->setPosition( ccp( screenSize.width * 0.5f, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT - highscoresButton->boundingBox().size.height/8  ) );

	CCAction* moveHighscoresAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp(screenSize.width * 0.5f, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	mainMenuHighscores->runAction( moveHighscoresAction );
	this->addChild( mainMenuHighscores, 5 );
#endif

#if NEED_OF
	// ������ openfeint
	if( mainMenuOpenFeint ) {
	mainMenuOpenFeint->removeFromParentAndCleanup( true );
	}
	CCMenuItemImage *ofButton = CCMenuItemImage::itemFromNormalImage( 
		IM_OF,
		IM_OF,
		NULL,
		this,
		menu_selector( MainMenuLayer::ofClick ) );

	mainMenuOpenFeint = CCMenu::menuWithItems( ofButton, NULL );
	mainMenuOpenFeint->setOpacity( 0 );
	mainMenuOpenFeint->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	mainMenuOpenFeint->setPosition( ccp( screenSize.width, MENU_OF_BUTTON_Y_COEFFICIENT * screenSize.height ) );

	CCAction* moveOFAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_RIGHT_BUTTON_X_COEFFICIENT, MENU_OF_BUTTON_Y_COEFFICIENT * screenSize.height ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	mainMenuOpenFeint->runAction( moveOFAction );
	this->addChild( mainMenuOpenFeint, 5 );
#endif

	//#####################################################################
	
	this->runAction( CCSequence::actions(
		CCActionInterval::actionWithDuration( FADE_TIME ), 
		CCCallFunc::actionWithTarget( this, callfunc_selector( MainMenuLayer::loaded ) ),
		NULL ) );
}

void MainMenuLayer::displaySceneSelection( CCObject* pSender ) {
	mainMenu = false;
	menuLoaded = false;
	
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	if ( mainMenuPlay != NULL ) {
		mainMenuPlay->removeFromParentAndCleanup( true );
		mainMenuPlay = 0;
	}
	if ( mainMenuSettings != NULL ) {
		mainMenuSettings->removeFromParentAndCleanup( true );
		mainMenuSettings = 0;
	}
	if ( mainMenuInfo != NULL ) {
		mainMenuInfo->removeFromParentAndCleanup( true );
		mainMenuInfo = 0;
	}
#if NEED_OF
	if ( mainMenuOpenFeint != NULL ) {
		mainMenuOpenFeint->removeFromParentAndCleanup( true );
		mainMenuOpenFeint = 0;
	}
#endif
#if NEED_HS
	if ( mainMenuHighscores != NULL ) {
		mainMenuHighscores->removeFromParentAndCleanup( true );
		mainMenuHighscores = 0;
	}
#endif


	CCLabelBMFont *playScene1Label = CCLabelBMFont::labelWithString( LABEL_HARD,
		kFont );
	CCMenuItemLabel *playScene1 = CCMenuItemLabel::itemWithLabel( playScene1Label,
		this,
		menu_selector( MainMenuLayer::playScene ) );
	playScene1->setTag( 1 );

	CCLabelBMFont *playScene2Label = CCLabelBMFont::labelWithString( LABEL_MEDIUM,
		kFont );
	CCMenuItemLabel *playScene2 = CCMenuItemLabel::itemWithLabel( playScene2Label,
		this,
		menu_selector( MainMenuLayer::playScene ) );
	playScene2->setTag( 2 );

	CCLabelBMFont *playScene3Label = CCLabelBMFont::labelWithString( LABEL_EASY,
		kFont );
	CCMenuItemLabel *playScene3 = CCMenuItemLabel::itemWithLabel( playScene3Label,
		this,
		menu_selector( MainMenuLayer::playScene ) );
	playScene3->setTag( 3 );

	CCLabelBMFont *backButtonLabel = CCLabelBMFont::labelWithString( LABEL_BACK, 
		kFont );
	CCMenuItemLabel *backButton = CCMenuItemLabel::itemWithLabel( backButtonLabel,
		this,
		menu_selector( MainMenuLayer::returnToMainMenuFromScenes ) );

	sceneSelectMenu = CCMenu::menuWithItems( playScene1,
		playScene2,
		playScene3,
		backButton,
		NULL );
	sceneSelectMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.02f );
	sceneSelectMenu->setPosition( ccp( screenSize.width / 2, screenSize.height * 0.30f ) );
	
	CCAction* moveAction = CCSpawn::actions(
			CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.5f, screenSize.height * 0.35f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL );
	//auto moveEffect = CCEaseIn::actionWithAction( moveAction, 1.0f );
	sceneSelectMenu->runAction( moveAction );
	this->addChild( sceneSelectMenu, 5, kSceneMenuTagValue );

	//#####################################################################

	this->runAction( CCSequence::actions(
		CCActionInterval::actionWithDuration( FADE_TIME ), 
		CCCallFunc::actionWithTarget( this, callfunc_selector( MainMenuLayer::loaded ) ),
		NULL ) );
}

//#####################################################################
// keyback menu
	
void MainMenuLayer::keyBackClicked() {
	// return from scene selection
	if( !mainMenu && menuLoaded ) {
		returnToMainMenuFromScenes( NULL );
	}
	// exit dialog
	else if( mainMenu && menuLoaded ) {	
		showExit( NULL );
	}
}

void MainMenuLayer::ccTouchesBegan( CCSet *touches, CCEvent *event ) {
	CCTouch *touch = ( CCTouch * ) touches->anyObject();
	if ( touch ) {
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)			
			keyBackClicked();
		#endif
	}
}

//#####################################################################
// keyback & touches

void MainMenuLayer::hideMainMenuButtons( CCNode *pSender ) {	
	
		mainMenuPlay->setIsVisible( false );	
		mainMenuSettings->setIsVisible( false );
		mainMenuInfo->setIsVisible( false );	
}

//####################################################################
// touches
