#include "MainMenuScene.h"

bool MainMenuScene::init() 
{
	bool pRet = 0;
	if( CCScene::init() )
	{	
		mainMenuLayer = MainMenuLayer::node();
		this->addChild( mainMenuLayer );
		pRet = 1;
	}
	return pRet;
}
