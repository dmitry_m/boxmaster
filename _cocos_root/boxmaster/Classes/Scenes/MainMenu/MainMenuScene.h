#ifndef MAINMENUSCENE_H
#define MAINMENUSCENE_H

#include "MainMenuLayer.h"

using namespace cocos2d;

class MainMenuScene : public CCScene 
{
public:
	MainMenuLayer *mainMenuLayer;

	SCENE_NODE_FUNC( MainMenuScene );
	bool init();
};

#endif