#ifndef CREDITSLAYER_H
#define CREDITSLAYER_H

#include "Singletons\GameManager.h"

using namespace cocos2d;

class CreditsLayer : public CCLayer {
	CCMenu *returnButtonMenu;
	bool disableInput;

public:
	CCLabelBMFont *label;
	LAYER_NODE_FUNC( CreditsLayer );
	
	void initCreditsLabels();
	void returnToMainMenu( CCObject* pSender );
	void displayMenu( CCObject* pSender );
	bool init();
	void keyBackClicked();
};

#endif