#ifndef CREDITSSCENE_H
#define CREDITSSCENE_H

#include "CreditsLayer.h"

using namespace cocos2d;

class CreditsScene : public CCScene {
public:
	SCENE_NODE_FUNC( CreditsScene );
	bool init();	
};

#endif
