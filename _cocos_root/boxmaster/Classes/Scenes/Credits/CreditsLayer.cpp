#include "CreditsLayer.h"

bool CreditsLayer::init() {
	bool pRet = 0;
	if ( CCLayer::init() ) {
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize(); 
		disableInput = false;
		this->setIsTouchEnabled( false );
		this->setIsKeypadEnabled( true );

		CCSprite *background_paper = CCSprite::spriteWithFile( IM_PAPER );
		background_paper->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background_paper, 0 );

		CCSprite *cocos = CCSprite::spriteWithFile( IM_COCOS );
		cocos->setPosition( ccp( screenSize.width * 0.55f, screenSize.height * 0.30f ) );
		this->addChild( cocos, 0 );

		initCreditsLabels();

		// for fps visible
#if NEED_DF
			CCSprite* fpsDisplay = CCSprite::spriteWithFile( IM_SKY );
			fpsDisplay->setPosition( ccp( fpsDisplay->boundingBox().size.width * FPS_COEFF, fpsDisplay->boundingBox().size.height * FPS_COEFF) );
			this->addChild(fpsDisplay, 200 );
#endif

		CCSprite *hider = CCSprite::spriteWithFile( IM_PAPER );
		hider->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( hider, 20 );
		
		hider->runAction( CCSpawn::actions( 
			CCFadeOut::actionWithDuration( FADE_TIME ), 
			CCCallFuncO::actionWithTarget( this, callfuncO_selector( CreditsLayer::displayMenu ), hider ),
			NULL ) );
		

		pRet = 1;
	}
	return pRet;
}


void CreditsLayer::returnToMainMenu( CCObject* pSender ) {
	if( !disableInput) {
		GameManager::sharedGameManager()->runSceneWithID( kHelpScene );
		disableInput = true;
	}
} 

void CreditsLayer::initCreditsLabels() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	// ������ "<��������> <������>"
	CCLabelBMFont* gameName = CCLabelBMFont::labelWithString( LABEL_GAME_NAME, kEngFont );
	gameName->setPosition( ccp( screenSize.width * 0.45f, screenSize.height * 0.90f ) );
	this->addChild( gameName, 10 );
	
	CCLabelBMFont* version = CCLabelBMFont::labelWithString( LABEL_VERSION, kFont );
	version->setPosition( ccp( screenSize.width * 0.75f, screenSize.height * 0.90f ) );
	this->addChild( version, 10 );
	
	// ������ "����������� � �������: ������ �������" 
	CCLabelBMFont* programmer = CCLabelBMFont::labelWithString( LABEL_PROGRAMMER, kFont );
	programmer->setPosition( ccp( screenSize.width * 0.30f, screenSize.height * 0.75f ) );
	programmer->setScale( 0.55 );
	this->addChild( programmer, 10 );

	CCLabelBMFont* graphics = CCLabelBMFont::labelWithString( LABEL_DESIGNER, kFont );
	graphics->setPosition( ccp( screenSize.width * 0.30f, screenSize.height * 0.65f ) );
	graphics->setScale( 0.55 );
	this->addChild( graphics, 10 );	

	CCLabelBMFont* me1 = CCLabelBMFont::labelWithString( LABEL_MY_NAME, kFont );
	me1->setPosition( ccp( screenSize.width * 0.65f, screenSize.height * 0.70f ) );
	me1->setScale( 0.55 );
	this->addChild( me1, 10 );
	
	// ������ "�������: ������ �������"

	// ������ "��������: ����� ���������"
	CCLabelBMFont* music = CCLabelBMFont::labelWithString( LABEL_MUSIC, kFont );
	music->setPosition( ccp( screenSize.width * 0.30f, screenSize.height * 0.45f ) );
	music->setScale( 0.55 );
	this->addChild( music, 10 );	

	CCLabelBMFont* musicAuthor = CCLabelBMFont::labelWithString( LABEL_MUSIC_AUTHOR, kFont );
	musicAuthor->setPosition( ccp( screenSize.width * 0.65f, screenSize.height * 0.45f ) );
	musicAuthor->setScale( 0.55 );
	this->addChild( musicAuthor, 10 );
	
	// ������ "��������"
	CCLabelBMFont* copyright = CCLabelBMFont::labelWithString( LABEL_COPYRIGHT, kFont );
	copyright->setPosition( ccp( screenSize.width * 0.55f, screenSize.height * 0.15f ) );
	copyright->setScale( 0.55 );
	this->addChild( copyright, 10 );
	
	// ������ "��� ����� ��������"
	CCLabelBMFont* allRightsReserved = CCLabelBMFont::labelWithString( LABEL_ALL_RIGHTS, kFont );
	allRightsReserved->setPosition( ccp( screenSize.width * 0.55f, screenSize.height * 0.07f ) );
	allRightsReserved->setScale( 0.55 );
	this->addChild( allRightsReserved, 10 );
} 

void CreditsLayer::displayMenu( CCObject* pSender ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	// return to main menu button	
	CCMenuItemImage *returnButton = CCMenuItemImage::itemFromNormalImage( 
		IM_RETURN,
		IM_RETURN_PR,
		NULL,
		this,
		menu_selector( CreditsLayer::returnToMainMenu ) );

	returnButtonMenu = CCMenu::menuWithItems( returnButton, NULL );
	returnButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	returnButtonMenu->setPosition( ccp( 0, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT) );
	returnButtonMenu->setOpacity( 0 );

	CCAction* returnButtonAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_LEFT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	returnButtonMenu->runAction( returnButtonAction );
	this->addChild( returnButtonMenu, 5, kMainMenuTagValue );
}

void CreditsLayer::keyBackClicked() {
	this->returnToMainMenu( NULL );		
}