#include "CreditsScene.h"

bool CreditsScene::init() {
	bool pRet = 0;
	if ( CCScene::init() ) {
		CreditsLayer* myCreditsLayer = CreditsLayer::node();
		this->addChild( myCreditsLayer );
		pRet = 1;
	}
	return pRet;
}

