#ifndef GREETINGSCENE_H
#define GREETINGSCENE_H

#include "GreetingLayer.h"

using namespace cocos2d;

class GreetingScene : public CCScene
{
public:
	GreetingLayer* introLayer;
	
	LAYER_NODE_FUNC( GreetingScene );
	bool init();	
};

#endif
