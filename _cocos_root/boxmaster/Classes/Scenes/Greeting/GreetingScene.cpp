#include "GreetingScene.h"

bool GreetingScene::init()
{
	bool pRet = 0;
	if( CCScene::init() )
	{
		introLayer = GreetingLayer::node();
		this->addChild( introLayer );
		pRet = 1;
	}	
	return pRet;
}
