#include "GreetingLayer.h"

using namespace cocos2d;

void GreetingLayer::startGamePlay() {
	GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );	
}

void GreetingLayer::ccTouchesBegan( CCSet *touches, CCEvent *event ) {
	if ( hasBeenSkipped == false ) {
		startGamePlay();
	}
}

bool GreetingLayer::init()
{
	bool pRet = false;
	if ( CCLayer::init() )
	{
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
		this->setIsTouchEnabled( true );


		// background
		CCSprite *background = CCSprite::spriteWithFile( IM_LOGO );
		background->setOpacity( 0 );
		background->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background, 1 );

		background->runAction( CCSequence::actions( 
			CCFadeIn::actionWithDuration( FADE_TIME * 4 ), 
			CCActionInterval::actionWithDuration( 0.0 ),			
			CCFadeOut::actionWithDuration( FADE_TIME / 2 ), 
			CCCallFunc::actionWithTarget( this, callfunc_selector( GreetingLayer::startGamePlay ) ), 
			NULL ) );

		pRet = true;
	}
	return pRet;
}

void GreetingLayer::draw() {
	glClearColor (1.f, 1.f, 1.f, 1.f);
	CCLayer::draw ();
}