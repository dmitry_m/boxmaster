#ifndef GREETINGLAYER_H
#define GREETINGLAYER_H

#include "Constants\Constants.h"
#include "Singletons\GameManager.h"

using namespace cocos2d;

class GreetingLayer : public CCLayer
{
	bool hasBeenSkipped;
public:
	LAYER_NODE_FUNC( GreetingLayer );
	void startGamePlay();	
	void ccTouchesBegan( CCSet *touches, CCEvent *cEvent );
	bool init();
	void draw();
};

#endif