#ifndef SCRIPT_LIST_HANDLER_H_
#define SCRIPT_LIST_HANDLER_H_

#include <list>
#include <memory>

#include "Constants\Constants.h"
#include "Service\DemoHandlers\WaitHandler.h"
#include "Service\DemoHandlers\WalkHandler.h"
#include "Service\DemoHandlers\PushHandler.h"
#include "Service\DemoHandlers\CreateBoxHandler.h"
#include "Service\DemoHandlers\JumpHandler.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"

class Scene1ActionLayer;

class ScriptListHandler {
public:
    static void func0(Scene1ActionLayer * layer, std::list< std::shared_ptr< AbstractHandler > > &list) {
        for (int i = 0; i < 3; i++) {
            list.push_back(std::shared_ptr<AbstractHandler>(new WalkHandler(layer, kRight)));
        }
        for (int i = 0; i < 2; i++) {
            list.push_back(std::shared_ptr<AbstractHandler>(new WalkHandler(layer, kLeft)));
        }
    }
    static void func1(Scene1ActionLayer * layer, std::list< std::shared_ptr< AbstractHandler > > &list) {
        for (int i = 0; i < 2; i++) {
            list.push_back(std::shared_ptr<AbstractHandler>(new JumpHandler(layer, kRight)));
        }
        list.push_back(std::shared_ptr<AbstractHandler>(new JumpHandler(layer, kLeft)));
    }
    static void func2(Scene1ActionLayer * layer, std::list< std::shared_ptr< AbstractHandler > > &list) {
        list.push_back(std::shared_ptr<AbstractHandler>(new  WaitHandler(layer, 1)));
        list.push_back(std::shared_ptr<AbstractHandler>(new CreateBoxHandler(layer, 5)));
        list.push_back(std::shared_ptr<AbstractHandler>(new WaitHandler(layer, 0.2f)));
    }
    static void func3(Scene1ActionLayer * layer, std::list< std::shared_ptr< AbstractHandler > > &list) {
        func0(layer, list);
        for (int i = 0; i < 2; i++) {
            func2(layer, list);
        }
        for (int i = 0; i < 3; i++) {
            func1(layer, list);
        }
        func0(layer, list);
    }
    static void main(Scene1ActionLayer * layer, std::list< std::shared_ptr< AbstractHandler > > &list) {
        func3 (layer, list);
    }
};

#endif