#ifndef SCENE1ACTIONLAYER_H
#define SCENE1ACTIONLAYER_H

#include "Box2D\Box2D.h"
#include "Service\GLES-Render.h"
#include "Service\SimpleQueryCallback.h"
#include "Service\Box2DHelpers.h"
#include "Service\ServiceInfo.h"
#include "Service\FileOperation.h"
#include "Constants\Constants.h"
#include "Singletons\GameManager.h"
#include "Scenes\Scene1\Layers\Scene1UILayer.h"

#include "GameObjects\Box2D\Loader.h"
#include "GameObjects\Box2D\Box.h"
#include "GameObjects\Box2D\Trophy.h"
#include "GameObjects\Cloud.h"
#include "GameObjects\Box2D\GamePhysicsContactListener.h"

#include "SneakyJoystick\SneakyButton.h"
#include "SneakyJoystick\SneakyButtonSkinnedBase.h"

#include "Service\DemoHandlers\AbstractHandler.h"

#include <vector>
#include <list>

using namespace cocos2d;
using namespace std;

class Loader;

class GamePhysicsContactListener;

class Scene1ActionLayer : public CCLayer {
private:	
	int boxFallSignalCount;
	bool active;
	bool touch;
	string configFileName;
	CCPoint currentPos;
	GamePhysicsContactListener* gamePhysicsContactListener;

#if NEED_TR
	Trophy* trophies[4];
	CCPoint arrowBegin;
	CCPoint arrowEnd;
	float arrowBeginSize;
	CCPoint touchBeginAt;
	CCParticleSystem* boxEmitter;
	CCParticleSystem* gravityEmitter;
	CCParticleSystem* teleportEmitter;
	CCSprite* fadingShield;
	CCSprite* fadingArrow;
	
	CC_SYNTHESIZE(CCSprite*, destroyBoxIndicator, DestroyBoxIndicator );
	CC_SYNTHESIZE(CCSprite*, lowGravityIndicator, LowGravityIndicator );
	CC_SYNTHESIZE(CCSprite*, teleportIndicator, TeleportIndicator );
	CC_SYNTHESIZE(CCSprite*, shieldIndicator, ShieldIndicator );
#endif

	CC_SYNTHESIZE( GameComplexityType, levelComplexity, LevelComplexity );
	b2World *world;
	GLESDebugDraw *debugDraw;
	CCSpriteBatchNode *sceneSpriteBatchNode;
	b2Body *groundBody;
	b2MouseJoint *mouseJoint;

	Scene1UILayer *uiLayer;
	CCSpriteBatchNode *groundSpriteBatchNode;
	float32 groundMaxX;
	CC_SYNTHESIZE( Loader*, loader, Loader );

	CCSprite *floor;
	b2Joint * lastBridgeStartJoint;
	b2Joint * lastBridgeEndJoint;
	
	b2Body *offscreenSensorBody;	
	
	CCSprite* fabricator;
	CCSprite* fallIndicator;
	
	std::list< std::shared_ptr< AbstractHandler > >			scriptList;
	std::shared_ptr< AbstractHandler >						currentScriptStep;

	CCParticleSystem *fireOnBridge;
	b2Body *finalBattleSensorBody;
	bool arrowMode;
	bool isGameOver;
	bool inFinalBattle;
	bool actionStopped;
	CCPoint coordinatesForBox;


	Box* field[kBoxCanPlacedMax][kBoxPerCell];	
	vector< FallingInfo > fallingBoxes;
	vector< Cloud* > clouds;
	
	CCLabelBMFont *scoresLabelCaption;
	CCLabelBMFont *scoresLabelText;
	CC_SYNTHESIZE( b2Body*, specialBody, SpecialBody );
	CC_SYNTHESIZE( float, randomizeValue, RandomizeValue );
	CC_SYNTHESIZE( float, strongBoxesValue, StrongBoxesValue );
	CC_SYNTHESIZE( CCLabelTTF*, leftUpperLabel, LeftUpperLabel );
	CC_SYNTHESIZE( bool, isCarRequested, IsCarRequested );
	CC_SYNTHESIZE( bool, backMoveButtonPressed, BackMoveButtonPressed );
	CC_SYNTHESIZE( SneakyButton *, leftButton, LeftButton );
	CC_SYNTHESIZE( SneakyButton *, rightButton, RightButton );
	CC_SYNTHESIZE( SneakyButton *, backMoveButton, BackMoveButton );
	CC_SYNTHESIZE( SneakyButton *, jumpButton, JumpButton );
	CC_SYNTHESIZE( DirectionTypes, lastDirection, LastDirection );

	// shared animations
	CC_SYNTHESIZE( CCAnimation*, boxFadeAnim, BoxFadeAnim );
	CC_SYNTHESIZE( CCAnimation*, trophyFadeAnim, TrophyFadeAnim );

	unsigned int boxFadeInSoundID;
	unsigned int boxFadeOutSoundID;
	unsigned int trophyFadeInSoundId;
	unsigned int trophyFadeOutSoundId;

	// MUSIC
	void AddBoxFallSound();
	void DispatchBoxFallSounds();
	void playBoxFadeInSound();
	void playBoxFadeOutSound();
	void playTrophyFadeInSound();
	void playTrophyFadeOutSound();	
	void playBoxFallSound();
	void playAllBoxFallSound();
	void playSequenceSound();
	void playSequence2Sound();

public:
	
	Box*** getField() {
		return (Box***)field;
	}

	Scene1ActionLayer();
	~Scene1ActionLayer();
	void setUpWorld();
#if NEED_DD
	void setUpDebugDraw();
	virtual void draw();
#endif
	void createBackground();
	void createGround();
	void prepareSpecialBodies();
	void prepareLabels(); 
	void createLoaderAtLocation( CCPoint location );
	void registerWithTouchDispatcher();
	bool initWithScene1UILayer( Scene1UILayer *Scene1UILayer );
	void initAnimations();
	void worldStep( double dt );
	void update( ccTime dt );
	void demoUpdate( ccTime dt );
	int updateTimer(); 
	void checkJoystick();
	DirectionTypes checkDirectionButtons();
	void addBoxAtSpecialPosition( ccTime dt );
	void addBoxAtRandomPosition( ccTime dt );
	void addBoxAtRandomPosition2( ccTime dt );
	void addBoxAtFirstPosition( ccTime dt );
	void addBoxesForDebug();
	void addBoxesForDebug2();
	
	void deleteBox( CCNode* box );
	void dropBox();
	void endBlink();
	void createBoxAtPlaceWithCoordinatesWithIndicator( int x, int y );
	void createBoxAtPlaceWithCoordinates( int x, int y );
	void createBodyAtLocation( CCPoint location, Box2DSprite *sprite, uint16 category, uint16 mask, int group, float32 friction, float32 restitution, float32 density );
	
	bool isReallyEmptyCell( Coordinates coord );
	bool noBoxAtPosition( CCPoint pos );
	void alignToCenterOfCell( b2Body *body );
	void alignToCenterOfCellByOx( b2Body *body );
	void checkAndClampLoaderSpritePosition();	
	
	int howManyBoxes();
	void refreshBoxes();
	void refreshBoxesAtPlaces( int from, int to, int under );
	void addFallingBox( Box *box, int x, int y );
	void checkFallingBoxes( ccTime dt );

	// get, plug, unplug for field
	Box* unplugBoxByCoordinates( Coordinates coordinates );
	Box* unplugBoxByCoordinates( int x, int y );
	bool plugBoxByCoordinates( Box* boxToAdd, Coordinates coordinates );
	bool plugBoxByCoordinates( Box* boxToAdd, int x, int y );
	Box* getBoxByCoordinates( Coordinates coordinates, bool& error );
	Box* getBoxByCoordinates( int x, int y, bool& error );
	Box* getBoxByCoordinates( Coordinates coordinates );
	Box* getBoxByCoordinates( int x, int y );
	int fullness( int index );

	void setDirButtonIsVisible( DirectionTypes button, bool visible );
	void setDirButtonTexture( DirectionTypes button, bool directionTexture );
	void setMoveButtonTexture( DirectionTypes button );
	void setMoveButtonVisible( bool visible );

	// clouds
	void initClouds();
	void stopClouds();
	void pauseClouds();
	void resumeClouds();
	void cloudDispatcher( ccTime dt );
	void freeClouds();

	void updateScoreLabel( int newScores );
	void deleteFirstLine();
	void updateScores( ccTime deltaTime );
	void gameOverWithWin( bool win );
	void exitScene( CCNode *pSender );
	void ShowSituation();
	
	void queryDeleteBox( Box* boxToDelete, bool withParticles, bool withSound );
	void checkForServive( Box* boxCandidate );
	void keyBackClicked();
	void keyMenuClicked();
	void pauseLayer( bool pause );

#if NEED_TR
	void prepareTrophiesIndicators();
	void checkForLoaderCapture();
	void queryDeleteTrophy( Trophy* trophy, bool withParticles, bool withSound );
	void deleteTrophy( CCNode* trophy );
	void ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent );
	void ccTouchesMoved( CCSet *pTouches, CCEvent *pEvent );
	void ccTouchesEnded( CCSet *pTouches, CCEvent *pEvent );
	void StartTeleport();
	void EndTeleport();
	void checkLoaderForTrophies();
	void trophyDispatcher( ccTime dt );
	Trophy* createTrophyAtPlaceWithCoordinates( int x, TrophyTypes type );
	void showBoxParticles( CCPoint position );
	void prepareParticlesForBox();
	void showGravityParticles();
	void prepareParticlesForGravity();
	void turnOffLowGravity();
	void shieldFade( CCPoint position );
	void checkTrophiesPositions();
#endif
};

#endif
	