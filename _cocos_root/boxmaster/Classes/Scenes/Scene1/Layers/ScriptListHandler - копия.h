#ifndef SCRIPT_H_
#define SCRIPT_H_

#include <list>
#include <memory>

#include "Constants\Constants.h"
#include "Service\DemoHandlers\WaitHandler.h"
#include "Service\DemoHandlers\WalkHandler.h"
#include "Service\DemoHandlers\PushHandler.h"
#include "Service\DemoHandlers\CreateBoxHandler.h"
#include "Service\DemoHandlers\JumpHandler.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"

class Scene1ActionLayer; 

class ScriptListHandler {
public:
	static void main(Scene1ActionLayer * layer, std::list< std::shared_ptr< AbstractHandler > > &list) {
		/*	
		list.push_back(std::shared_ptr<AbstractHandler>(new CreateBoxHandler(layer, 1)));
		list.push_back(std::shared_ptr<AbstractHandler>(new WaitHandler(layer, 1.0f)));
		list.push_back(std::shared_ptr<AbstractHandler>(new CreateBoxHandler(layer, 1)));
		list.push_back(std::shared_ptr<AbstractHandler>(new WaitHandler(layer, 1.0f)));
		list.push_back(std::shared_ptr<AbstractHandler>(new JumpHandler(layer, kRight)));
		*/
		list.push_back(std::shared_ptr<AbstractHandler>(new CreateBoxHandler(layer, 3)));
		list.push_back(std::shared_ptr<AbstractHandler>(new WaitHandler(layer, 2.0f)));
		list.push_back(std::shared_ptr<AbstractHandler>(new WalkHandler(layer, kRight)));
		list.push_back(std::shared_ptr<AbstractHandler>(new WalkHandler(layer, kRight)));
		list.push_back(std::shared_ptr<AbstractHandler>(new WalkHandler(layer, kRight)));
		list.push_back(std::shared_ptr<AbstractHandler>(new WalkHandler(layer, kRight)));	
		list.push_back(std::shared_ptr<AbstractHandler>(new WalkHandler(layer, kRight)));
		list.push_back(std::shared_ptr<AbstractHandler>(new PushHandler(layer, kRight)));	
		list.push_back(std::shared_ptr<AbstractHandler>(new WaitHandler(layer, 1.0f)));
		list.push_back(std::shared_ptr<AbstractHandler>(new JumpHandler(layer, kRight)));
	}
};

#endif