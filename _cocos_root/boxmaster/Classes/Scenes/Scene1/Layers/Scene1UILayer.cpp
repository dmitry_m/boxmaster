#include "Scene1UILayer.h"
bool Scene1UILayer::init()
{
	bool pRet = false;
	
	if ( CCLayer::init() )
	{	
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

		// hider
		hider = CCSprite::spriteWithFile( IM_PAPER );
		hider->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( hider, 20 );
		hider->runAction( CCFadeOut::actionWithDuration( FADE_TIME ) );
		// ----------------------------------------------------------------
				
		inverted = _CONFIG.getOrientation();
		this->setIsTouchEnabled( true );
		this->setIsKeypadEnabled( true );

		bigLabel = CCLabelBMFont::labelWithString( "", kBigFont );
		bigLabel->setPosition( ccp( screenSize.width/2, screenSize.height * 0.45f ) );
		bigLabel->setIsVisible( false );
		this->addChild( bigLabel );

		smallLabel = CCLabelBMFont::labelWithString( "", kFont );
		smallLabel->setPosition( ccp( screenSize.width/2, screenSize.height * 0.45f ) );
		smallLabel->setIsVisible( false );
		this->addChild( smallLabel );

		// ���� �����

		// ����������
		continueLabel = CCLabelBMFont::labelWithString( LABEL_CONTINUE, kFont );
		CCMenuItemLabel *continueItem = CCMenuItemLabel::itemWithLabel( continueLabel,
			this,
			menu_selector( Scene1UILayer::continueGame ) );

		// �������� �������  
		sfxLabel = CCLabelBMFont::labelWithString( GameManager::sharedGameManager()->getIsSoundEffectsON() ? LABEL_SFX_ON : LABEL_SFX_OFF, kFont );
		CCMenuItemLabel *sfxItem = CCMenuItemLabel::itemWithLabel( sfxLabel,
			this,
			menu_selector( Scene1UILayer::turnSFX ) );

		// ������
		musicLabel = CCLabelBMFont::labelWithString( GameManager::sharedGameManager()->getIsMusicON() ? LABEL_MUSIC_ON : LABEL_MUSIC_OFF, kFont );
		CCMenuItemLabel *musicItem = CCMenuItemLabel::itemWithLabel( musicLabel,
			this,
			menu_selector( Scene1UILayer::turnMusic ) );

		// ���������
		returnToMainMenuLabel = CCLabelBMFont::labelWithString( LABEL_MAIN_MENU, kFont );
		CCMenuItemLabel *mainMenuItem = CCMenuItemLabel::itemWithLabel( returnToMainMenuLabel,
			this,
			menu_selector( Scene1UILayer::returnToMainMenu ) );

		pauseMenu = CCMenu::menuWithItems( continueItem, sfxItem, musicItem, mainMenuItem, NULL );
		pauseMenu->setOpacity( 0 );
		pauseMenu->setIsVisible( false );
		pauseMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.1f );
		pauseMenu->setPosition( ccp( screenSize.width / 2, screenSize.height * 0.45f ) );
		//pauseMenu->setIsVisible( false );
		this->addChild( pauseMenu, 225 );

		// loading joystick's textures
		CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile( JOYSTICK_PLIST );
		sceneSpriteBatchNode = CCSpriteBatchNode::batchNodeWithFile( JOYSTICK_PNG );

		this->currentLeftButtonTextureIsDirection = false;
		this->currentRightButtonTextureIsDirection = false;		
		this->currentBackMoveButtonTextureIsLeft = false;
		this->initButtons();
		this->addChild( sceneSpriteBatchNode, 1 );

		pRet = true;
	}

	return pRet;
}

/*
void Scene1UILayer::setLoadingProcess( int percent ) {
	if( percent <= 0 || percent > 100 ) {
		return;
	}
	hider->draw();
	if( percent == 100 ) {
		hider->runAction( CCFadeOut::actionWithDuration( FADE_TIME ) );
		klax->runAction( CCFadeOut::actionWithDuration( FADE_TIME ) );
	}
	else {
		klax->setTextureRect( CCRect( 0, 0, percent * klaxWidth / 100, klax->boundingBox().size.height ) );
	}
}
*/

bool Scene1UILayer::displayText( const char *text, CCObject *target, SEL_CallFuncN selector, bool bigText )
{
	CCLabelBMFont *label;
	if( bigText ) {
		label = bigLabel;
	}
	else {
		label = smallLabel;
	}

	label->stopAllActions();
	label->setString( text );
	label->setIsVisible( true );
	label->setScale( 0.6 );
	label->setOpacity( 255 );

	label->runAction( 
		CCSequence::actions(
			CCSpawn::actions( 
				CCScaleTo::actionWithDuration( 3*FADE_TIME, 0.8 ), 
				CCFadeIn::actionWithDuration( 3*FADE_TIME ),
				NULL ),
			CCFadeOut::actionWithDuration( 4*FADE_TIME ),
			NULL ) );

	return true;
}

void Scene1UILayer::initButtons()
{
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	CCRect buttonDimensions = CCRectMake( 0, 0, 64.0f, 64.0f ); 

	CCPoint backMoveButtonPosition;
	CCPoint leftButtonPosition;
	CCPoint rightButtonPosition;
	CCPoint jumpButtonPosition;

	if( !inverted ) {
		backMoveButtonPosition = ccp( screenSize.width * k3dButtonOffset, kMarginBottom/2 );
		leftButtonPosition = ccp( screenSize.width * k1stButtonOffset, kMarginBottom/2 );	
		rightButtonPosition = ccp( screenSize.width * k2dButtonOffset, kMarginBottom/2 );
		jumpButtonPosition = ccp( screenSize.width * k4thButtonOffset, kMarginBottom/2 );
	}
	else {
		backMoveButtonPosition = ccp( screenSize.width * k2dButtonOffset, kMarginBottom/2 );
		leftButtonPosition = ccp( screenSize.width * k3dButtonOffset, kMarginBottom/2 );	
		rightButtonPosition = ccp( screenSize.width * k4thButtonOffset, kMarginBottom/2 );
		jumpButtonPosition = ccp( screenSize.width * k1stButtonOffset, kMarginBottom/2 );
	}
	
	// left button
	leftButtonBase = SneakyButtonSkinnedBase::node(); 
	leftButtonBase->setPosition( leftButtonPosition );
	
	setLeftDirectionButtonTexture( true );
	leftButtonBase->setScale( kJoystickScale );	

	SneakyButton *lButton = new SneakyButton();
	lButton->initWithRect( buttonDimensions );
	lButton->autorelease();

	leftButtonBase->setButton( lButton );	
	leftButton = leftButtonBase->getButton();
	leftButton->setIsHoldable( true );
	leftButton->setIsToggleable( false );
	this->addChild( leftButtonBase ); 

	// right button
	rightButtonBase = SneakyButtonSkinnedBase::node(); 
	rightButtonBase->setPosition( rightButtonPosition );
	
	setRightDirectionButtonTexture( true );
	rightButtonBase->setScale( kJoystickScale );	

	SneakyButton *rButton = new SneakyButton();
	rButton->initWithRect( buttonDimensions );
	rButton->autorelease();

	rightButtonBase->setButton( rButton ); 
	rightButton = rightButtonBase->getButton();
	rightButton->setIsHoldable( true );
	this->addChild( rightButtonBase ); 

	// back move button
	backMoveButtonBase = SneakyButtonSkinnedBase::node(); 
	backMoveButtonBase->setPosition( backMoveButtonPosition );
	backMoveButtonBase->setIsVisible( false );
	
	setBackMoveDirectionButtonTexture( true );
	backMoveButtonBase->setScale( kJoystickScale );

	SneakyButton *bmButton = new SneakyButton();
	bmButton->initWithRect( buttonDimensions );
	bmButton->autorelease();

	backMoveButtonBase->setButton( bmButton ); 
	backMoveButton = backMoveButtonBase->getButton();
	backMoveButton->setIsHoldable( true );
	
	this->addChild( backMoveButtonBase ); 
	
	// jump button
	jumpButtonBase = SneakyButtonSkinnedBase::node(); 
	jumpButtonBase->setPosition( jumpButtonPosition );
	
	setJumpDirectionButtonTexture();
	jumpButtonBase->setScale( kJoystickScale );

	SneakyButton *jButton = new SneakyButton();
	jButton->initWithRect( buttonDimensions );
	jButton->autorelease();

	jumpButtonBase->setButton( jButton ); 
	jumpButton = jumpButtonBase->getButton();
	jumpButton->setIsHoldable( true );
	this->addChild( jumpButtonBase ); 		
}

void Scene1UILayer::setLeftDirectionButtonTexture( bool isDirection ) {
	if( isDirection && !currentLeftButtonTextureIsDirection ) {
		leftButtonBase->setDefaultSprite( CCSprite::spriteWithSpriteFrameName( "leftUp.png" ) ); 
		leftButtonBase->setActivatedSprite( CCSprite::spriteWithSpriteFrameName( "leftUp.png" ) ); 
		leftButtonBase->setPressSprite( CCSprite::spriteWithSpriteFrameName( "leftDown.png" ) );
		currentLeftButtonTextureIsDirection = true;
	} 
	else if( !isDirection && currentLeftButtonTextureIsDirection ) {
		leftButtonBase->setDefaultSprite( CCSprite::spriteWithSpriteFrameName( "boxLeftUp.png" ) ); 
		leftButtonBase->setActivatedSprite( CCSprite::spriteWithSpriteFrameName( "boxLeftDown.png" ) ); 
		leftButtonBase->setPressSprite( CCSprite::spriteWithSpriteFrameName( "boxLeftDown.png" ) ); 
		currentLeftButtonTextureIsDirection = false;
	}
}

void Scene1UILayer::setRightDirectionButtonTexture( bool isDirection ) {
	if( isDirection && !currentRightButtonTextureIsDirection ) {
		rightButtonBase->setDefaultSprite( CCSprite::spriteWithSpriteFrameName( "rightUp.png" ) ); 
		rightButtonBase->setActivatedSprite( CCSprite::spriteWithSpriteFrameName( "rightUp.png" ) );
		rightButtonBase->setPressSprite( CCSprite::spriteWithSpriteFrameName( "rightDown.png" ) );
		currentRightButtonTextureIsDirection = true;
	} 
	else if( !isDirection && currentRightButtonTextureIsDirection ) {
		rightButtonBase->setDefaultSprite( CCSprite::spriteWithSpriteFrameName( "boxRightUp.png" ) ); 
		rightButtonBase->setActivatedSprite( CCSprite::spriteWithSpriteFrameName( "boxRightDown.png" ) ); 
		rightButtonBase->setPressSprite( CCSprite::spriteWithSpriteFrameName( "boxRightDown.png" ) ); 
		currentRightButtonTextureIsDirection = false;
	}
}

void Scene1UILayer::setBackMoveDirectionButtonTexture( bool left ) {
	if( left && !currentBackMoveButtonTextureIsLeft ) {
		backMoveButtonBase->setDefaultSprite( CCSprite::spriteWithSpriteFrameName( "boxLeftUp.png" ) ); 
		backMoveButtonBase->setActivatedSprite( CCSprite::spriteWithSpriteFrameName( "boxLeftUp.png" ) );
		backMoveButtonBase->setPressSprite( CCSprite::spriteWithSpriteFrameName( "boxLeftDown.png" ) );
		currentBackMoveButtonTextureIsLeft = true;
	} 
	else if( !left && currentBackMoveButtonTextureIsLeft ) {
		backMoveButtonBase->setDefaultSprite( CCSprite::spriteWithSpriteFrameName( "boxRightUp.png" ) ); 
		backMoveButtonBase->setActivatedSprite( CCSprite::spriteWithSpriteFrameName( "boxRightDown.png" ) ); 
		backMoveButtonBase->setPressSprite( CCSprite::spriteWithSpriteFrameName( "boxRightDown.png" ) ); 
		currentBackMoveButtonTextureIsLeft = false;
	}
}

void Scene1UILayer::setJumpDirectionButtonTexture() {
	jumpButtonBase->setDefaultSprite( CCSprite::spriteWithSpriteFrameName( "UpUp.png" ) ); 
	jumpButtonBase->setActivatedSprite( CCSprite::spriteWithSpriteFrameName( "UpUp.png" ) );
	jumpButtonBase->setPressSprite( CCSprite::spriteWithSpriteFrameName( "UpDown.png" ) );
}

void Scene1UILayer::keyBackClicked() {
	showMenu();
}

void Scene1UILayer::keyMenuClicked() {
	showMenu();
}

void Scene1UILayer::showMenu() {
	if( !GameManager::sharedGameManager()->getGamePaused() ) {
		GameManager::sharedGameManager()->setGamePaused( true );
		displayPauseMenu( NULL );
	}
	else {
		this->continueGame( NULL );
	}
}

void Scene1UILayer::continueGame( CCObject* pSender ) {
	CCFiniteTimeAction *hiderFadeOut = CCFadeOut::actionWithDuration( FADE_TIME );
	hider->runAction( hiderFadeOut );

	CCAction* pauseMenuFadeOut = CCSequence::actions( 
			CCFadeOut::actionWithDuration( FADE_TIME ), 
			CCCallFuncN::actionWithTarget( this, callfuncN_selector( Scene1UILayer::continued ) ),
			NULL );
	pauseMenu->runAction( pauseMenuFadeOut );

	GameManager::sharedGameManager()->setGamePaused( false );
}
void Scene1UILayer::turnSFX( CCObject* pSender ) {
	GameManager::sharedGameManager()->setIsSoundEffectsON( !GameManager::sharedGameManager()->getIsSoundEffectsON() );
	PLAYSOUNDEFFECT( TURN_ON );
	sfxLabel->setString( GameManager::sharedGameManager()->getIsSoundEffectsON() ? LABEL_SFX_ON : LABEL_SFX_OFF );
	_CONFIGMAN->settingsUpdated();	
}

void Scene1UILayer::turnMusic( CCObject* pSender ) {
	GameManager::sharedGameManager()->setIsMusicON( !GameManager::sharedGameManager()->getIsMusicON() );
	if( GameManager::sharedGameManager()->getIsMusicON() ) {
		PLAYSOUNDEFFECT( TURN_ON );
		musicLabel->setString( LABEL_MUSIC_ON );
		GameManager::sharedGameManager()->playBackgroundTrack( BACKGROUND_MUSIC1 );
	}
	else {
		musicLabel->setString( LABEL_MUSIC_OFF );
	}

	_CONFIGMAN->settingsUpdated();	
}

void Scene1UILayer::continued( CCNode *pSender ) {
	pauseMenu->setIsVisible( false );
}

void Scene1UILayer::returnToMainMenu( CCObject* pSender ) {
	GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );
}

void Scene1UILayer::displayPauseMenu( CCObject* pSender ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	
	CCFiniteTimeAction *hiderFadeIn = CCFadeIn::actionWithDuration( FADE_TIME );
	hider->runAction( hiderFadeIn );	
	pauseMenu->setOpacity( 0 );
	pauseMenu->setIsVisible( true );
	pauseMenu->setPosition( ccp( screenSize.width / 2, screenSize.height * 0.45f ) );

	CCAction* moveAction = CCSpawn::actions(
			CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * 0.5f, screenSize.height * 0.5f ) ),
			CCFadeIn::actionWithDuration( FADE_TIME ),
			NULL );
	pauseMenu->runAction( moveAction );
}