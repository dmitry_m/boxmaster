#include <math.h>
#include "Scene1ActionLayer.h"
#include "Constants\Constants.h"
#include "Scenes\Scene1\Layers\ScriptListHandler.h"

Scene1ActionLayer::Scene1ActionLayer()
	: debugDraw(NULL), 
	  boxFadeInSoundID(0),
	  boxFadeOutSoundID(0),
	  trophyFadeInSoundId(0),
	  trophyFadeOutSoundId(0),
	  gamePhysicsContactListener(0),
	  fallIndicator(0),
	  floor(0),
	  leftUpperLabel(0),	
	  scoresLabelCaption(0),
	  scoresLabelText(0),
	  boxFadeAnim(0),
	  trophyFadeAnim(0) 
{
}

Scene1ActionLayer::~Scene1ActionLayer( void ) {
	freeClouds();
	if( boxFadeAnim ) {
		boxFadeAnim->release();
	}
	if( trophyFadeAnim ) {
		trophyFadeAnim->release();
	}
	CC_SAFE_DELETE( gamePhysicsContactListener );
	CC_SAFE_DELETE( world );
	CC_SAFE_DELETE( debugDraw );
}

bool Scene1ActionLayer::initWithScene1UILayer( Scene1UILayer *Scene1UILayer ) {
	bool pRet = false;

	if ( CCLayer::init() )
	{
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();		

		srand( time( 0 ) );
		this->setIsTouchEnabled( true );
		this->setIsKeypadEnabled( false );
		GameManager::sharedGameManager()->setScores( 1000l );
		GameManager::sharedGameManager()->setIsRecord( false );
		GameManager::sharedGameManager()->setGamePaused( false );
		uiLayer = Scene1UILayer;
		leftButton = Scene1UILayer->getLeftButton();
		rightButton = Scene1UILayer->getRightButton();
		backMoveButton = Scene1UILayer->getBackMoveButton();
		jumpButton = Scene1UILayer->getJumpButton();
		lastDirection = kRight;
		isGameOver = false;
		actionStopped = false;
		mouseJoint = NULL;
		boxFallSignalCount = 0;
		levelComplexity = GameManager::sharedGameManager()->getGameComplexity();

		for( int x = 0; x < kBoxCanPlaced; x++ ) {
			for( int y = 0; y < kBoxPerCell ; y++ ) {
				field[x][y] = NULL;
			}
		}		
		
		// loading textures for loader
		CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile( LOADER_PLIST );
		sceneSpriteBatchNode = CCSpriteBatchNode::batchNodeWithFile( LOADER_PNG );	
					
		this->setUpWorld();
		this->initAnimations();
		this->initClouds();
		this->createBackground();
		this->createGround();
		this->prepareSpecialBodies();
		this->prepareLabels();
		this->createLoaderAtLocation( C2P( 0,0 ) );

		ScriptListHandler::main(this, scriptList);
		

		int caption = rand() % 3;
		if( caption == 0 ) {
			uiLayer->displayText( LABEL_GO, NULL, NULL, true );
		}
		else if( caption == 1 ) {
			uiLayer->displayText( LABEL_GOOD_LUCK, NULL, NULL, true );
		}
		else if( caption == 2 ) {
			uiLayer->displayText( LABEL_LETS_GO, NULL, NULL, true );
		}

		if( GameManager::sharedGameManager()->getOldScene() != kLevelCompleteScene ) {
			GameManager::sharedGameManager()->playBackgroundTrack( BACKGROUND_MUSIC1 );
		}
		
#if NEED_DF
			CCSprite* fpsDisplay = CCSprite::spriteWithFile( IM_SKY );
			fpsDisplay->setPosition( ccp( fpsDisplay->boundingBox().size.width * FPS_COEFF, fpsDisplay->boundingBox().size.height * FPS_COEFF) );
			this->addChild(fpsDisplay, 200 );
#endif
#if	NEED_DD
		this->setUpDebugDraw();	
#endif
#if NEED_TR
		for( int i = 0; i < 4; i++ ) {
			trophies[i] = NULL;
		}
		this->prepareTrophiesIndicators();
		this->prepareParticlesForBox();
		this->prepareParticlesForGravity();
#endif

		if( levelComplexity == kHard ) {
			randomizeValue = 1.0f;
			strongBoxesValue = 0.5f;
			this->schedule( schedule_selector( Scene1ActionLayer::addBoxAtSpecialPosition ), 1.2f );
		}
		else if( levelComplexity == kMedium ) {
			randomizeValue = 0.75f;
			strongBoxesValue = 0.25f;
			this->schedule( schedule_selector( Scene1ActionLayer::addBoxAtSpecialPosition ), 1.7f );
		} 
		else if( levelComplexity == kEasy ) {
			randomizeValue = 0.5f;
			strongBoxesValue = 0.0f;
			this->schedule( schedule_selector( Scene1ActionLayer::addBoxAtSpecialPosition ), 2.5f );
		}

		//ccBlendFunc ccBF;
		//ccBF.dst = GL_ZERO;
		//ccBF.dst = GL_ZERO;
		//sceneSpriteBatchNode->setBlendFunc( ccBF );
		this->addChild( sceneSpriteBatchNode, -1 );

		this->scheduleUpdate();
		pRet = true;
	}
	return pRet;
}

void Scene1ActionLayer::worldStep( double dt ) {
	int32 velocityIterations = 3;
	int32 positionIterations = 2;
	world->Step( dt,
		velocityIterations, 
		positionIterations );
}

void Scene1ActionLayer::update( ccTime dt ) {
	if( isGameOver || GameManager::sharedGameManager()->getGamePaused() ) {
		return;
	}

	//float lockedTimestepAccumulator = dt;
	float FixedTimeStep = 1.0f / 60.0f;
	worldStep( FixedTimeStep );

	/*
	int count = 0;
	int MaxSteps = 100;
	while( lockedTimestepAccumulator >= FixedTimeStep && count < MaxSteps ) {
		worldStep( FixedTimeStep );
		lockedTimestepAccumulator -= FixedTimeStep;
		count++;		
	}
	char value[SMALL_BUF_LEN];
	sprintf( value, "%d", count );
	leftUpperLabel->setString( value );
	*/
	
//###################################################################################
//###################################################################################

	
// BEFORE SYNCRONIZING ##############################################################
	
	loader->checkSpritePosition();

//###################################################################################
	
	int ticker = updateTimer();
	for ( b2Body *b = world->GetBodyList(); b != NULL; b = b->GetNext() ) {
		Box2DSprite *sprite = ( Box2DSprite * ) b->GetUserData();
		if ( sprite != NULL ) {
			if( sprite->getNeedProcess() ) {
				// when moving
				if( sprite == loader && loader->getCharacterState() == kMoving ) {
						loader->ProcessMoving( dt );
				}

				sprite->setPosition( V2P( b->GetPosition() ) );
				sprite->setRotation( CC_RADIANS_TO_DEGREES( b->GetAngle() *  - 1 ) );
			}
		}
	}

// AFTER SYNCRONIZING ##############################################################

	loader->refreshSensors();	
	loader->setCurrentDirection( checkDirectionButtons() );	

	// Demo
	demoUpdate(dt);

	loader->updateStateWithDeltaTime( dt );
	DispatchBoxFallSounds();

	int every = 4;
	if( ticker % every == 0 ) {
		cloudDispatcher( dt * every );
		refreshBoxes();				
#if NEED_TR
		checkTrophiesPositions();
		checkLoaderForTrophies();
#endif
	}

	if( ( ticker + 1 ) % every == 0 ) {
		updateScores( dt * every );
		checkFallingBoxes( dt * every );				
#if NEED_TR
		checkForLoaderCapture();
		trophyDispatcher( dt * every );
#endif
	}

#if NEED_SS
	ShowSituation();
#endif	
}


void Scene1ActionLayer::demoUpdate(ccTime dt) {
	if (!currentScriptStep) {
		if (scriptList.size()) {
			currentScriptStep = scriptList.front(); 
			scriptList.pop_front();
			currentScriptStep->initHandler();
		}
	}
	else {
		if (currentScriptStep->updateWithDeltaTime(dt)) {
			currentScriptStep.reset();
		}
	}
}

int Scene1ActionLayer::updateTimer() {
	static int count = 0;
	static int to = 16;
	count++;
	if( count == to ) {
		count = 0;	
	}
	return count;
}

// ####################################################################################################
// initialization #####################################################################################
// ####################################################################################################

void Scene1ActionLayer::setUpWorld() {
	b2Vec2 gravity = b2Vec2( 0.0f, -kGravity );
	world = new b2World( gravity );
	world->SetAllowSleeping( true );
	world->SetAutoClearForces( true );

	gamePhysicsContactListener = new GamePhysicsContactListener();
	gamePhysicsContactListener->setLayer( this );
	world->SetContactListener( gamePhysicsContactListener );
	//(b2World*)world->SetContactListener( gamePhysicsContactListener );
}

#if NEED_DD	
	void Scene1ActionLayer::setUpDebugDraw() {
		debugDraw = new GLESDebugDraw( PTM_RATIO * CCDirector::sharedDirector()->getContentScaleFactor() );
		world->SetDebugDraw( debugDraw );
		debugDraw->SetFlags( b2Draw::e_shapeBit );
	}

	void Scene1ActionLayer::draw() {
		glDisable( GL_TEXTURE_2D );
		glDisableClientState( GL_COLOR_ARRAY );
		glDisableClientState( GL_TEXTURE_COORD_ARRAY );

		if ( world ) {
			world->DrawDebugData();
		}

		glEnable( GL_TEXTURE_2D );
		glEnableClientState( GL_COLOR_ARRAY );
		glEnableClientState( GL_TEXTURE_COORD_ARRAY );
	}
#endif

void Scene1ActionLayer::registerWithTouchDispatcher() {
	CCTouchDispatcher::sharedDispatcher()->addStandardDelegate( this, 0 );
}

void Scene1ActionLayer::createBackground() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	
	//CCTexture2D::setDefaultAlphaPixelFormat( kCCTexture2DPixelFormat_RGB5A1 );	

	CCSprite *background = CCSprite::spriteWithFile( IM_BACK );	
	background->setPosition( ccp( screenSize.width/2 , screenSize.height/2 ) );
	this->addChild( background, kBackgroundZValue );


	int firstOffset = screenSize.width / SKY_FIRST_OFFSET;
	int nextOffset = screenSize.width / SKY_BETWEEN_WINDOWS;
	int altOffset = screenSize.height * kSkyAltOffset;

	CCSprite *sky;	
	for( int i = 0; i < 3; i++ ) {		
		sky = CCSprite::spriteWithFile( IM_SKY );
		sky->setPosition( ccp( firstOffset + i * nextOffset, screenSize.height - altOffset ) );
		this->addChild( sky, kSkyZValue );
	}
	
	//CCTexture2D::setDefaultAlphaPixelFormat( kCCTexture2DPixelFormat_Default );

#if NEED_TR
	// fading shield
	fadingShield = CCSprite::spriteWithSpriteFrameName( IM_BIG_SHIELD );	
	fadingShield->setPosition( ccp( -screenSize.width/2 , screenSize.height/2 ) );
	fadingShield->setOpacity( 0 );
	this->addChild( fadingShield, kShieldSpriteZValue );
	
	// arrow
	fadingArrow = CCSprite::spriteWithSpriteFrameName( IM_ARROW );	
	fadingArrow->setPosition( ccp( -screenSize.width/2 , screenSize.height ) );
	fadingArrow->setIsVisible( false );
	arrowBeginSize = fadingArrow->boundingBox().size.height;
	this->addChild( fadingArrow, kArrowSpriteZValue );
#endif
	// fall indicator
	fallIndicator = CCSprite::spriteWithSpriteFrameName( IM_FALL_INDICATOR );
	fallIndicator->setIsVisible( false );
	this->addChild( fallIndicator, kIndicatorSpriteZValue );

	CCSprite *fabricator = CCSprite::spriteWithFile( IM_FABRICATOR );	
	fabricator->setPosition( ccp( screenSize.width/2, kMarginBottom + kAreaHeight +  1.44 *kBoxSide + fabricator->boundingBox().size.height/2 ) );
	this->addChild( fabricator, kFabricatorSpriteZValue );
}

void Scene1ActionLayer::createGround() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();	

	// creating sprite
	Box* box = new Box();
	box->setNeedTexture( false );
	box->setGameObjectType( kGroundType );
	box->init();
	box->setBorder( true );
	box->setNeedProcess( false );
	//box->autorelease();
		
	//Defining prototype for ground
	b2BodyDef groundBodyDef;
	groundBodyDef.type = b2_staticBody;
	groundBodyDef.allowSleep = true;
	groundBodyDef.position.Set( 0, 0 );
	groundBody = world->CreateBody( &groundBodyDef );

	groundBody->SetUserData( box );
	box->setBody( groundBody );

	float tbWidth = kAreaWidth;
	float tbHeight =  screenSize.height / 50;
	float lrWidth =  screenSize.width / 50;
	float lrHeight = kBoxSide * 7;
	
	b2PolygonShape groundShape;
	b2FixtureDef groundFixtureDef;
	groundFixtureDef.shape = &groundShape;
	groundFixtureDef.density = 0.0;
	
	groundFixtureDef.filter.categoryBits = 0xFFFF;
	groundFixtureDef.filter.maskBits = 0xFFFF;
	groundFixtureDef.filter.groupIndex = 4;
	
	b2Vec2 top    = b2Vec2( screenSize.width/2/PTM_RATIO, ( kMarginBottom + kAreaHeight + 1.5 *kBoxSide+ tbHeight/2 )/PTM_RATIO );
	b2Vec2 bottom = b2Vec2( screenSize.width/2/PTM_RATIO, ( kMarginBottom - tbHeight/2 )/PTM_RATIO );

	b2Vec2 left  = b2Vec2( ( kLeftBorder - lrWidth * 0.6 )/PTM_RATIO, (kMarginBottom + lrHeight/2)/PTM_RATIO );
	b2Vec2 right = b2Vec2( ( kRightBorder + lrWidth * 0.6 )/PTM_RATIO, (kMarginBottom + lrHeight/2)/PTM_RATIO );

	//groundShape.SetAsBox( tbWidth/2/PTM_RATIO, tbHeight/2/PTM_RATIO, top, 0 );
	//groundBody->CreateFixture( &groundFixtureDef );
	groundShape.SetAsBox( tbWidth/2/PTM_RATIO, tbHeight/2/PTM_RATIO, bottom, 0 );
	groundBody->CreateFixture( &groundFixtureDef );
	groundShape.SetAsBox( lrWidth/2/PTM_RATIO, lrHeight/2/PTM_RATIO, left, 0 );
	groundBody->CreateFixture( &groundFixtureDef );
	groundShape.SetAsBox( lrWidth/2/PTM_RATIO, lrHeight/2/PTM_RATIO, right, 0 );
	groundBody->CreateFixture( &groundFixtureDef );
	

	floor = CCSprite::spriteWithFile( IM_FLOOR );
	floor->setPosition( ccp( screenSize.width/2, kMarginBottom - floor->boundingBox().size.height/2 ) );
	this->addChild( floor, kFloorAndSidesZValue );

	CCSprite *left_side = CCSprite::spriteWithFile( IM_SIDE );
	left_side->setPosition(  ccp( kLeftBorder - left_side->boundingBox().size.width/2, kMarginBottom + left_side->boundingBox().size.height/2 ) );
	this->addChild( left_side, kFloorAndSidesZValue );

	CCSprite *right_side = CCSprite::spriteWithFile( IM_SIDE );
	right_side->setPosition( ccp( kRightBorder + right_side->boundingBox().size.width/2, kMarginBottom + right_side->boundingBox().size.height/2 ) );
	this->addChild( right_side, kFloorAndSidesZValue );
}

void Scene1ActionLayer::prepareSpecialBodies() {
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.allowSleep = true;
	
	b2FixtureDef fixtureDef;	
	b2PolygonShape shape;
	shape.SetAsBox( ( 2 * kBoxSide + kBoxSeparator)*0.9f/2/PTM_RATIO,kBoxSide/2/PTM_RATIO );	
	fixtureDef.shape = &shape;
	fixtureDef.density = 5.0f;
	fixtureDef.friction = 1.0f;
	fixtureDef.restitution = 0.0f;

	fixtureDef.filter.categoryBits = TROPHY_CATEGORY;
	fixtureDef.filter.maskBits = BOX_CATEGORY;
	fixtureDef.filter.groupIndex = 1;

	// special body
	bodyDef.position = P2V( -100, 100 );

	specialBody = world->CreateBody( &bodyDef );
	specialBody->CreateFixture( &fixtureDef );
	specialBody->GetFixtureList()->SetSensor( false );
}

void Scene1ActionLayer::prepareLabels() { 
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
		/*
		// info label
		leftUpperLabel = CCLabelTTF::labelWithString( "", "Thonburi", 48 );
		leftUpperLabel->setPosition( ccp( screenSize.width*0.1f, screenSize.height*0.89f ) );
		leftUpperLabel->setPosition( ccp( screenSize.width*0.18f, screenSize.height*0.65f ) );
		this->addChild( leftUpperLabel, 1000 );
		*/
		// scores label caption
		scoresLabelCaption = CCLabelBMFont::labelWithString( "SCORES:", kFont );
		scoresLabelCaption->setPosition( ccp( screenSize.width * 0.42f, screenSize.height * 0.12f ) );
		scoresLabelCaption->setScale( 0.6f );
		scoresLabelCaption->setIsVisible( true );
		this->addChild( scoresLabelCaption );

		// scores label text
		scoresLabelText = CCLabelBMFont::labelWithString( "1000", kFont );
		scoresLabelText->setPosition( ccp( screenSize.width * 0.42f, screenSize.height * 0.05f ) );
		scoresLabelText->setScale( 0.6f );
		scoresLabelText->setIsVisible( true );
		this->addChild( scoresLabelText );
}
// ####################################################################################################
// handling buttons ###################################################################################
// ####################################################################################################

DirectionTypes Scene1ActionLayer::checkDirectionButtons() {
	backMoveButtonPressed = false;
	DirectionTypes direction;

	// left
	bool leftKeyPressed = leftButton->getIsActive();
	// right
	bool rightKeyPressed = rightButton->getIsActive();
	// jump
	bool upKeyPressed = jumpButton->getIsActive();
	// back move
	bool bottomKeyPressed = backMoveButton->getIsActive() && backMoveButton->getIsVisible(); 

#if IS_WIN32
	leftKeyPressed = ( GetKeyState( VK_LEFT ) & 0x8000 ) || leftKeyPressed;
	rightKeyPressed = ( GetKeyState( VK_RIGHT ) & 0x8000 ) || rightKeyPressed;
	upKeyPressed = ( GetKeyState( VK_UP ) & 0x8000 ) || upKeyPressed;
	bottomKeyPressed = ( GetKeyState( VK_DOWN ) & 0x8000 ) || bottomKeyPressed; 
#endif 

	loader->setJumpQuery( upKeyPressed );

	if( leftKeyPressed && lastDirection == kLeft ) {
		direction = kLeft;
	}
	else if( rightKeyPressed && lastDirection == kRight ) {
		direction = kRight;
	}
	else if( bottomKeyPressed ) {
		direction = lastDirection;
		backMoveButtonPressed = true;
	}
	else {
		if( leftKeyPressed ) {
			direction = kLeft;
			lastDirection = kLeft;
		}
		else if( rightKeyPressed ) {
			direction = kRight;
			lastDirection = kRight;
		}else {
			direction = kNoDirection;
		}
	}
	return direction;
}

void Scene1ActionLayer::createLoaderAtLocation( CCPoint location ) {
	loader = new Loader();
	loader->autorelease();
	loader->initWithWorld( world, location, this );	
	sceneSpriteBatchNode->addChild( loader, 100, kLoaderSpriteTagValue );
}

void Scene1ActionLayer::setDirButtonIsVisible( DirectionTypes button, bool visible ) {
	if( button == kLeft ) {
		uiLayer->getLeftButtonBase()->setIsVisible( visible );
	}
	else {
		uiLayer->getRightButtonBase()->setIsVisible( visible );
	}
}

void Scene1ActionLayer::setDirButtonTexture( DirectionTypes button, bool directionTexture ) {
	if( button == kLeft ) {
		uiLayer->setLeftDirectionButtonTexture( directionTexture );
	}
	else {
		uiLayer->setRightDirectionButtonTexture( directionTexture );
	}
}

void Scene1ActionLayer::setMoveButtonTexture( DirectionTypes button ) {
	uiLayer->setBackMoveDirectionButtonTexture( button == kLeft ); 
}

void Scene1ActionLayer::setMoveButtonVisible( bool visible ) {
	uiLayer->getBackMoveButtonBase()->setIsVisible( visible );
	uiLayer->getBackMoveButton()->setIsVisible( visible );
}

// ####################################################################################################
// adding boxes and loader ##########################################################################
// ####################################################################################################
void Scene1ActionLayer::addBoxAtSpecialPosition( ccTime dt ) {
	if ( !isGameOver && !GameManager::sharedGameManager()->getGamePaused() ) {
		int randomIndex = rand() % kBoxCanPlaced;
		int maxBoxes = ( rand() % 100 ) < ( randomizeValue * 100 ) ? kBoxPerCell : kBoxPerSpecialCell;	
		int validPlaces[kBoxCanPlacedMax];
		int count = 0;
		for( int i = 0; i < kBoxCanPlaced; i++ ) {
			if( fullness( i ) < maxBoxes ) {
				validPlaces[count++] = i;
			}
			if( randomIndex == count - 1 ) {
				break;
			}
		}	
		if( randomIndex >= count ) {
			randomIndex = rand() % count;
		}
		createBoxAtPlaceWithCoordinatesWithIndicator( validPlaces[ randomIndex ], 0 );
	}
}

void Scene1ActionLayer::addBoxAtRandomPosition( ccTime dt ) {
	if ( !isGameOver && !GameManager::sharedGameManager()->getGamePaused() ) {
		bool enoughPlaces = false;
		for( int i = 0; i < kBoxCanPlaced; i++ ) {
			if( fullness( i ) < kBoxPerCell ) {
				enoughPlaces = true;
				break;
			}
		}
		if( !enoughPlaces ) {
			return;
		}

		// -------------------------------------------------

		// finding place where can add new box
		int placeNumber;
		while( true ) {
			if( fullness( placeNumber = rand() % kBoxCanPlaced ) < kBoxPerCell ) {
				createBoxAtPlaceWithCoordinatesWithIndicator( placeNumber, 0 );
				break;
			}
		}		
	}
	else
	{
		//this->unschedule( schedule_selector( Scene1ActionLayer::addBoxAtRandomPosition ) );
	}
}

void Scene1ActionLayer::addBoxAtRandomPosition2( ccTime dt ) {
	if ( !isGameOver && !GameManager::sharedGameManager()->getGamePaused() ) {
		static int cnt = 0;
		static int x = 3;
		if( cnt < 2 ) {
			createBoxAtPlaceWithCoordinatesWithIndicator( x, 0 );
		}
		else
		{
			cnt = 0;
			x++;
		}
	}
}

void Scene1ActionLayer::addBoxAtFirstPosition( ccTime dt ) {
	if ( !isGameOver && !GameManager::sharedGameManager()->getGamePaused() ) {
		createBoxAtPlaceWithCoordinates( 0, 0 );
	}
}

void Scene1ActionLayer::addBoxesForDebug() { 	
	if ( !isGameOver && !GameManager::sharedGameManager()->getGamePaused() ) {
		for( int i = 1; i < 13; i++ ) {
			createBoxAtPlaceWithCoordinates( i, 0 );
			createBoxAtPlaceWithCoordinates( i, 1 );
		}
		createBoxAtPlaceWithCoordinates( 2, 0 );
	}
}

void Scene1ActionLayer::addBoxesForDebug2() { 	
	if ( !isGameOver && !GameManager::sharedGameManager()->getGamePaused() ) {
		createBoxAtPlaceWithCoordinates( 2, 0 );
	}
}

void Scene1ActionLayer::createBoxAtPlaceWithCoordinatesWithIndicator( int x, int y ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	CCPoint indicatorPosition = C2P( x, y );
	indicatorPosition.y = kMarginBottom + kAreaHeight +  1.6 *kBoxSide + fallIndicator->boundingBox().size.height/2 ;
	fallIndicator->setPosition( indicatorPosition );
	
	coordinatesForBox = ccp( x, y );

	int blinks;
	if( levelComplexity == kHard ) {
		blinks = 2;
	}
	else if( levelComplexity == kMedium ) {
		blinks = 2;
	}
	else {
		blinks = 3;
	}

	fallIndicator->runAction( CCSequence::actions( CCBlink::actionWithDuration( blinks * 0.33f, blinks ),
		CCCallFunc::actionWithTarget( this, callfunc_selector( Scene1ActionLayer::dropBox ) ), 
		CCCallFunc::actionWithTarget( this, callfunc_selector( Scene1ActionLayer::endBlink ) ),
		NULL ) );
}

void Scene1ActionLayer::endBlink() {
	fallIndicator->setIsVisible( false );
}

void Scene1ActionLayer::dropBox() {
	createBoxAtPlaceWithCoordinates( coordinatesForBox.x, coordinatesForBox.y );
}

void Scene1ActionLayer::createBoxAtPlaceWithCoordinates( int x, int y )  {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	// creating sprite

	Box* newBox = new Box();
	newBox->setStrong( ( rand() % 100 ) < ( strongBoxesValue * 100 ) );
	newBox->init();
	newBox->setBoxFadeAnim( boxFadeAnim );
	newBox->autorelease();
	newBox->setPosition( C2P( x, 6 ) );

	// creating body & linking it with sprite
	createBodyAtLocation( C2P( x, 6 ), newBox, BOX_CATEGORY, ( LOADER_CATEGORY | SENSOR_CATEGORY | TROPHY_CATEGORY ), 1, 1.0, 0.0, 5.0 );
	addFallingBox( newBox, x, y );
		
	sceneSpriteBatchNode->addChild( newBox, kBoxSpriteZValue, kBoxSpriteTagValue );
}

void Scene1ActionLayer::createBodyAtLocation( CCPoint location, Box2DSprite *sprite, uint16 category, uint16 mask, int group, float32 friction, float32 restitution, float32 density ) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = P2V( location );
	bodyDef.allowSleep = true;
	b2Body *body = world->CreateBody( &bodyDef );
	
	// linking body with sprite
	body->SetUserData( sprite );
	sprite->setBody( body );

	b2FixtureDef fixtureDef;	
	b2PolygonShape shape;

	shape.SetAsBox( sprite->boundingBox().size.width/2/PTM_RATIO, sprite->boundingBox().size.height/2/PTM_RATIO );	
	fixtureDef.shape = &shape;
	fixtureDef.density = density;
	fixtureDef.friction = friction;
	fixtureDef.restitution = restitution;

	fixtureDef.filter.categoryBits = category;
	fixtureDef.filter.maskBits = mask;
	fixtureDef.filter.groupIndex = group;

	body->CreateFixture( &fixtureDef );
	body->GetFixtureList()->SetSensor( false );
}

void Scene1ActionLayer::initAnimations() {
	// box fade
	if( !boxFadeAnim ) {
		boxFadeAnim = GameCharacter::loadPlistForAnimationWithName( "box_fade", LOADER_ANIM_PLIST );
		if( boxFadeAnim ) {
			boxFadeAnim->retain();
		}
	}

#if NEED_TR
	if( !trophyFadeAnim ) {
		trophyFadeAnim = GameCharacter::loadPlistForAnimationWithName( "trophy_fade", LOADER_ANIM_PLIST );
		if( trophyFadeAnim ) {
			trophyFadeAnim->retain();
		}
	}
#endif
}

// ####################################################################################################
// plug and unplug ... handling #######################################################################
// ####################################################################################################

Box* Scene1ActionLayer::unplugBoxByCoordinates( Coordinates coordinates ) {
	// rewrite
	if( coordinates.y >= 0 && coordinates.y < kBoxPerCell && 
		coordinates.x >= 0 && coordinates.x < kBoxCanPlaced &&
		field[coordinates.x][coordinates.y] ) {

		Box* box = field[coordinates.x][coordinates.y];
		box->setPlugged( false );
		field[coordinates.x][coordinates.y] = 0;	
		return box;
	}
	else {
		return NULL;
	}
}

Box* Scene1ActionLayer::unplugBoxByCoordinates( int x, int y ) {
	// rewrite
	if( y >= 0 && y < kBoxPerCell && 
		x >= 0 && x < kBoxCanPlaced &&
		field[x][y] ) {

		Box* box = field[x][y];
		box->setPlugged( false );
		field[x][y] = 0;	
		return box;
	}
	else {
		return NULL;
	}
}

bool Scene1ActionLayer::plugBoxByCoordinates( Box* boxToAdd, Coordinates coordinates ) {
	if( coordinates.y >= 0 && coordinates.y < kBoxPerCell && 
		coordinates.x >= 0 && coordinates.x < kBoxCanPlaced ) {

		boxToAdd->setPlugged( true );
		field[coordinates.x][coordinates.y] = boxToAdd;	
		return true;
	}
	else {
		return false;
	}
}

bool Scene1ActionLayer::plugBoxByCoordinates( Box* boxToAdd, int x, int y ) {
	if( y >= 0 && y < kBoxPerCell && 
		x >= 0 && x < kBoxCanPlaced ) {

		boxToAdd->setPlugged( true );
		field[x][y] = boxToAdd;	
		return true;
	}
	else {
		return false;
	}
}

Box* Scene1ActionLayer::getBoxByCoordinates( Coordinates coordinates, bool& error ) {
	if( coordinates.y >= 0 && coordinates.y < kBoxPerCell && 
		coordinates.x >= 0 && coordinates.x < kBoxCanPlaced ) {

		error = false;
		return field[coordinates.x][coordinates.y];
	}
	else {
		error = true;
		return NULL;
	}
}

Box* Scene1ActionLayer::getBoxByCoordinates( int x, int y, bool& error ) {
	if( y >= 0 && y < kBoxPerCell && 
		x >= 0 && x < kBoxCanPlaced ) {

		error = false;
		return field[x][y];
	}
	else {
		error = true;
		return NULL;
	}
}


Box* Scene1ActionLayer::getBoxByCoordinates( Coordinates coordinates ) {
	if( coordinates.y >= 0 && coordinates.y < kBoxPerCell && 
		coordinates.x >= 0 && coordinates.x < kBoxCanPlaced ) {

		return field[coordinates.x][coordinates.y];
	}
	else {
		return NULL;
	}
}

Box* Scene1ActionLayer::getBoxByCoordinates( int x, int y ) {
	if( y >= 0 && y < kBoxPerCell && 
		x >= 0 && x < kBoxCanPlaced ) {

		return field[x][y];
	}
	else {
		return NULL;
	}
}

int Scene1ActionLayer::fullness( int index ) {
	int count = 0;
	for( int y = 0; y < kBoxPerCell; y++ ) {
		if( field[index][y] == 0 ) {
			break;
		}
		count++;
	}
	return count;
}

// ####################################################################################################
// ������ ##############################################################################################
// ####################################################################################################

bool Scene1ActionLayer::isReallyEmptyCell( Coordinates coord ) {
	if( getBoxByCoordinates( coord ) != NULL ) {
		return false;
	}
	CCPoint leftUpper = C2P( coord );
	leftUpper.x += -kBoxSide * CHECK_EMPTY_CELL_SIZE;
	leftUpper.y += kBoxSide * CHECK_EMPTY_CELL_SIZE; 

	CCPoint rightUpper = C2P( coord );
	rightUpper.x += kBoxSide * CHECK_EMPTY_CELL_SIZE;
	rightUpper.y += kBoxSide * CHECK_EMPTY_CELL_SIZE; 

	CCPoint rightBottom = C2P( coord );
	rightBottom.x += kBoxSide * CHECK_EMPTY_CELL_SIZE;
	rightBottom.y += -kBoxSide * CHECK_EMPTY_CELL_SIZE; 

	CCPoint leftBottom = C2P( coord );
	leftBottom.x += -kBoxSide * CHECK_EMPTY_CELL_SIZE;
	leftBottom.y += -kBoxSide * CHECK_EMPTY_CELL_SIZE; 

	if( noBoxAtPosition( leftUpper ) && 
		noBoxAtPosition( rightUpper ) &&
		noBoxAtPosition( rightBottom ) &&
		noBoxAtPosition( leftBottom ) ) {
			return true;
	}
	else {
		return false;
	}
}

bool Scene1ActionLayer::noBoxAtPosition( CCPoint pos ) {
	b2Vec2 locationWorld = b2Vec2( pos.x/PTM_RATIO, pos.y/PTM_RATIO );
	b2AABB aabb;
	b2Vec2 delta = b2Vec2(1.0/PTM_RATIO, 1.0/PTM_RATIO);
	aabb.lowerBound = locationWorld - delta;
	aabb.upperBound = locationWorld + delta;

	SimpleQueryCallback callback(locationWorld);
	callback.setObjectToFind( kBoxType );
	world->QueryAABB(&callback, aabb);

	if ( callback.fixtureFound ) {
		return false;
	}
	else {
		return true;
	}
}

void Scene1ActionLayer::alignToCenterOfCell( b2Body *body ) {
	body->SetTransform( C2V( V2C( body->GetPosition() ) ), 0.0f );	
}

void Scene1ActionLayer::alignToCenterOfCellByOx( b2Body *body ) {
	//float y =  body->GetPosition().y;
//	CCPoint position = C2P( V2C( body->GetPosition(), PTM_RATIO ) ) );
//	position.y = y * PTM_RATIO;
	//body->SetTransform( P2V( position), 0.0f );	
}

void Scene1ActionLayer::checkAndClampLoaderSpritePosition() {	
	// checks sensors
	CorrectionTypes result = loader->checkSpritePosition();
}    
	 
// ####################################################################################################
// handling falling boxes #############################################################################
// ####################################################################################################

void Scene1ActionLayer::refreshBoxes() {
	bool performed;
	for( int x = 0; x < kBoxCanPlaced; x++ ) {
		performed = false;
		for( int y = 1; y < kBoxPerCell ; y++ ) {
			if( field[x][y] ) {
				if( field[x][y] != loader->getBoxToMove() && field[x][y]->getPlugged() ) {

					b2Vec2 specialBoxPosition = specialBody->GetPosition();
					Coordinates specialBoxCoordinates = V2C( specialBoxPosition );
					if( !(	specialBoxPosition.x > 0.0f && 
							specialBoxPosition.y > 0.0f &&
							( x == specialBoxCoordinates.x && ( y - 1 ) == specialBoxCoordinates.y ) ) ) {
									
						// nothing at bottom
						if( field[x][y - 1] == 0 ) {
							// fast unplug and adding to falling boxes
							field[x][y]->setPlugged( false );
							addFallingBox( field[x][y], x, 0 );
							field[x][y] = 0;
							break;
						}
					}
				}
			}
		}
	}
}

void Scene1ActionLayer::refreshBoxesAtPlaces( int from, int to, int under ) {	
	/*
	for( auto col = field.begin(); col != field.end(); col++ ) {
		int index = col - field.begin();
		if( from <= index && index <= to && col->size() > 0 ) {
			for( auto element = col->begin(); element != col->end(); element++ ) {
				int alt = element - col->begin();
				if( *element != 0 && alt >= under ) {
					Box* boxForFall = *element;
					addFallingBox( boxForFall, col - field.begin() , element - col->begin() );
					*element = 0;
				}
			}
		}
	}*/
}

int Scene1ActionLayer::howManyBoxes() {
	int boxesCount = 0;
	
	for( int x = 0; x < kBoxCanPlaced; x++ ) {
		for( int y = 0; y < kBoxPerCell ; y++ ) {
			if( field[x][y] ) {
				boxesCount++;
			}
		}
	}
	return boxesCount;
}

void Scene1ActionLayer::addFallingBox( Box *box, int x, int y ) {
	box->getBody()->SetType( b2_dynamicBody );
	box->setBodyLastPosition( b2Vec2( -10.0f, -10.0f ) );
	box->getBody()->SetLinearVelocity( b2Vec2( 0.0f, -0.1f ) );
	fallingBoxes.push_back( FallingInfo( box, x, y ) );
}
	
void Scene1ActionLayer::checkFallingBoxes( ccTime dt ) {

	vector< FallingInfo >::iterator iter = fallingBoxes.begin();
	CCRect adjRect;
	for( iter; iter != fallingBoxes.end(); ) {
		Box* sprite = ( Box* )iter->getObject();
		if( !sprite->getNeedProcess() ) {
			continue;
		}
		adjRect = sprite->adjustedBoundingBox();
		

		// falling on loader
		if ( CCRect::CCRectIntersectsRect( adjRect, loader->adjustedBoundingBox() ) &&
			sprite->getPosition().y >= loader->getPosition().y + loader->boundingBox().size.height/2 ) {

			checkForServive( (Box*) sprite );
			return;
		}

		float delta = adjRect.size.height * fabs( sprite->getBody()->GetLinearVelocity().y ) * 0.08f;
		adjRect.origin.y -= delta;
		adjRect.size.height += delta;

#if NEED_TR
		for( int i = 0; i < 4; i++ ) {
			if( trophies[i] ) {
				if ( CCRect::CCRectIntersectsRect( adjRect, trophies[i]->trophyBoundingBox() ) &&
					sprite->getPosition().y > trophies[i]->getPosition().y + trophies[i]->boundingBox().size.height/2 ) {
					// destroy trophy
					queryDeleteTrophy( trophies[i], true, true );
					trophies[i] = NULL;
					return;
				}
			}
		}
#endif

		b2Body* body = sprite->getBody();
		b2Vec2 bodyDestinationPosition( C2V( iter->getX(), 0 ) );
		b2Vec2 bodyCurrentPosition = body->GetPosition();

		// align if not
		body->SetTransform( b2Vec2( bodyDestinationPosition.x, bodyCurrentPosition.y ), 0.0f );

		// if box fell
		//if( bodyCurrentPosition.y < bodyDestinationPosition.y + 10.0f/PTM_RATIO ) {
		if( dt != 0.0 &&
			fabs( bodyCurrentPosition.y - sprite->getBodyLastPosition().y ) < 0.005f ) {
			if( plugBoxByCoordinates( sprite, V2C( body->GetPosition() ) ) ) {	
				AddBoxFallSound();

				alignToCenterOfCell( body );
				body->SetType( b2_staticBody );
			
				iter = fallingBoxes.erase( iter );
				sprite->setFirstFall( false );


				// checks for first line
				bool enough = true;
				for ( int x = 0; x < kBoxCanPlaced; x++ ) {
					if( field[x][0] == 0 ) {
						enough = false;
						break;
					}
				}
				if( enough ) {
					deleteFirstLine();
					break;
				}
			}
			else {
				iter++;
			}
		}		
		else {
			iter++;
		}
		sprite->setBodyLastPosition( bodyCurrentPosition );
	}
}

// ####################################################################################################
// game changes #######################################################################################
// ####################################################################################################

void Scene1ActionLayer::deleteBox( CCNode* box ) {
	box->removeFromParentAndCleanup( true );
}

void Scene1ActionLayer::deleteFirstLine() {
	for ( int x = 0; x < kBoxCanPlaced; x++ ) {
		this->queryDeleteBox( field[x][0], false, false );
	}
	
	playBoxFadeOutSound();
	int scores = GameManager::sharedGameManager()->getScores();
	scores += 1000;
	updateScoreLabel( scores );
	GameManager::sharedGameManager()->setScores( scores );	
}

void Scene1ActionLayer::gameOverWithWin( bool win ) {
	isGameOver = true;
	this->stopClouds();
	this->stopAllActions();
	loader->stopAllActions();
	if( loader->getBoxToMove() ) {
		loader->getBoxToMove()->stopAllActions();
	}
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	
	CCSprite *newBackground = CCSprite::spriteWithFile( IM_MENU_BACK );	
	newBackground->setPosition( ccp( screenSize.width/2 , screenSize.height/2 ) );
	newBackground->setOpacity( 0 );
	uiLayer->addChild( newBackground, kGameOverBackZValue );

	CCFiniteTimeAction *fadeIn = CCSequence::actions( 
		CCActionInterval::actionWithDuration( FADE_TIME ), 
		CCFadeIn::actionWithDuration( FADE_TIME ), 
		CCCallFuncN::actionWithTarget( this, callfuncN_selector( Scene1ActionLayer::exitScene ) ),
		NULL );
	newBackground->runAction( fadeIn );
}

void Scene1ActionLayer::updateScores( ccTime deltaTime ) {
	static float timeElapsed = 0.0f;
	int oldScores = GameManager::sharedGameManager()->getScores();
	if( timeElapsed > 2.0f ) {
		int scores = oldScores;
		scores -= howManyBoxes();
		if( scores < 0 ) {
			scores = 0;
		}
		if( scores > 2000000000 ) {
			scores = 2000000000;
		}
		GameManager::sharedGameManager()->setScores( scores );
		if( scores != oldScores ) {
			updateScoreLabel( scores );
		}
		timeElapsed = 0.0f;
	}
	else {
		timeElapsed += deltaTime;
	}	
}

void Scene1ActionLayer::updateScoreLabel( int newScores ) {	
	char scoresStr[16];
	sprintf( scoresStr, "%ld", newScores );
	this->scoresLabelText->setString( scoresStr );
}

void Scene1ActionLayer::exitScene( CCNode *pSender ) {
	GameManager::sharedGameManager()->runSceneWithID( kLevelCompleteScene );
}

// ####################################################################################################
// clouds handling ####################################################################################
// ####################################################################################################

void Scene1ActionLayer::initClouds() {
	clouds.clear();
	for( int i = 0; i < CLOUDS_COUNT; i++ ) {
		Cloud* newCloud(new Cloud());
		newCloud->init();	
		clouds.push_back(newCloud);
		this->addChild(newCloud, kCloudZValue);
	}
}

void Scene1ActionLayer::freeClouds() {/*
	for( vector< Cloud* >::iterator cloud = clouds.begin(); cloud != clouds.end(); cloud++ ) {
		if( ( *cloud ) ) {
			( *cloud )->release();
		}
	}*/
}

void Scene1ActionLayer::stopClouds() {	
	for (auto cloud : clouds) {
		cloud->stopAllActions();
	}
}

void Scene1ActionLayer::pauseClouds() {	
	for (auto cloud : clouds) {
		cloud->pauseSchedulerAndActions();
	}
}

void Scene1ActionLayer::resumeClouds() {	
	for (auto cloud : clouds) {
		cloud->resumeSchedulerAndActions();
	}
}

void Scene1ActionLayer::cloudDispatcher( ccTime dt ) {	
	static double funcTimer= 0;

	// checks every 0.5*second
	if( funcTimer < CLOUD_DISPATCHER_TIME ) {		
		funcTimer += dt;
		return;
	}
	else {
		funcTimer = 0.0f;
	}
	
	// checks all clouds
	for (auto cloud : clouds) {
		if( cloud->NeedReposition() ) {
			cloud->AlignToBegin();
			cloud->Fly();
			break;
		}
	}
}

// ####################################################################################################
// for debug ##########################################################################################
// ####################################################################################################

void Scene1ActionLayer::ShowSituation() {
	cocos2d::CCLog( "-------------------------------" );
	for( int row = kBoxPerCell - 1 ; row >= 0; row-- ) {
		string formatStr;
		for( int col = 0; col < kBoxCanPlaced; col++ ) {
			bool error = false;
			formatStr += getBoxByCoordinates( Coordinates( col, row ), error ) == 0 ? "0 " : "1 ";				
		}
		cocos2d::CCLog( formatStr.c_str() );
	}
}

// ####################################################################################################
// not for free #######################################################################################
// ####################################################################################################


void Scene1ActionLayer::checkForServive( Box* boxCandidate ) {
#if NEED_TR
	if( loader->getTrophy( kShield ) ) {
		if( boxCandidate ) {
			shieldFade( loader->getPosition() );
			queryDeleteBox( boxCandidate, true, true );
			loader->setTrophy( kShield, false );
		}
	}
	else {
#endif
		gameOverWithWin( false );
#if NEED_TR
	}
#endif
}

void Scene1ActionLayer::pauseLayer( bool pause ) {
	if( pause ) {
		this->pauseClouds();
		this->pauseSchedulerAndActions();
		this->fallIndicator->pauseSchedulerAndActions();
		loader->pauseSchedulerAndActions();
		if( loader->getBoxToMove() ) {
			loader->getBoxToMove()->pauseSchedulerAndActions();
		}
	} 
	else {		
		this->resumeClouds();
		this->resumeSchedulerAndActions();
		this->fallIndicator->resumeSchedulerAndActions();
		loader->resumeSchedulerAndActions();
		if( loader->getBoxToMove() ) {
			loader->getBoxToMove()->resumeSchedulerAndActions();
		}
	}
}

void Scene1ActionLayer::keyBackClicked() {
	if( isGameOver ) {
		return;
	}

	pauseLayer( true );
	uiLayer->showMenu();
}

void Scene1ActionLayer::keyMenuClicked() {
	if( isGameOver ) {
		return;
	}
	pauseLayer( true );
	uiLayer->showMenu();
}

void Scene1ActionLayer::queryDeleteBox( Box* boxToDelete, bool withParticles, bool withSound ) {
	if( isGameOver ) {
		return;
	}
	boxToDelete->setNeedProcess( false );
	b2Body* body = boxToDelete->getBody();			
	// deleting body
	body->GetWorld()->DestroyBody( body );
		
	if( boxToDelete->getPlugged() ) {
		unplugBoxByCoordinates( P2C( boxToDelete->getPosition() ) );
	}
	else {
		// delete from falling boxes
		for( vector< FallingInfo >::iterator iter = fallingBoxes.begin(); iter != fallingBoxes.end(); iter++ ) {
			if( iter->getObject() == boxToDelete ) {
				fallingBoxes.erase( iter );
				break;
			}
		}
	}
#if NEED_TR
	if( withParticles ) {
		showBoxParticles( boxToDelete->getPosition() );
	}
#endif
	if( withSound ) {
		playBoxFadeOutSound();
	}

	boxToDelete->runAction( CCSequence::actions( CCAnimate::actionWithAnimation( boxToDelete->getBoxFadeAnim(), false ), 
		CCCallFuncN::actionWithTarget( this, callfuncN_selector( Scene1ActionLayer::deleteBox ) ),
		NULL ) );
}


// MUSIC

void Scene1ActionLayer::AddBoxFallSound() {
	boxFallSignalCount++;
}

void Scene1ActionLayer::DispatchBoxFallSounds() {
	for( int i = 0; i < boxFallSignalCount && i < 5; i++ ) {
		playBoxFallSound();
	}
	boxFallSignalCount = 0;
}

void Scene1ActionLayer::playBoxFadeInSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( boxFadeInSoundID );
	boxFadeInSoundID = PLAYSOUNDEFFECT( BOX_FADE_IN1 );
#endif
}

void Scene1ActionLayer::playBoxFadeOutSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( boxFadeOutSoundID );
	boxFadeOutSoundID = PLAYSOUNDEFFECT( BOX_FADE_OUT1 );
#endif
}

void Scene1ActionLayer::playTrophyFadeInSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( trophyFadeInSoundId );
	trophyFadeInSoundId = PLAYSOUNDEFFECT( TROPHY_FADE_IN1 );
#endif
}

void Scene1ActionLayer::playTrophyFadeOutSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( trophyFadeOutSoundId );
	trophyFadeOutSoundId = PLAYSOUNDEFFECT( TROPHY_FADE_OUT1 );
#endif
}

void Scene1ActionLayer::playBoxFallSound() {
#if NEED_EM
	PLAYSOUNDEFFECT( BOX_FALL1 );
#endif
}

void Scene1ActionLayer::playAllBoxFallSound() {
#if NEED_EM
	PLAYSOUNDEFFECT( BOX_FALL_ALL );
#endif
}

void Scene1ActionLayer::playSequenceSound() {
#if NEED_EM
	PLAYSOUNDEFFECT( SEQUENCE );
#endif
}

void Scene1ActionLayer::playSequence2Sound() {
#if NEED_EM
	PLAYSOUNDEFFECT( SEQUENCE2 );
#endif
}

// ####################################################################################################
// trophies ###########################################################################################
// ####################################################################################################

#if NEED_TR

void Scene1ActionLayer::prepareTrophiesIndicators() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	
	destroyBoxIndicator = CCSprite::spriteWithSpriteFrameName( IM_DESTROY_BOX );
	destroyBoxIndicator->setIsVisible( false );
	this->addChild( destroyBoxIndicator );
	lowGravityIndicator = CCSprite::spriteWithSpriteFrameName( IM_LOW_GRAVITY );
	lowGravityIndicator->setIsVisible( false );
	this->addChild( lowGravityIndicator );
	teleportIndicator = CCSprite::spriteWithSpriteFrameName( IM_TELEPORT );
	teleportIndicator->setIsVisible( false );
	this->addChild( teleportIndicator );
	shieldIndicator = CCSprite::spriteWithSpriteFrameName( IM_SHIELD );
	shieldIndicator->setIsVisible( false );
	this->addChild( shieldIndicator );

	float spriteWidth = destroyBoxIndicator->boundingBox().size.width;
	float spriteHeight = destroyBoxIndicator->boundingBox().size.height;
	
	int floorHeight = floor ? floor->boundingBox().size.height : 0;
	float margin = ( kMarginBottom - floorHeight - 2 * spriteHeight ) / 3.0f;
	CCPoint leftCorner = ccp( screenSize.width * TROPHIES_INDICATORS_X_OFFSET, margin );



	teleportIndicator->setPosition( ccp( leftCorner.x + spriteWidth * 0.5f, leftCorner.y + spriteHeight * 0.5f ) );
	shieldIndicator->setPosition( ccp( leftCorner.x + spriteWidth * 1.5f + margin, leftCorner.y + spriteHeight * 0.5f ) );	
	destroyBoxIndicator->setPosition( ccp( leftCorner.x + spriteWidth * 0.5f, leftCorner.y + spriteHeight * 1.5f + margin ) );
	lowGravityIndicator->setPosition( ccp( leftCorner.x + spriteWidth * 1.5f + margin, leftCorner.y + spriteHeight * 1.5f + margin ) );
}

void Scene1ActionLayer::checkForLoaderCapture() {
	if( arrowMode ) {
		return;
	}

	if( loader->getTrophy( kTeleport ) && 
		loader->getCharacterState() != kMoving && 
		loader->getCharacterState() != kWaiting ) {

		b2Vec2 locationWorld = b2Vec2(currentPos.x/PTM_RATIO, currentPos.y/PTM_RATIO);
		b2AABB aabb;
		b2Vec2 delta = b2Vec2(1.0/PTM_RATIO, 1.0/PTM_RATIO);
		aabb.lowerBound = locationWorld - delta;
		aabb.upperBound = locationWorld + delta;

		SimpleQueryCallback callback(locationWorld);
		callback.setObjectToFind( kLoaderType );
		world->QueryAABB(&callback, aabb);

		if ( callback.fixtureFound ) {
			arrowMode = true;
			arrowBegin = arrowEnd = currentPos;
			return;
		}
	}
}

void Scene1ActionLayer::deleteTrophy( CCNode* trophy ) {
	trophy->removeFromParentAndCleanup( true );
}

void Scene1ActionLayer::queryDeleteTrophy( Trophy* trophy, bool withParticles, bool withSound ) {
	if( isGameOver ) {
		return;
	}


	b2Body* bodyToDelete = trophy->getBody();
	bodyToDelete->GetWorld()->DestroyBody( bodyToDelete );

	if( withParticles ) {
		showBoxParticles( trophy->getPosition() );
	}
	if( withSound ) {
		playTrophyFadeOutSound();
	}

	trophy->runAction( CCSequence::actions( CCAnimate::actionWithAnimation( trophy->getTrophyFadeAnim(), false ), 
		CCCallFuncN::actionWithTarget( this, callfuncN_selector( Scene1ActionLayer::deleteTrophy ) ),
		NULL ) );
}

void Scene1ActionLayer::shieldFade( CCPoint position ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	
	fadingShield->setOpacity( 0 );
	fadingShield->setScale( 0.4f );
	fadingShield->setPosition( position );
	fadingShield->runAction( CCSequence::actions(
		CCSpawn::actions( 
			CCScaleTo::actionWithDuration( 0.3f, 1.0f, 1.0f ),
			CCFadeIn::actionWithDuration( 0.3f ),
			NULL ),
		CCSpawn::actions( 
			CCScaleTo::actionWithDuration( 0.1f, 0.9f, 0.9f ),
			CCFadeOut::actionWithDuration( 0.1f ),
			NULL ),
		CCMoveTo::actionWithDuration( 0.1, ccp( -screenSize.width/2 , screenSize.height/2 ) ),
		NULL ) );
}

void Scene1ActionLayer::turnOffLowGravity() {
	if( isGameOver ) {
		return;
	}
	world->SetGravity( b2Vec2( 0.0f, -kGravity ) );
	loader->setTrophy( kLowGravity, false );
}

void Scene1ActionLayer::ccTouchesBegan( CCSet *pTouches, CCEvent *pEvent ) {
	if( isGameOver ) {
		return;
	}
	touch = true;	
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	arrowMode = false;
	touchBeginAt = ccp( 0.0f, screenSize.height );
	CCTouch *touch = ( CCTouch * ) pTouches->anyObject();
	CCPoint touchLocation = touch->locationInView();
	touchLocation = CCDirector::sharedDirector()->convertToGL(touchLocation);
	touchLocation = this->convertToNodeSpace(touchLocation);
	currentPos = touchLocation;

	if ( loader->getTrophy( kDestroyBox ) && 
		loader->getCharacterState() != kMoving && 
		loader->getCharacterState() != kWaiting ) {
		//for( CCSetIterator each = touches->begin(); each != touches->end(); each++ ) {
			/*
			CCTouch *touch = ( CCTouch * ) ( *each );
			CCPoint touchLocation = touch->locationInView(touch->view());
			touchLocation = CCDirector::sharedDirector()->convertToGL(touchLocation);
			touchLocation = this->convertToNodeSpace(touchLocation);
			*/
			b2Vec2 locationWorld = b2Vec2(touchLocation.x/PTM_RATIO, touchLocation.y/PTM_RATIO);
			b2AABB aabb;
			b2Vec2 delta = b2Vec2(1.0/PTM_RATIO, 1.0/PTM_RATIO);
			aabb.lowerBound = locationWorld - delta;
			aabb.upperBound = locationWorld + delta;

			SimpleQueryCallback callback(locationWorld);
			callback.setObjectToFind( kBoxType );
			world->QueryAABB(&callback, aabb);

			if ( callback.fixtureFound ) {
				b2Body *body = callback.fixtureFound->GetBody();
				queryDeleteBox( (Box*) body->GetUserData(), true, true );
				loader->setTrophy( kDestroyBox, false );
				return;
			}
	}

	touchBeginAt = touchLocation;

#if IS_WIN32
	//uiLayer->showMenu();
#endif
}

void Scene1ActionLayer::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent) {
	if( isGameOver ) {
		return;
	}

	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	CCTouch* touch = (CCTouch*)pTouches->anyObject();
	CCPoint touchLocation = touch->locationInView();
	touchLocation = CCDirector::sharedDirector()->convertToGL(touchLocation);
	touchLocation = this->convertToNodeSpace(touchLocation);
	currentPos = touchLocation;

	if( !arrowMode ) {
		float gypotenuse = sqrt (
			( touchLocation.x - touchBeginAt.x ) * ( touchLocation.x - touchBeginAt.x ) +
			( touchLocation.y - touchBeginAt.y ) * ( touchLocation.y - touchBeginAt.y ) );

		float k = 0.0f;
		if(  ( touchLocation.x - touchBeginAt.x ) == 0 ) {
			k = 100.0f;
		}
		else {
			k = fabs( ( touchLocation.y - touchBeginAt.y ) / ( touchLocation.x - touchBeginAt.x ) );
		}

		if( loader->getTrophy( kLowGravity ) &&
			gypotenuse > 0.4f * screenSize.height && 
			k > 4.0f  &&
			touchLocation.y > touchBeginAt.y &&
			world->GetGravity().y < -kGravity * 0.8f ) {

			world->SetGravity( b2Vec2( 0.0f, -kGravity*0.70f ) );
			showGravityParticles();
			lowGravityIndicator->runAction( CCBlink::actionWithDuration( LOW_GRAVITY_DURATION, (int)LOW_GRAVITY_DURATION ) );
			this->runAction( CCSequence::actions( CCActionInterval::actionWithDuration( LOW_GRAVITY_DURATION ),
				CCCallFunc::actionWithTarget( this, callfunc_selector( Scene1ActionLayer::turnOffLowGravity ) ),
				NULL ) );
		}
	}
	else {
		GameManager::sharedGameManager()->setGamePaused( true );		
		pauseLayer( true );

		arrowBegin = loader->getPosition();
		arrowEnd = touchLocation;

		fadingArrow->setIsVisible( true );
		fadingArrow->setPosition( ccp( (arrowEnd.x + arrowBegin.x)/2, (arrowEnd.y + arrowBegin.y)/2 ) );

		float gypotenuse = sqrt( (arrowEnd.x - arrowBegin.x) * (arrowEnd.x - arrowBegin.x) +
			(arrowEnd.y - arrowBegin.y) * (arrowEnd.y - arrowBegin.y) );
		float angle = acos(  (arrowEnd.x - arrowBegin.x) / gypotenuse ) * 180.0f / M_PI;
		if( arrowEnd.y > arrowBegin.y ) {
			angle *= -1;
		}
		fadingArrow->setRotation( angle + 90 );
		float arrowScale = gypotenuse / arrowBeginSize;

		fadingArrow->setScaleY( arrowScale );
	}
}

void Scene1ActionLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent) {
	if( isGameOver ) {
		return;
	}
	
	touch = false;

	if( arrowMode ) {
		StartTeleport();
	}
}

void Scene1ActionLayer::StartTeleport() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	fadingArrow->setPosition( ccp( -screenSize.width/2 , screenSize.height ) );
	fadingArrow->setIsVisible( false );
	arrowMode = false;

	// will teleport
	if( !getBoxByCoordinates( P2C( arrowEnd ) ) &&
		arrowEnd.x > kLeftBorder && arrowEnd.x < kRightBorder && 
		arrowEnd.y > kMarginBottom && arrowEnd.y < kMarginBottom +  ( kBoxPerCell + 1.5 ) *kBoxSide ) {

		loader->setTrophy( kTeleport, false );
		
		playSequenceSound(); 

		// moving
		double length = sqrt( pow( arrowEnd.x - arrowBegin.x, 2 ) + pow( arrowEnd.y - arrowBegin.y, 2 ) );
		double duration = 0.1 + length * 0.0005;

		CCFiniteTimeAction *moveToEndPosition = CCSequence::actions(
			CCSpawn::actions( 
				CCMoveTo::actionWithDuration( duration, arrowEnd ),
				CCAnimate::actionWithAnimation( loader->getJumpingStartAnim(), false ),
				NULL ),
			CCCallFunc::actionWithTarget( this, callfunc_selector( Scene1ActionLayer::EndTeleport ) ),
			NULL );
		
		loader->runAction( moveToEndPosition );		
	}
	else {
		EndTeleport();
	}
}

void Scene1ActionLayer::EndTeleport() {
	// teleported
	if( !loader->getTrophy( kTeleport ) ) {
		//loader->setAnim( 0, true );
		loader->getBody()->SetTransform( P2V( arrowEnd ), 0.0 );
		loader->getBody()->SetLinearVelocity( b2Vec2( 0.0, 0.0 ) );
		this->alignToCenterOfCell( loader->getBody() );
	}
	GameManager::sharedGameManager()->setGamePaused( false );		
	pauseLayer( false );	
}

void Scene1ActionLayer::showBoxParticles( CCPoint position ) {
	if( isGameOver ) {
		return;
	}

	boxEmitter->resetSystem();	
	boxEmitter->setPosition( position );
	playSequence2Sound();
}

void Scene1ActionLayer::showGravityParticles() {
	if( isGameOver ) {
		return;
	}

	gravityEmitter->resetSystem();	
}

void Scene1ActionLayer::prepareParticlesForBox() {
	boxEmitter = new CCParticleSystemQuad();
	boxEmitter->initWithTotalParticles(20);
	boxEmitter->autorelease();

	boxEmitter->stopSystem();
	boxEmitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( IM_PARTICLE ) );
	boxEmitter->setDuration( 0.3 );
	boxEmitter->setGravity( ccp( 0.0f, 0.0f ) );
	boxEmitter->setAngleVar( 360 );
	boxEmitter->setSpeed( BOX_EMITTER_SPEED );
	boxEmitter->setSpeedVar( 20 );
	boxEmitter->setPosVar(CCPointZero);
	boxEmitter->setLife( BOX_EMITTER_LIFE );
	boxEmitter->setLifeVar( 0.15 );


	ccColor4F startColor = {0.0f, 0.0f, 1.0f, 1.0f};
	boxEmitter->setStartColor( startColor );
	ccColor4F startColorVar = {0.0f, 0.0f, 0.0f, 0.0f};
	boxEmitter->setStartColorVar(startColorVar);
	ccColor4F endColor = {0.0f, 0.0f, 1.0f, 0.5f};
	boxEmitter->setEndColor(endColor);
	ccColor4F endColorVar = {0.0f, 0.0f, 0.0f, 0.0f};
	boxEmitter->setEndColorVar(endColorVar);

	boxEmitter->setStartSize( BOX_EMITTER_SIZE );
	boxEmitter->setStartSizeVar( BOX_EMITTER_SIZE_VAR );
	boxEmitter->setEndSize(kParticleStartSizeEqualToEndSize);

	boxEmitter->setEmissionRate(5*boxEmitter->getTotalParticles()/boxEmitter->getLife());	
	boxEmitter->setIsBlendAdditive(false);
	boxEmitter->setPosition( 100, 200 );
	this->addChild( boxEmitter, kBoxParticlesZValue );

}

void Scene1ActionLayer::prepareParticlesForGravity() {		
	gravityEmitter = CCParticleSnow::node();
	gravityEmitter->stopSystem();

	gravityEmitter->setTexture( CCTextureCache::sharedTextureCache()->addImage( IM_PARTICLE ) );
	CCPoint p = gravityEmitter->getPosition();
	gravityEmitter->setDuration( LOW_GRAVITY_DURATION );
	gravityEmitter->setPosition( CCPointMake( p.x, p.y+110) );
	gravityEmitter->setLife(LOW_GRAVITY_DURATION);
	gravityEmitter->setLifeVar(5);
	gravityEmitter->setSpeed( GRAVITY_EMITTER_SPEED );
	gravityEmitter->setSpeedVar(30);
	gravityEmitter->setGravity( ccp( 0.0f, 0.0f ) ); 	
	
	ccColor4F startColor = {0.0f, 0.0f, 1.0f, 1.0f};
	gravityEmitter->setStartColor( startColor );
	ccColor4F startColorVar = {0.0f, 0.0f, 0.0f, 0.0f};
	gravityEmitter->setStartColorVar(startColorVar);

	gravityEmitter->setStartSize( GRAVITY_EMITTER_SIZE );
	gravityEmitter->setStartSizeVar( GRAVITY_EMITTER_SIZE_VAR );
	gravityEmitter->setEndSize(kParticleStartSizeEqualToEndSize);	
	
	gravityEmitter->setEmissionRate(gravityEmitter->getTotalParticles()/gravityEmitter->getLife());
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();	
	gravityEmitter->setIsBlendAdditive(false);
	gravityEmitter->setPosition( ccp( screenSize.width / 2, kMarginBottom ) );	
	this->addChild( gravityEmitter, kGravityParticlesZValue );
}


void Scene1ActionLayer::checkTrophiesPositions() {
	if( isGameOver ) {
		return;
	}

	for( int i = 0; i < 4; i++ ) {
		if( !trophies[i] ) {
			continue;
		}

		CCPoint curPos = trophies[i]->getPosition();
		
		if( curPos.y >= kMarginBottom + kAreaHeight + 1.0 *kBoxSide ) {
			curPos.y = kMarginBottom + kAreaHeight + 1.0 *kBoxSide;
			trophies[i]->getBody()->SetTransform( P2V( curPos ), 0.0f );
			trophies[i]->getBody()->SetLinearVelocity( b2Vec2( 0.0f, -0.2f ) );
		}

		if( curPos.x < kLeftBorder ) {
			curPos.x = kLeftBorder;
			trophies[i]->getBody()->SetTransform( P2V( curPos ), 0.0f );
		}
		else if( curPos.x > kRightBorder ) {
			curPos.x = kRightBorder;
			trophies[i]->getBody()->SetTransform( P2V( curPos ), 0.0f );
		}

		bool error = false;
		if( getBoxByCoordinates( P2C( curPos ), error ) ) {
			this->queryDeleteTrophy( trophies[i], true, true );
			trophies[i] = NULL;
		}
	}
}

void Scene1ActionLayer::checkLoaderForTrophies() {
	if( isGameOver ) {
		return;
	}
	
	for( int i = 0; i < 4; i++ ) {
		if( trophies[i] ) {
			if ( CCRect::CCRectIntersectsRect( loader->adjustedBoundingBox(), trophies[i]->trophyBoundingBox() ) ) {
				loader->setTrophy( (TrophyTypes)i, true );
				// pick trophy
				this->queryDeleteTrophy( trophies[i], true, false );
				trophies[i] = NULL;
				return;
			}
		}
	}
}

void Scene1ActionLayer::trophyDispatcher( ccTime dt ) {
	if( isGameOver ) {
		return;
	}

	const int trophyProbability[] = { TROPHY_DESTROY * 100.0f, TROPHY_GRAVITY * 100.0f, TROPHY_TELEPORT * 100.0f, TROPHY_SHIELD * 100.0f };
	static const float every = 1.0f;
	static float funcTime1 = 0.0f;

	if ( !isGameOver && !GameManager::sharedGameManager()->getGamePaused() ) {

		funcTime1 += dt;
		if( funcTime1 < every ) {
			return;
		}
		else {
			funcTime1 = 0.0f;
		}

		for( int i = 0; i < 4; i++ ) {
			bool success = ( rand() % 100 ) < trophyProbability[i];
			if( !trophies[i] && 
				!loader->getTrophy( (TrophyTypes)i ) 
				&& success ) {

				trophies[i] = createTrophyAtPlaceWithCoordinates( rand() % kBoxCanPlaced, (TrophyTypes)i );
			}
		}
	}
}

Trophy* Scene1ActionLayer::createTrophyAtPlaceWithCoordinates( int x, TrophyTypes type ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	// creating sprite
	Trophy* newTrophy = new Trophy();
	newTrophy->init();
	newTrophy->setTrophyFadeAnim( trophyFadeAnim );
	newTrophy->setType( type );
	newTrophy->autorelease();
	newTrophy->setPosition( C2P( x, 6 ) );

	// creating body
	// linking it with sprite
	createBodyAtLocation( C2P( x, 6 ), newTrophy, TROPHY_CATEGORY, BOX_CATEGORY, 9, 1.0, 0.25, 0.5 );
	int sign = ( rand() % 2 ) == 0 ? 1 : -1; 
	int angularVelocity = sign * ( ( rand() % 10 ) + 10 );
	newTrophy->getBody()->SetLinearVelocity( P2V( 0.0f, 0.0f ) );
	newTrophy->getBody()->SetAngularVelocity( angularVelocity );
		
	sceneSpriteBatchNode->addChild( newTrophy, kTrophySpriteZValue, kTrophySpriteTagValue );
	return newTrophy;
}

#endif
