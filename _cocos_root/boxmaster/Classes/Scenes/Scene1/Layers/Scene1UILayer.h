#ifndef SCENE1UILAYER_H
#define SCENE1UILAYER_H

#include "Constants\Constants.h"
#include "SneakyJoystick\SneakyButton.h"
#include "SneakyJoystick\SneakyButtonSkinnedBase.h"
#include "Singletons\GameManager.h"

using namespace cocos2d;

class Scene1UILayer : public CCLayer
{
protected:
	CCSprite *hider;
	//CCSprite* klax;
	//float klaxWidth;

	bool inverted;
	
	CCLabelBMFont *continueLabel;
	CCLabelBMFont *returnToMainMenuLabel;
	CCLabelBMFont *sfxLabel;
	CCLabelBMFont *musicLabel;
	CCMenu *pauseMenu;	

	CCLabelBMFont *bigLabel;
	CCLabelBMFont *smallLabel;
	CCSpriteBatchNode *sceneSpriteBatchNode;
	CC_SYNTHESIZE( SneakyButton *, jumpButton, JumpButton );
	CC_SYNTHESIZE( SneakyButton *, leftButton, LeftButton );
	CC_SYNTHESIZE( SneakyButton *, rightButton, RightButton );
	CC_SYNTHESIZE( SneakyButton *, backMoveButton, BackMoveButton );

	CC_SYNTHESIZE( SneakyButtonSkinnedBase *, jumpButtonBase, JumpButtonBase );	
	CC_SYNTHESIZE( SneakyButtonSkinnedBase *, leftButtonBase, LeftButtonBase );
	CC_SYNTHESIZE( SneakyButtonSkinnedBase *, rightButtonBase, RightButtonBase );	
	CC_SYNTHESIZE( SneakyButtonSkinnedBase *, backMoveButtonBase, BackMoveButtonBase );	

	// stores current kind of texture 
	bool currentLeftButtonTextureIsDirection;        
	bool currentRightButtonTextureIsDirection;   	      
	bool currentBackMoveButtonTextureIsLeft;  

public:
	LAYER_NODE_FUNC( Scene1UILayer );
	bool init();
	bool displayText( const char *text, CCObject *target, SEL_CallFuncN selector, bool bigText );
	void initButtons();
	void setLeftDirectionButtonTexture( bool isDirection );
	void setRightDirectionButtonTexture( bool isDirection );
	void setBackMoveDirectionButtonTexture( bool left );	
	void setJumpDirectionButtonTexture();
	void keyBackClicked();
	void keyMenuClicked();
	void displayPauseMenu( CCObject* pSender );
	void continueGame( CCObject* pSender );
	void turnSFX( CCObject* pSender );
	void turnMusic( CCObject* pSender );
	void continued( CCNode *pSender );
	void returnToMainMenu( CCObject* pSender );
	void showMenu();
	//void setLoadingProcess( int percent );
};

#endif