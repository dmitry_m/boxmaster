#include "Scene1.h"

using namespace cocos2d;

bool Scene1::init()
{
	bool pRet = false;

	if ( CCScene::init() )
	{
		Scene1UILayer *uiLayer = Scene1UILayer::node();
		this->addChild( uiLayer, 1 );

		Scene1ActionLayer *actionLayer = new Scene1ActionLayer();
		actionLayer->initWithScene1UILayer( uiLayer );
		this->addChild( actionLayer, 0 );
		actionLayer->autorelease();
		pRet = true;
	}
	
	return pRet;
}