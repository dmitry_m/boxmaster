#ifndef SCENE1_H
#define SCENE1_H

#include "Constants\Constants.h"
#include "Scenes\Scene1\Layers\Scene1UILayer.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"

class Scene1 : public cocos2d::CCScene {
public:
	SCENE_NODE_FUNC( Scene1 );
	bool init();
};

#endif
