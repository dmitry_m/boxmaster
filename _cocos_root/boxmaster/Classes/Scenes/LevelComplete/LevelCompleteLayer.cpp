#include "LevelCompleteLayer.h"

using namespace cocos2d;

void LevelCompleteLayer::ccTouchesBegan( cocos2d::CCSet *touches, cocos2d::CCEvent *event ) {
	if ( hasBeenSkipped == false )
	{
		GameManager::sharedGameManager()->setHasPlayerDied( false ); // Reset this for the next level
		GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );
		hasBeenSkipped = true;
	}
}

bool LevelCompleteLayer::init() {
	bool pRet = false;
	if ( CCLayer::init() )
	{
		disableInput = false;
		hasBeenSkipped = false;
		this->setIsTouchEnabled( false );
		this->setIsKeypadEnabled( true );

		GameComplexityType complexity = GameManager::sharedGameManager()->getGameComplexity();
		int newResult = GameManager::sharedGameManager()->getScores();
		int oldResult = _CONFIGMAN->getMaxScoresForComplexity( complexity );
		bool newRecord = _CONFIGMAN->dispatchScores( newResult, complexity );

		// ����������� ��������	
		this->initBackgroundAndLabels( newRecord, newResult, oldResult );

		pRet = true;
	}
	displayMenu(0);
	return pRet;
}

bool LevelCompleteLayer::gotNewMedal( int newRes, int oldRes, char* buf ) {
	if( newRes >= FIRST_CUP_SCORES && oldRes < FIRST_CUP_SCORES ) {
		strcpy( buf, IM_MEDAL1 );
	}
	else if( newRes >= SECOND_CUP_SCORES && oldRes < SECOND_CUP_SCORES ) {
		strcpy( buf, IM_MEDAL2 );
	}
	else if( newRes >= THIRD_CUP_SCORES && oldRes < THIRD_CUP_SCORES ) {
		strcpy( buf, IM_MEDAL3 );
	}
	else {
		return false;
	}
	return true;
}

void LevelCompleteLayer::initBackgroundAndLabels( bool newRecord, int newResult, int oldResult ) {	
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

		char buf[128];
		float scale = 0.8;	
		
		// ���
		CCSprite *background = CCSprite::spriteWithFile( IM_MENU_BACK );
		background->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background, 0 );	

		// ����� ������ ��� �������� ����������
		if( !newRecord ) {
			switch( rand() % 3 ) {
			case 0:
				strcpy( buf, LABEL_RESULT1 );
				scale = 0.8;
				break;
			case 1:
				strcpy( buf, LABEL_RESULT2 );
				scale = 0.8;
				break;
			case 2:
				strcpy( buf, LABEL_RESULT3 );
				scale = 0.65;
				break;
			}
		}
		
		// ����� ���������
		textLabel1 = CCLabelBMFont::labelWithString( newRecord ? LABEL_RECORD : buf, kBigFont );
#if NEED_FREE
		textLabel1->setPosition( ccp( screenSize.width * 0.50f,  screenSize.height * 0.75f ) );
#else
		textLabel1->setPosition( ccp( screenSize.width * 0.50f,  screenSize.height * 0.85f ) );
#endif
		textLabel1->setOpacity( 0 );
		textLabel1->setScale( scale );
		this->addChild( textLabel1, 1 );
		textLabel1->runAction( CCFadeIn::actionWithDuration( 2*FADE_TIME ) );
				
		sprintf( buf, "%d", newResult );
		scoresLabel1 = CCLabelBMFont::labelWithString( buf, kBigFont );
		scoresLabel1->setPosition( ccp( screenSize.width * 0.50f, screenSize.height * 0.55f ) );
		scoresLabel1->setOpacity( 0 );
		scoresLabel1->setScale( 0.9 );
		scoresLabel1->setRotation( ( rand() % 3 )* 5 - 5 );
		this->addChild( scoresLabel1, 1 );	
		scoresLabel1->runAction( CCFadeIn::actionWithDuration( 2*FADE_TIME ) );	

#if NEED_HS
		// ������ ���������
		if( oldResult >= 0 ) {
			textLabel2 = CCLabelBMFont::labelWithString( newRecord ? LABEL_OLD_RECORD : LABEL_CUR_RECORD, kBigFont );
			textLabel2->setPosition( ccp( screenSize.width * 0.50f, screenSize.height * 0.20f ) );
			textLabel2->setOpacity( 0 );
			textLabel2->setScale( 0.5 );
			this->addChild( textLabel2, 1 );	
			textLabel2->runAction( CCFadeIn::actionWithDuration( 2*FADE_TIME ) );	
			
			sprintf( buf, "%d", oldResult );
			scoresLabel2 = CCLabelBMFont::labelWithString( buf, kBigFont );
			scoresLabel2->setPosition( ccp( screenSize.width * 0.5, screenSize.height * 0.10f ) );
			scoresLabel2->setOpacity( 0 );
			scoresLabel2->setScale( 0.4 );
			this->addChild( scoresLabel2, 1 );	
			scoresLabel2->runAction( CCFadeIn::actionWithDuration( 2*FADE_TIME ) );
		}

		if( gotNewMedal( newResult, oldResult, buf ) ) {
			CCSprite *medal = CCSprite::spriteWithFile( buf );
			medal->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
			medal->setOpacity( 0 );
			medal->setScale( 10.0f );
			medal->runAction( CCSequence::actions(
				CCActionInterval::actionWithDuration( 2*FADE_TIME ),
				CCSpawn::actions( CCFadeIn::actionWithDuration( FADE_TIME ),
					CCMoveTo::actionWithDuration( 2*FADE_TIME, ccp( screenSize.width * 0.80f, screenSize.height * 0.55f ) ),
					CCScaleTo::actionWithDuration( 2*FADE_TIME, 0.8f ),
					NULL ),
				CCScaleTo::actionWithDuration( 0.1f, 1.0f ),
				NULL ) );

			this->addChild( medal, 10 );	
		}			
#endif

		// ��� ����������� FPS
#if NEED_DF
		CCSprite* fpsDisplay = CCSprite::spriteWithFile( IM_SKY );
		fpsDisplay->setPosition( ccp( fpsDisplay->boundingBox().size.width * FPS_COEFF, fpsDisplay->boundingBox().size.height * FPS_COEFF) );
		this->addChild(fpsDisplay, 200 );
#endif
}

void LevelCompleteLayer::returnToMainMenu( CCObject* pSender ) {
	if( !disableInput) {
		GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );
		disableInput = true;
	}
} 

void LevelCompleteLayer::restartLevel( CCObject* pSender ) {
	if( !disableInput) {
		GameManager::sharedGameManager()->runSceneWithID( kGameLevel1 );
		disableInput = true;
	}
} 

void LevelCompleteLayer::displayMenu( CCObject* pSender ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	// return to main menu button	
	CCMenuItemImage *returnButton = CCMenuItemImage::itemFromNormalImage( 
		IM_RETURN,
		IM_RETURN_PR,
		NULL,
		this,
		menu_selector( LevelCompleteLayer::returnToMainMenu ) );

	returnButtonMenu = CCMenu::menuWithItems( returnButton, NULL );
	returnButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	returnButtonMenu->setPosition( ccp( 0, MENU_BOTTOM_BUTTON_Y_COEFFICIENT * screenSize.height ) );
	returnButtonMenu->setOpacity( 0 );
	CCAction* returnButtonAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_LEFT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	returnButtonMenu->runAction( returnButtonAction );
	this->addChild( returnButtonMenu, 0, kMainMenuTagValue );

	// restart level button	
	CCMenuItemImage *restartButton = CCMenuItemImage::itemFromNormalImage( 
		IM_RESTART,
		IM_RESTART_PR,
		NULL,
		this,
		menu_selector( LevelCompleteLayer::restartLevel ) );

	restartButtonMenu = CCMenu::menuWithItems( restartButton, NULL );
	restartButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	restartButtonMenu->setPosition( ccp( screenSize.width, MENU_BOTTOM_BUTTON_Y_COEFFICIENT * screenSize.height ) );
	restartButtonMenu->setOpacity( 0 );
	CCAction* restartButtonAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_RIGHT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	restartButtonMenu->runAction( restartButtonAction );
	this->addChild( restartButtonMenu, 0, kMainMenuTagValue );
}

void LevelCompleteLayer::keyBackClicked() {
	this->returnToMainMenu( NULL );		
}