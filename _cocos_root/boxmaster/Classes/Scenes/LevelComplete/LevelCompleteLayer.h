#ifndef LEVELCOMPLETELAYER_H
#define LEVELCOMPLETELAYER_H

#include "Constants\Constants.h"
#include "Singletons\GameManager.h"

class LevelCompleteLayer : public cocos2d::CCLayer
{
protected:
	CCLabelBMFont* textLabel1;
	CCLabelBMFont* scoresLabel1;
	
	CCLabelBMFont* textLabel2;
	CCLabelBMFont* scoresLabel2;

	CCMenu *returnButtonMenu;	
	CCMenu *restartButtonMenu;

	bool hasBeenSkipped;
	bool disableInput;

	void initBackgroundAndLabels( bool newRecord, int newResult, int oldResult );
public:
	LAYER_NODE_FUNC( LevelCompleteLayer );
	void ccTouchesBegan( cocos2d::CCSet *touches, cocos2d::CCEvent *event );
	void returnToMainMenu( CCObject* pSender );
	void restartLevel( CCObject* pSender );
	void displayMenu( CCObject* pSender );
	bool init();
	void keyBackClicked();
	bool gotNewMedal( int newRes, int oldRes, char* buf );
};

#endif
