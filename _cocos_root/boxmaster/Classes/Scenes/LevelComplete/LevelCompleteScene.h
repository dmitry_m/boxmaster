#ifndef LEVELCOMPLETESCENE_H
#define LEVELCOMPLETESCENE_H

#include "LevelCompleteLayer.h"

using namespace std;

class LevelCompleteScene : public CCScene
{
public:
	SCENE_NODE_FUNC( LevelCompleteScene );
	bool init();
};

#endif