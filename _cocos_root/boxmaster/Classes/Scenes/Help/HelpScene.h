#ifndef HELPSCENE_H
#define HELPSCENE_H

#include "HelpLayer.h"

using namespace cocos2d;

class HelpScene : public CCScene {
public:
	SCENE_NODE_FUNC( HelpScene );
	bool init();	
};

#endif
