#ifndef HELPLAYER_H
#define HELPLAYER_H

#include "Singletons\GameManager.h"

using namespace cocos2d;

class HelpLayer : public CCLayer {
	bool disableInput;
	CCSprite* hider;

	CCLabelBMFont* caption;
	CCLabelBMFont* mainDiscription;
	CCLabelBMFont* dbDiscription;
	CCLabelBMFont* lgDiscription;
	CCLabelBMFont* tpDiscription;
	CCLabelBMFont* shDiscription;
	
	CCMenu* destroyButtonMenu;
	CCMenu* gravityButtonMenu;
	CCMenu* tpButtonMenu;
	CCMenu* shieldButtonMenu;

	CCMenu* menu;

	CCMenuItemImage* destroyButton;
	CCMenuItemImage* gravityButton;
	CCMenuItemImage* tpButton;
	CCMenuItemImage* shieldButton;
	CCMenuItemImage* creditsButton;

	int requestedPage;
	float width, height;

	void dbPressed( CCObject* pSender );
	void lgPressed( CCObject* pSender );
	void tpPressed( CCObject* pSender );
	void shPressed( CCObject* pSender );

public:
	CCLabelBMFont *label;
	LAYER_NODE_FUNC( HelpLayer );
	
	void initHelpLabels();
	void returnToMainMenu( CCObject* pSender );
	void openCredits( CCObject* pSender );
	void displayMenu( CCObject* pSender );
	bool init();
	void keyBackClicked();
	void gotoPage( int page );
	void showAndHideLabels( CCObject* pSender );
	void prepareImagesForPage( CCObject* pSender );
};

#endif
