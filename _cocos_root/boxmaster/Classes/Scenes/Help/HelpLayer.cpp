#include "HelpLayer.h"

bool HelpLayer::init() {
	bool pRet = 0;
	if ( CCLayer::init() ) {
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize(); 
		disableInput = false;
		this->setIsTouchEnabled( false );
		this->setIsKeypadEnabled( true );

		CCSprite *background_paper = CCSprite::spriteWithFile( IM_PAPER );
		background_paper->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( background_paper, 0 );
		
		initHelpLabels();

		// for fps visible
#if NEED_DF
			CCSprite* fpsDisplay = CCSprite::spriteWithFile( IM_SKY );
			fpsDisplay->setPosition( ccp( fpsDisplay->boundingBox().size.width * FPS_COEFF, fpsDisplay->boundingBox().size.height * FPS_COEFF) );
			this->addChild(fpsDisplay, 200 );
#endif

		hider = CCSprite::spriteWithFile( IM_PAPER );
		hider->setPosition( ccp( screenSize.width/2, screenSize.height/2 ) );
		this->addChild( hider, 20 );
		
		hider->runAction( CCSpawn::actions( 
			CCFadeOut::actionWithDuration( FADE_TIME ), 
			CCCallFuncO::actionWithTarget( this, callfuncO_selector( HelpLayer::displayMenu ), hider ),
			NULL ) );
		

		pRet = 1;
	}
	return pRet;
}

void HelpLayer::initHelpLabels() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	float scale = 0.40f;
	// ������ "<����������>"
	caption = CCLabelBMFont::labelWithString( LABEL_HELP_INSTRUCTION, kEngFont );
	caption->setPosition( ccp( screenSize.width * 0.5f, screenSize.height * 0.93f ) );
	this->addChild( caption, 10 );
	
	// ������� ��������
#if !FREE
	mainDiscription = CCLabelBMFont::labelWithString( LABEL_HELP_TROPHY, kFont );
#else
	mainDiscription = CCLabelBMFont::labelWithString( LABEL_HELP_TROPHY_FREE, kEngFont );
#endif
	mainDiscription->setPosition( ccp( screenSize.width * 0.52f, screenSize.height * 0.5f ) );
	mainDiscription->setScale( scale );
	this->addChild( mainDiscription, 10 );

	// ���������� � ������ "���������� ����"		
	dbDiscription = CCLabelBMFont::labelWithString( LABEL_HELP_DB, kFont );
	dbDiscription->setPosition( ccp( screenSize.width * 0.52f, screenSize.height * 0.70f ) );
	dbDiscription->setScale( scale );
	this->addChild( dbDiscription, 10 );
	
	// ���������� � ������ "������ ����������"
	lgDiscription = CCLabelBMFont::labelWithString( LABEL_HELP_LG, kFont );
	lgDiscription->setPosition( ccp( screenSize.width * 0.52f, screenSize.height * 0.70f ) );
	lgDiscription->setScale( scale );
	this->addChild( lgDiscription, 10 );

	// ���������� � ������ "������������"
	tpDiscription = CCLabelBMFont::labelWithString( LABEL_HELP_TP, kFont );
	tpDiscription->setPosition( ccp( screenSize.width * 0.52f, screenSize.height * 0.65f ) );
	tpDiscription->setScale( scale );
	this->addChild( tpDiscription, 10 );

	// ���������� � ������ "���"
	shDiscription = CCLabelBMFont::labelWithString( LABEL_HELP_SH, kFont );
	shDiscription->setPosition( ccp( screenSize.width * 0.52f, screenSize.height * 0.70f ) );
	shDiscription->setScale( scale );
	this->addChild( shDiscription, 10 );
} 

void HelpLayer::gotoPage( int page ) {
	requestedPage = page;
	hider->runAction( CCSpawn::actions( 
		CCFadeIn::actionWithDuration( FADE_TIME ), 
		CCCallFuncO::actionWithTarget( this, callfuncO_selector( HelpLayer::showAndHideLabels ), hider ),
		CCCallFuncO::actionWithTarget( this, callfuncO_selector( HelpLayer::prepareImagesForPage ), hider ),			
		CCFadeOut::actionWithDuration( FADE_TIME ), 
		NULL ) );
}

void HelpLayer::prepareImagesForPage(  CCObject* pSender  ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	destroyButton->stopAllActions();
	gravityButton->stopAllActions();
	tpButton->stopAllActions();
	shieldButton->stopAllActions();

	float alt = 0.24f;
	float startScale = 1.0f;
	float beginTime = 1.0f * FADE_TIME;
	float dbTime = 0.5f * FADE_TIME;
	float lgTime = 1.5f * FADE_TIME;
	float tpTime = 1.0f * FADE_TIME;
	float shTime = 0 * FADE_TIME;	
	float actionTime = FADE_TIME;
	
	float angle1 = -7.0f;
	float angle2 = 2.0f;
	float angle3 = 0.0f;
	float angle4 = -5.0f;

	CCPoint dbPos1 = ccp( 0, -height * 2.0f );
	CCPoint lgPos1 = ccp( 0, -height * 2.0f );
	CCPoint tpPos1 = ccp( 0, -height * 2.0f );
	CCPoint shPos1 = ccp( 0, -height * 2.0f );

	CCPoint dbPos2 = ccp( - 0.45f * screenSize.width, screenSize.height * ( 0.8f ) );
	CCPoint lgPos2 = ccp( - 0.45f * screenSize.width, screenSize.height * ( 0.45f ) );
	CCPoint tpPos2 = ccp( + 0.45f * screenSize.width, screenSize.height * ( 0.8f ) );
	CCPoint shPos2 = ccp( + 0.45f * screenSize.width, screenSize.height * ( 0.45f ) ) ;
	
	CCPoint centerPos = ccp( 0.0f, screenSize.height * 0.2f ) ;
		
	destroyButton->setPosition( dbPos1 );
	destroyButton->setScale( startScale );
	destroyButton->setRotation( angle1 );

	gravityButton->setPosition( lgPos1 );
	gravityButton->setScale( startScale );
	gravityButton->setRotation( angle2 );

	tpButton->setPosition( tpPos1 );
	tpButton->setScale( startScale );
	tpButton->setRotation( angle3 );

	shieldButton->setPosition( shPos1 );
	shieldButton->setScale( startScale );
	shieldButton->setRotation( angle4 );

	switch( requestedPage ) {
	case 0:
		creditsButton->setIsVisible( true );
		// ��������� ���� �����������
		destroyButton->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( dbTime + beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),				
					CCMoveTo::actionWithDuration( actionTime, dbPos2 ),
					//CCRotateTo::actionWithDuration( actionTime, angle1 ), 
					NULL ),
				NULL ) );
	
		gravityButton->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( lgTime + beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, lgPos2 ),
					//CCRotateTo::actionWithDuration( actionTime, angle2 ),
					NULL ),
				NULL ) );
	
		tpButton->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( tpTime + beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, tpPos2 ),
					//CCRotateTo::actionWithDuration( actionTime, angle3 ),
					NULL ),
				NULL ) );
	
		shieldButton->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( shTime + beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, shPos2 ),
					//CCRotateTo::actionWithDuration( actionTime, angle4 ),
					NULL ),
				NULL ) );
			break;
	case 1:
		creditsButton->setIsVisible( false );
		// ��������� "���������� ����"
		destroyButton->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),				
					CCMoveTo::actionWithDuration( actionTime, centerPos ),
					//CCRotateTo::actionWithDuration( actionTime, angle1 ),
					NULL ),
				NULL ) );

		break;
	case 2:
		creditsButton->setIsVisible( false );
		// ��������� "������ ����������"
		gravityButton->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, centerPos ),
					//CCRotateTo::actionWithDuration( actionTime, angle2 ),
					NULL ),
				NULL ) );

		break;
	case 3:
		creditsButton->setIsVisible( false );
		// ��������� "��������"
		tpButton->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, centerPos ),
					//CCRotateTo::actionWithDuration( actionTime, angle3 ),
					NULL ),
				NULL ) );
		break;
	case 4:
		creditsButton->setIsVisible( false );
		// ��������� "���"
		shieldButton->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, centerPos ),
					//CCRotateTo::actionWithDuration( actionTime, angle4 ),
					NULL ),
				NULL ) );
		break;
	}
}

void HelpLayer::showAndHideLabels( CCObject* pSender ) {
	switch( requestedPage ) {
	case 0:
		caption->setString( LABEL_HELP_INSTRUCTION );
		mainDiscription->setOpacity( 255 );
		dbDiscription->setOpacity( 0 );
		lgDiscription->setOpacity( 0 );
		tpDiscription->setOpacity( 0 );
		shDiscription->setOpacity( 0 );
		break;
	case 1:
		caption->setString( LABEL_HELP_CAPTION_DB ); 
		mainDiscription->setOpacity( 0 );
		dbDiscription->setOpacity( 255 );
		lgDiscription->setOpacity( 0 );
		tpDiscription->setOpacity( 0 );
		shDiscription->setOpacity( 0 );

		break;
	case 2:
		caption->setString( LABEL_HELP_CAPTION_LG ); 
		mainDiscription->setOpacity( 0 );
		dbDiscription->setOpacity( 0 );
		lgDiscription->setOpacity( 255 );
		tpDiscription->setOpacity( 0 );
		shDiscription->setOpacity( 0 );

		break;
	case 3:
		caption->setString( LABEL_HELP_CAPTION_TP ); 
		mainDiscription->setOpacity( 0 );
		dbDiscription->setOpacity( 0 );
		lgDiscription->setOpacity( 0 );
		tpDiscription->setOpacity( 255 );
		shDiscription->setOpacity( 0 );

		break;
	case 4:
		caption->setString( LABEL_HELP_CAPTION_SH ); 
		mainDiscription->setOpacity( 0 );
		dbDiscription->setOpacity( 0 );
		lgDiscription->setOpacity( 0 );
		tpDiscription->setOpacity( 0 );
		shDiscription->setOpacity( 255 );

		break;
	}
}

void HelpLayer::displayMenu( CCObject* pSender ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	float alt = 0.24f;
		
	CCSprite *forSizeDetecting = CCSprite::spriteWithFile( IM_HELP_DB );
	width = forSizeDetecting->boundingBox().size.width;
	height = forSizeDetecting->boundingBox().size.height;
	forSizeDetecting->release();

	CCPoint dbPos1 = ccp( screenSize.width * 0.50f, -height * 2.0f );
	CCPoint lgPos1 = ccp( screenSize.width * 0.50f, -height * 2.0f );
	CCPoint tpPos1 = ccp( screenSize.width * 0.50f, -height * 2.0f );
	CCPoint shPos1 = ccp( screenSize.width * 0.50f, -height * 2.0f );

	// ����������� � ������� ����
	CCMenuItemImage *returnButton = CCMenuItemImage::itemFromNormalImage( 
		IM_RETURN,
		IM_RETURN_PR,
		NULL,
		this,
		menu_selector( HelpLayer::returnToMainMenu ) );

	CCMenu* returnButtonMenu = CCMenu::menuWithItems( returnButton, NULL );
	returnButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	returnButtonMenu->setPosition( ccp( 0, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT) );
	returnButtonMenu->setOpacity( 0 );

	CCAction* returnButtonAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_LEFT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	returnButtonMenu->runAction( returnButtonAction );
	this->addChild( returnButtonMenu, 5 );

	// ���������	
	creditsButton = CCMenuItemImage::itemFromNormalImage( 
		IM_CREDITS,
		IM_CREDITS_PR,
		NULL,
		this,
		menu_selector( HelpLayer::openCredits ) );

	CCMenu* creditsButtonMenu = CCMenu::menuWithItems( creditsButton, NULL );
	creditsButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	creditsButtonMenu->setPosition( ccp( screenSize.width, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT) );
	creditsButtonMenu->setOpacity( 0 );

	CCAction* creditsButtonAction = CCSpawn::actions( 
		CCMoveTo::actionWithDuration( BUTTONS_APPEAR_TIME, ccp( screenSize.width * MENU_RIGHT_BUTTON_X_COEFFICIENT, screenSize.height * MENU_BOTTOM_BUTTON_Y_COEFFICIENT ) ),
		CCFadeIn::actionWithDuration( FADE_TIME ),
		NULL );
	creditsButtonMenu->runAction( creditsButtonAction );
	this->addChild( creditsButtonMenu, 5 );
	
	destroyButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HELP_DB,
		IM_HELP_DB,
		NULL,
		this,
		menu_selector( HelpLayer::dbPressed ) );

	// ���������� � ������ "������ ����������"
	
	gravityButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HELP_LG,
		IM_HELP_LG,
		NULL,
		this,
		menu_selector( HelpLayer::lgPressed ) );

	// ���������� � ������ "������������"
	
	tpButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HELP_TP,
		IM_HELP_TP,
		NULL,
		this,
		menu_selector( HelpLayer::tpPressed ) );

	// ���������� � ������ "���"
	
	shieldButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HELP_SH,
		IM_HELP_SH,
		NULL,
		this,
		menu_selector( HelpLayer::shPressed ) );

	menu = CCMenu::menuWithItems( shieldButton, destroyButton, tpButton, gravityButton, NULL );
	menu->setPosition( ccp( screenSize.width * 0.5f, 0.0f ) );
	
	//menu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	this->addChild( menu, 4 );	

	// ���������
	gotoPage( 0 );
}

void HelpLayer::returnToMainMenu( CCObject* pSender ) {
	GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );
} 

void HelpLayer::openCredits( CCObject* pSender ) {
	GameManager::sharedGameManager()->runSceneWithID( kCreditsScene );
} 

void HelpLayer::dbPressed( CCObject* pSender ) {
#if !FREE
	gotoPage( requestedPage == 1 ? 0 : 1 );
#endif
} 

void HelpLayer::lgPressed( CCObject* pSender ) {
#if !FREE
	gotoPage( requestedPage == 2 ? 0 : 2 );
#endif
} 

void HelpLayer::tpPressed( CCObject* pSender ) {
#if !FREE
	gotoPage( requestedPage == 3 ? 0 : 3 );
#endif
} 

void HelpLayer::shPressed( CCObject* pSender ) {
#if !FREE
	gotoPage( requestedPage == 4 ? 0 : 4 );
#endif
} 

void HelpLayer::keyBackClicked() {
	this->returnToMainMenu( NULL );		
}


/*
void HelpLayer::prepareImagesForPage(  CCObject* pSender  ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	destroyButtonMenu->stopAllActions();
	gravityButtonMenu->stopAllActions();
	tpButtonMenu->stopAllActions();
	shieldButtonMenu->stopAllActions();

	float alt = 0.24f;
	float startScale = 1.0f;
	float beginTime = 1 * FADE_TIME;
	float dbTime = 1 * FADE_TIME;
	float lgTime = 3 * FADE_TIME;
	float tpTime = 2 * FADE_TIME;
	float shTime = 0 * FADE_TIME;	
	float actionTime = FADE_TIME;
	
	float angle1 = -15.0f;
	float angle2 = 2.0f;
	float angle3 = -10.0f;
	float angle4 = 10.0f;

	CCPoint dbPos1 = ccp( screenSize.width * 0.50f, -height * 2.0f );
	CCPoint lgPos1 = ccp( screenSize.width * 0.50f, -height * 2.0f );
	CCPoint tpPos1 = ccp( screenSize.width * 0.50f, -height * 2.0f );
	CCPoint shPos1 = ccp( screenSize.width * 0.50f, -height * 2.0f );

	CCPoint dbPos2 = ccp( screenSize.width * 0.50f - 0.6f * width, screenSize.height * ( alt + 0.04f ) );
	CCPoint lgPos2 = ccp( screenSize.width * 0.50f - 0.25f * width, screenSize.height * ( alt - 0.02f ) );
	CCPoint tpPos2 = ccp( screenSize.width * 0.50f + 0.25f * width, screenSize.height * ( alt + 0.04f ) );
	CCPoint shPos2 = ccp( screenSize.width * 0.50f + 0.6f * width, screenSize.height * ( alt - 0.01f ) ) ;
	
	CCPoint centerPos = ccp( screenSize.width * 0.50f, screenSize.height *  alt  ) ;
		
	destroyButtonMenu->setPosition( dbPos1 );
	destroyButtonMenu->setScale( startScale );
	destroyButton->setRotation( angle1 );

	gravityButtonMenu->setPosition( lgPos1 );
	gravityButtonMenu->setScale( startScale );
	gravityButton->setRotation( angle2 );

	tpButtonMenu->setPosition( tpPos1 );
	tpButtonMenu->setScale( startScale );
	tpButton->setRotation( angle3 );

	shieldButtonMenu->setPosition( shPos1 );
	shieldButtonMenu->setScale( startScale );
	shieldButton->setRotation( angle4 );

	switch( requestedPage ) {
	case 0:
		creditsButton->setIsVisible( true );
		// ��������� ���� �����������
		destroyButtonMenu->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( dbTime + beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),				
					CCMoveTo::actionWithDuration( actionTime, dbPos2 ),
					//CCRotateTo::actionWithDuration( actionTime, angle1 ), 
					NULL ),
				NULL ) );
	
		gravityButtonMenu->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( lgTime + beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, lgPos2 ),
					//CCRotateTo::actionWithDuration( actionTime, angle2 ),
					NULL ),
				NULL ) );
	
		tpButtonMenu->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( tpTime + beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, tpPos2 ),
					//CCRotateTo::actionWithDuration( actionTime, angle3 ),
					NULL ),
				NULL ) );
	
		shieldButtonMenu->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( shTime + beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, shPos2 ),
					//CCRotateTo::actionWithDuration( actionTime, angle4 ),
					NULL ),
				NULL ) );
			break;
	case 1:
		creditsButton->setIsVisible( false );
		// ��������� "���������� ����"
		destroyButtonMenu->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),				
					CCMoveTo::actionWithDuration( actionTime, centerPos ),
					//CCRotateTo::actionWithDuration( actionTime, angle1 ),
					NULL ),
				NULL ) );

		break;
	case 2:
		creditsButton->setIsVisible( false );
		// ��������� "������ ����������"
		gravityButtonMenu->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, centerPos ),
					//CCRotateTo::actionWithDuration( actionTime, angle2 ),
					NULL ),
				NULL ) );

		break;
	case 3:
		creditsButton->setIsVisible( false );
		// ��������� "��������"
		tpButtonMenu->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, centerPos ),
					//CCRotateTo::actionWithDuration( actionTime, angle3 ),
					NULL ),
				NULL ) );
		break;
	case 4:
		creditsButton->setIsVisible( false );
		// ��������� "���"
		shieldButtonMenu->runAction( 
			CCSequence::actions( 
				CCActionInterval::actionWithDuration( beginTime ),
				CCSpawn::actions( 
					CCFadeIn::actionWithDuration( 0 ), 
					//CCScaleTo::actionWithDuration( scaleTime, 1.0f ),			
					CCMoveTo::actionWithDuration( actionTime, centerPos ),
					//CCRotateTo::actionWithDuration( actionTime, angle4 ),
					NULL ),
				NULL ) );
		break;
	}
}
*/


/*
	// ���������� � ������ "���������� ����"
	
	destroyButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HELP_DB,
		IM_HELP_DB,
		NULL,
		this,
		menu_selector( HelpLayer::dbPressed ) );
	
	destroyButtonMenu = CCMenu::menuWithItems( destroyButton, NULL );
	destroyButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	destroyButtonMenu->setPosition( dbPos1 );
	destroyButtonMenu->setOpacity( 0 );
	this->addChild( destroyButtonMenu, 5 );
	destroyButtonMenu->

	// ���������� � ������ "������ ����������"
	
	gravityButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HELP_LG,
		IM_HELP_LG,
		NULL,
		this,
		menu_selector( HelpLayer::lgPressed ) );

	gravityButtonMenu = CCMenu::menuWithItems( gravityButton, NULL );
	gravityButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	gravityButtonMenu->setPosition( lgPos1 );
	gravityButtonMenu->setOpacity( 0 );
	this->addChild( gravityButtonMenu, 7 );

	// ���������� � ������ "������������"
	
	tpButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HELP_TP,
		IM_HELP_TP,
		NULL,
		this,
		menu_selector( HelpLayer::tpPressed ) );

	tpButtonMenu = CCMenu::menuWithItems( tpButton, NULL );
	tpButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	tpButtonMenu->setPosition( tpPos1 );
	tpButtonMenu->setOpacity( 0 );
	this->addChild( tpButtonMenu, 6 );

	// ���������� � ������ "���"
	
	shieldButton = CCMenuItemImage::itemFromNormalImage( 
		IM_HELP_SH,
		IM_HELP_SH,
		NULL,
		this,
		menu_selector( HelpLayer::shPressed ) );

	shieldButtonMenu = CCMenu::menuWithItems( shieldButton, NULL );
	shieldButtonMenu->alignItemsVerticallyWithPadding( screenSize.height * 0.059f );
	shieldButtonMenu->setPosition( shPos1 );
	shieldButtonMenu->setOpacity( 0 );
	this->addChild( shieldButtonMenu, 4 );	
*/
	// ���������� � ������ "���������� ����"
