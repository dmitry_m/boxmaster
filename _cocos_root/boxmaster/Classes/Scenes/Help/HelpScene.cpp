#include "HelpScene.h"

bool HelpScene::init() {
	bool pRet = 0;
	if ( CCScene::init() ) {
		HelpLayer* myHelpLayer = HelpLayer::node();
		this->addChild( myHelpLayer );
		pRet = 1;
	}
	return pRet;
}

