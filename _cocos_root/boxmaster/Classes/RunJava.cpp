﻿#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "RunJava.h"
#include "Configuration\Configuration.h"

using namespace cocos2d;

extern "C"
{
// Вызов этих функций происходит из Java

#if ( FREE == 1 )
	void Java_org_cocos2dx_boxmasterfree_boxmaster_setIsRecord( JNIEnv* env, jobject thiz ) {
#else
	void Java_org_cocos2dx_boxmaster_boxmaster_setIsRecord( JNIEnv* env, jobject thiz ) {
#endif
		GameManager::sharedGameManager()->setIsRecord( true );	
	}


#if ( FREE == 1 )
	void Java_org_cocos2dx_boxmasterfree_boxmaster_setUserLoggedIn( JNIEnv* env, jobject thiz, jboolean logged ) {
#else
	void Java_org_cocos2dx_boxmaster_boxmaster_setUserLoggedIn( JNIEnv* env, jobject thiz, jboolean logged ) {
#endif
	#if NEEDS_OF
		GameManager::sharedGameManager()->setUserLoggedIn( logged );	
	#endif
	}

#if ( FREE == 1 )
	void Java_org_cocos2dx_boxmasterfree_boxmaster_writeScoresFromOF( JNIEnv* env, jobject thiz, jint scores, jint complexity ) {
#else
	void Java_org_cocos2dx_boxmaster_boxmaster_writeScoresFromOF( JNIEnv* env, jobject thiz, jint scores, jint complexity ) {
#endif
	#if NEEDS_OF
		_CONFIGMAN->syncOpenfeintScoresWithLocal( scores, complexity );
	#endif
	}

// Отсюда происходит вызов кода в Java
	void RunJava::openDashBoard() {
		JniMethodInfo methodInfo;
		if (! JniHelper::getStaticMethodInfo( methodInfo, CLASS_OPEN_NAME, 
			"showLeaderboard", "()V"))
		{
			CCLog("Can't not find static method showLeadboard");
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	void RunJava::userLoggedIn() {
		JniMethodInfo methodInfo;
		if (! JniHelper::getStaticMethodInfo( methodInfo, CLASS_OPEN_NAME, 
			"userLoggedIn", "()V"))
		{
			CCLog("Can't not find static method showLeadboard");
			return;
		}
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	void RunJava::submitScores( int scores, int complexity ) {
		JniMethodInfo methodInfo;
		if (! JniHelper::getStaticMethodInfo( methodInfo, CLASS_OPEN_NAME, 
			"submitScores", "(II)V"))
		{
			CCLog("Can't not find static method submitScores");
			return;
		}
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, scores, complexity);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	
	void RunJava::getScores() {
		JniMethodInfo methodInfo;
		if (! JniHelper::getStaticMethodInfo( methodInfo, CLASS_OPEN_NAME, 
			"getScoresForPlayer", "()V"))
		{
			CCLog("Can't not find static method getScoresForPlayer");
			return;
		}
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

}
#endif // CC_PLATFORM_ANDROID
