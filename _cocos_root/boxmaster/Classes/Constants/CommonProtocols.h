#ifndef COMMON_PROTOCOLS_H
#define COMMON_PROTOCOLS_H

#include "Constants\Constants.h"

using namespace cocos2d;

#define ENEMY_STATE_DEBUG 0

//My defines

#define MY_CHECKANDRELEASE( Obj )\
if( Obj ) { Obj->release();	 Obj = NULL; }

#define MY_SETANIMFROMPLIST( varName, varMethod, animName, plistName )\
this->set##varMethod( this->loadPlistForAnimationWithName( animName, plistName ) );\
if( varName ) varName##->retain(); \
else CCLOG( "###ERROR READING ANIMATION###" );

#endif