#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "cocos2d.h"
/*
	�������������� ����������

	480 320
	800 480
	854 480
	960 540
	1184 720
	1280 720
	1280 800
*/

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	//#include <vld.h>
	#define SOURCE_SCREEN_WIDTH 800
	#define SOURCE_SCREEN_HEIGHT 480
#endif

#define RUS_LANG				0
#define FREE					0
#define DISPLAY_FPS				0
#define DISPLAY_DEF_FPS			0
#define ENABLE_MUSIC			1
#define DEBUG_DRAW				0
#define SHOW_DEBUG_INFORMATION	0
#define SHOW_SITUATION			0

#if NEED_DI
#define CCLOG( format, ... )      cocos2d::CCLog( format, ##__VA_ARGS__ )
#elif !NEED_DI
#define CCLOG( ... )  
#endif

// ������� ��� ������������������� ����
// NEED_OF NEED_AD NEED_HS NEED_TR
#define NEED_OF					( ( CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID ) && FREE == 0 )
#define NEED_AD					( ( CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID ) && FREE == 1 )
#define NEED_HS					( FREE == 0 )
#define NEED_TR					( FREE == 0 )
#define NEED_FREE				( FREE == 1 )
#define NEED_SS					( SHOW_SITUATION == 1 )
#define NEED_DD					( DEBUG_DRAW == 1 )
#define NEED_DI					( SHOW_DEBUG_INFORMATION == 1 )
#define NEED_DF					( DISPLAY_FPS == 1 )
#define NEED_EM					( ENABLE_MUSIC == 1 )
#define NEED_RUS				( RUS_LANG == 1 )
#define IS_WIN32				( CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 )
#define IS_ANDROID				( CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID )

#include "Singletons\ConstantsClass.h"

#define CONFIG_FILENAME			"cfg"
#define _CONFIGMAN				GameConfiguration::sharedConfigurationManager()
#define _CONFIG					GameConfiguration::sharedConfigurationManager()->Conf

// share plists
#define SOUNDS_PLIST			"Sounds/SoundEffects.plist"
#define LOADER_ANIM_PLIST		GET_PATH("Plists/loader_anim.plist").c_str()
#define LOADER_PLIST			GET_PATH("Plists/loader.plist").c_str()
#define LOADER_PNG				GET_PATH("Plists/loader.png").c_str()
#define JOYSTICK_PLIST			GET_PATH("Plists/joystick.plist").c_str()
#define JOYSTICK_PNG			GET_PATH("Plists/joystick.png").c_str()

// images	
#define IM_LOGO					GET_PATH("Images/greeting/logo.png").c_str()
#define IM_KLAX					GET_PATH("Images/other/klax.png").c_str()
#define IM_WHITE				GET_PATH("Images/other/white.png").c_str()
#define IM_SKY					GET_PATH("Images/background/sky.png").c_str()
#define IM_YES					GET_PATH("Images/menu/buttons/yes.png").c_str()
#define IM_YES_PR				GET_PATH("Images/menu/buttons/yes_pressed.png").c_str()
#define IM_NO					GET_PATH("Images/menu/buttons/no.png").c_str()
#define IM_NO_PR				GET_PATH("Images/menu/buttons/no_pressed.png").c_str()
#define IM_INFO					GET_PATH("Images/menu/buttons/info.png").c_str()
#define IM_INFO_PR				GET_PATH("Images/menu/buttons/info_pressed.png").c_str()
#define IM_HIGHSCORES			GET_PATH("Images/menu/buttons/cup.png").c_str()
#define IM_HIGHSCORES_PR		GET_PATH("Images/menu/buttons/cup_pressed.png").c_str()
#define IM_CREDITS				GET_PATH("Images/menu/buttons/credits.png").c_str()
#define IM_CREDITS_PR		GET_PATH("Images/menu/buttons/credits_pressed.png").c_str()
#define IM_PLAY					GET_PATH("Images/menu/buttons/play.png").c_str()
#define IM_PLAY_PR				GET_PATH("Images/menu/buttons/play_pressed.png").c_str()
#define IM_RESTART				GET_PATH("Images/menu/buttons/restart.png").c_str()
#define IM_RESTART_PR			GET_PATH("Images/menu/buttons/restart_pressed.png").c_str()
#define IM_RETURN				GET_PATH("Images/menu/buttons/return.png").c_str()
#define IM_RETURN_PR			GET_PATH("Images/menu/buttons/return_pressed.png").c_str()
#define IM_RETURN_INVERTED		GET_PATH("Images/menu/buttons/return_inverted.png").c_str()
#define IM_RETURN_INVERTED_PR	GET_PATH("Images/menu/buttons/return_inverted_pressed.png").c_str()
#define IM_SETTINGS				GET_PATH("Images/menu/buttons/settings.png").c_str()
#define IM_SETTINGS_PR			GET_PATH("Images/menu/buttons/settings_pressed.png").c_str()
#define IM_OF					GET_PATH("Images/other/openfeint.png").c_str()	
#define IM_COCOS				GET_PATH("Images/other/cocos_logo.png").c_str()	
#define IM_BACK					GET_PATH("Images/background/back.png").c_str()
#define IM_FABRICATOR			GET_PATH("Images/background/fabricator.png").c_str()
#define IM_FLOOR				GET_PATH("Images/background/floor.png").c_str()
#define IM_SIDE					GET_PATH("Images/background/side.png").c_str()
#define IM_ABOUT				GET_PATH("Images/menu/about.png").c_str()
#define IM_MAIN_MENU_BACK		GET_PATH("Images/menu/main_menu_back.png").c_str()
#define IM_MENU_BACK			GET_PATH("Images/menu/menu_back.png").c_str()
#define IM_PAPER				GET_PATH("Images/menu/paper.png").c_str()
#define IM_PARTICLE				GET_PATH("Images/trophies/particle.png").c_str()
#define IM_MEDAL0				GET_PATH("Images/medals/m0.png").c_str()
#define IM_MEDAL1				GET_PATH("Images/medals/m1.png").c_str()
#define IM_MEDAL2				GET_PATH("Images/medals/m2.png").c_str()
#define IM_MEDAL3				GET_PATH("Images/medals/m3.png").c_str()
#define IM_HELP_DB				GET_PATH("Images/help/destroy_box.png").c_str()
#define IM_HELP_LG				GET_PATH("Images/help/gravity.png").c_str()
#define IM_HELP_SH				GET_PATH("Images/help/shield.png").c_str()
#define IM_HELP_TP				GET_PATH("Images/help/tp.png").c_str()
#define IM_HELP_TR				GET_PATH("Images/help/trophies.png").c_str()

#define IM_FALL_INDICATOR		"fall_indicator.png"
#define IM_TELEPORT				"teleport.png"
#define IM_SHIELD				"shield.png"
#define IM_LOW_GRAVITY			"low_gravity.png"
#define IM_DESTROY_BOX			"destroy_box.png"
#define IM_BIG_SHIELD			"big_shield.png"
#define IM_ARROW				"arrow.png"

//labels
#define LABEL_GAME_NAME			"BoxMaster"
#define LABEL_GAME_NAME_FREE	"BoxMasterFREE"

#if !NEED_RUS
	#define LABEL_VERSION			"1.0"
	#define LABEL_COPYRIGHT			"Copyright 2012 Malyuga Dmitry."
	#define LABEL_ALL_RIGHTS		"All right reserved."
	#define LABEL_PROGRAMMER		"Programming"
	#define LABEL_DESIGNER			"Graphics"
	#define LABEL_TWO_POINTS		":"
	#define LABEL_MUSIC				"Music"
	#define LABEL_MY_NAME			"Malyuga Dmitry"
	#define LABEL_MUSIC_AUTHOR		"Anton Gulkevich"	

	#define LABEL_HELP_INSTRUCTION	"Instruction"
	#define LABEL_HELP_CAPTION_DB	"Box destruction"
	#define LABEL_HELP_CAPTION_LG	"Low gravity"
	#define LABEL_HELP_CAPTION_TP	"Teleport"
	#define LABEL_HELP_CAPTION_SH	"Shield"

	#define LABEL_HELP_TROPHY_FREE	"In a game You should make\na line of the boxes. This\nis a free version.\nIn full version:\n- bonuses\n- highscores\n- openfeint integration\n- and other"
	#define LABEL_HELP_TROPHY		"In a game You should make\na line of the boxes. Then\nthey disappear and You earn\nscores. There are four\nbonus types in the game:\nbox destruction, low gravity,\nteleportation and shield.\nThey appears at random time.\nPicked bonuses displayed at\nthe bottom of the screen."
	#define LABEL_HELP_DB			"- click on the box to destroy it"
	#define LABEL_HELP_LG			"- slide up Your finger to activate\nlow gravity\n- lasts for 15 seconds"
	#define LABEL_HELP_TP			"- slide Your finger throw BoxMaster\nand release it on the point where\nYou want to teleport him - the\ngame will be paused until You choose\nthe location - to cancel teleportation\npoint to the top or to the\n bottom of the screen."
	#define LABEL_HELP_SH			"- passive single-use bonus\n- protect You from the falling\nof the box"

	#define LABEL_HARD				"hard"
	#define LABEL_MEDIUM			"medium"
	#define LABEL_EASY				"easy"
	#define LABEL_BACK				"back"
	#define LABEL_SFX_ON			"sound effects on"
	#define LABEL_SFX_OFF			"sound effects off"
	#define LABEL_MUSIC_ON			"music on"
	#define LABEL_MUSIC_OFF			"music off"
	#define LABEL_RIGHT_HANDED		"right handed"
	#define LABEL_LEFT_HANDED		"left handed"
	#define LABEL_EXIT				"exit?"
	#define LABEL_RESULT1			"your result:"
	#define LABEL_RESULT2			"your scores:"
	#define LABEL_RESULT3			"your achievement:"
	#define LABEL_RECORD			"new record!"
	#define LABEL_OLD_RECORD		"old record:"
	#define LABEL_CUR_RECORD		"current record:"
	#define LABEL_SCORES			"scores:"
	#define LABEL_LEVEL				"level:"
	#define LABEL_GO				"Go!"
	#define LABEL_GOOD_LUCK			"Good luck!"
	#define LABEL_LETS_GO			"Lets go!"
	#define LABEL_CONTINUE			"continue"
	#define LABEL_MAIN_MENU			"main menu"
#endif
// configs

// cups
#define	THIRD_CUP_SCORES		25000
#define	SECOND_CUP_SCORES		50000
#define	FIRST_CUP_SCORES		100000

#define LOW_GRAVITY_DURATION	15.0f
#define CHECK_EMPTY_CELL_SIZE	0.4

// Categories
#define LOADER_CATEGORY			0x1
#define BOX_CATEGORY			0x2
#define SENSOR_CATEGORY			0x4
#define TROPHY_CATEGORY			0x8

// Z-values
#define kSkyZValue				-10
#define kCloudZValue			-9 
#define kBackgroundZValue		-5
#define kGravityParticlesZValue -4
#define kFloorAndSidesZValue	-3
#define kArrowSpriteZValue		97
#define kShieldSpriteZValue		98
#define kLoaderSpriteZValue		100
#define kBoxParticlesZValue		108
#define kBoxSpriteZValue		110
#define kTrophySpriteZValue		112
#define kFabricatorSpriteZValue 120
#define kIndicatorSpriteZValue	130
#define kGameOverBackZValue		200

// Tag-values
#define kLoaderSpriteTagValue 0
#define kBoxSpriteTagValue 1
#define kTrophySpriteTagValue 2
#define kLoaderIdleTimer 3.0f

#define kModelHeight 32.0f
#define kModelWidth 32.0f
#define kSensorMargin 4.0f
#define kSensorHeight 16.0f
#define kSideSensorMargin 4.0f
#define kBottomSensorHeight ( kBoxSide / 5 )
#define kBottomSensorWidth ( kBoxSide / 3 )
#define kBoxPerCell 5
#define kBoxPerSpecialCell 2
#define kBoxCanPlacedMax 15
#define kBoxSeparator 2.0f
#define kAreaWidth ( kBoxCanPlaced * kBoxSide + ( kBoxCanPlaced - 1 ) * kBoxSeparator )
#define kAreaHeight ( kBoxPerCell * kBoxSide )
#define kLeftBorder  ( ( SCREEN_WIDTH - kAreaWidth )/2 )
#define kRightBorder ( ( SCREEN_WIDTH + kAreaWidth )/2 )
#define kSkyAltOffset ( 0.3f + SCREEN_WIDTH/400.0f * 0.025 )

#define kBoxSpawnAltCoefficient 0.6f
#define kLoaderSpawnAltCoefficient 0.6f

#define TROPHIES_INDICATORS_X_OFFSET 0.54f
#define kMaxCloudMoveDuration 10
#define kMinCloudMoveDuration 1
#define SKY_FIRST_OFFSET 5.4f
#define SKY_BETWEEN_WINDOWS 3.2f
#define JUMP_TIME 0.7f
#define FADE_TIME 0.5f
#define BUTTONS_APPEAR_TIME 0.4f
#define TIME_FOR_MOVE 0.5f
#define DEFAULT_LOOKS_RIGHT true
#define DEFAULT_TEXTURE_NAME "walk0000.png"
#define CLOUD_DISPATCHER_TIME 5.0
#define CLOUDS_COUNT 3
#define PTM_RATIO 50.0

#define SMALL_BUF_LEN 32
#define MEDIUM_BUF_LEN 64
#define BIG_BUF_LEN 128

#define TROPHY_DESTROY 0.04f
#define TROPHY_GRAVITY 0.01f
#define TROPHY_TELEPORT 0.01f
#define TROPHY_SHIELD 0.02f

#define	BOX_EMITTER_SPEED ( SCREEN_WIDTH/400.0f * 160.0f )
#define BOX_EMITTER_LIFE 0.5f
#define BOX_EMITTER_SIZE ( SCREEN_WIDTH/400.0f * 5.0f )
#define BOX_EMITTER_SIZE_VAR ( SCREEN_WIDTH/400.0f * 2.5f )

#define	GRAVITY_EMITTER_SPEED ( SCREEN_WIDTH/400.0f * 50.0f * ( -1 ) )
#define GRAVITY_EMITTER_SIZE ( SCREEN_WIDTH/400.0f * 5.0f )
#define GRAVITY_EMITTER_SIZE_VAR ( SCREEN_WIDTH/400.0f * 2.5f )

#define MENU_Y_N_BUTTON_Y_COEFFICIENT 0.40f
#define MENU_Y_BUTTON_X_COEFFICIENT 0.4f
#define MENU_N_BUTTON_X_COEFFICIENT 0.6f
#define MENU_OF_BUTTON_Y_COEFFICIENT 0.90f
#define MENU_BOTTOM_BUTTON_Y_COEFFICIENT 0.15f
#define MENU_LEFT_BUTTON_X_COEFFICIENT 0.1f
#define MENU_RIGHT_BUTTON_X_COEFFICIENT 0.9f

#define FPS_COEFF ( (int(SCREEN_WIDTH/400) - 1 ) * ( -0.25f ) )

#define kJoystickScale			ConstantsClass::sharedConstantsClass()->_JoystickScale
#define k1stButtonOffset		ConstantsClass::sharedConstantsClass()->_1stButtonOffset	
#define k2dButtonOffset			ConstantsClass::sharedConstantsClass()->_2dButtonOffset
#define k3dButtonOffset			ConstantsClass::sharedConstantsClass()->_3dButtonOffset	
#define k4thButtonOffset		ConstantsClass::sharedConstantsClass()->_4thButtonOffset
#define kMarginBottom			ConstantsClass::sharedConstantsClass()->_MarginBottom
#define kPathPrefix				ConstantsClass::sharedConstantsClass()->_PathPrefix
// ������
#define kEngBigFont				ConstantsClass::sharedConstantsClass()->_BigFont.c_str()
#define kEngFont				ConstantsClass::sharedConstantsClass()->_Font.c_str()
#if NEED_RUS
	#define kBigFont			ConstantsClass::sharedConstantsClass()->_RusBigFont.c_str()
	#define kFont				ConstantsClass::sharedConstantsClass()->_RusFont.c_str()
#else
	#define kBigFont			ConstantsClass::sharedConstantsClass()->_BigFont.c_str()
	#define kFont				ConstantsClass::sharedConstantsClass()->_Font.c_str()
#endif
#define kBoxCanPlaced			ConstantsClass::sharedConstantsClass()->_BoxCanPlaced
#define kBoxSide				ConstantsClass::sharedConstantsClass()->_BoxSide
#define kSensorDepth			ConstantsClass::sharedConstantsClass()->_SensorDepth
#define kJumpImpulse			ConstantsClass::sharedConstantsClass()->_JumpImpulse 
#define kDeltaWalk				ConstantsClass::sharedConstantsClass()->_DeltaWalk
#define kGravity				ConstantsClass::sharedConstantsClass()->_Gravity
#define GET_PATH				ConstantsClass::sharedConstantsClass()->getPath
#define INIT_CONSTANTS			ConstantsClass::sharedConstantsClass()->_initConstants();

// 32   15   10
// 48   >16  10
// 56   >17  10
// 72   >17  10




typedef enum {
	kDestroyBox=0,
	kLowGravity=1,
	kTeleport=2,
	kShield=3,
} TrophyTypes;

typedef enum {
    kMenu=1,
    kScenes=2,
} MainMenuState;

typedef enum {
	kIdle=0,
    kWalking=1,
    kJumping=2,
    kBreathing=3,
    kWaiting=4,
    kMoving=5   
} CharacterStates;


typedef enum {
    kObjectTypeNone,
    kLoaderType,
	kBoxType,
    kGroundType,	
	kMovingBackSensorType,
	kLeftSensorType,
	kRightSensorType,
	kBottomSensorType,
	kTrophyType
} GameObjectType;

typedef enum {
	kEasy=0,
	kMedium=1,
	kHard=2
} GameComplexityType;

#define kMainMenuTagValue 10
#define kSceneMenuTagValue 20

typedef enum {
    kNoSceneUninitialized=0,
    kMainMenuScene=1,
    kOptionsScene=2,
    kCreditsScene=3,
	kHelpScene=4,
	kHighscoresScene=5,
    kGreetingScene=6,
    kLevelCompleteScene=7,
    kExitScene=8,
    kGameLevel1=101
} SceneTypes;

typedef enum {
    kNoDirection=0,
	kLeft=1,
	kRight=2
} DirectionTypes;

typedef enum {
	kNone=0,
    kCorrectNeeded=1,
	kFall=2
} CorrectionTypes;

// Screen size macros
#define SCREEN_SIZE ( cocos2d::CCDirector::sharedDirector()->getWinSize() )
#define SCREEN_WIDTH ( cocos2d::CCDirector::sharedDirector()->getWinSize().width )
#define SCREEN_HEIGHT ( cocos2d::CCDirector::sharedDirector()->getWinSize().height )

#define SCREEN_SIZE_PX ( cocos2d::CCDirector::sharedDirector()->getWinSizeInPixels() )
#define SCREEN_WIDTH_PX ( cocos2d::CCDirector::sharedDirector()->getWinSizeInPixels().width )
#define SCREEN_HEIGHT_PX ( cocos2d::CCDirector::sharedDirector()->getWinSizeInPixels().height )

// Audio Items
#define AUDIO_MAX_WAITTIME 150

typedef enum {
    kAudioManagerUninitialized=0,
    kAudioManagerFailed=1,
    kAudioManagerInitializing=2,
    kAudioManagerInitialized=100,
    kAudioManagerLoading=200,
    kAudioManagerReady=300
} GameManagerSoundState;


// Background Music

// Menu Scenes
#define BACKGROUND_MUSIC1 "music1.mp3"
#define BACKGROUND_MUSIC2 "music2.mp3"

// Game Scene

#define WALK_LENGTH 2.0f

// Audio Constants
#define SFX_NOTLOADED false
#define SFX_LOADED true

#define PLAYSOUNDEFFECT( ... ) \
	GameManager::sharedGameManager()->playSoundEffect( #__VA_ARGS__ )

#define STOPSOUNDEFFECT( ... ) \
	GameManager::sharedGameManager()->stopSoundEffect( __VA_ARGS__ )

#endif