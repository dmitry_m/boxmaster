#include "GameObject.h"
#include <vector>

using namespace cocos2d;

bool GameObject::init() 
{
	bool pRet = false;

	if( CCSprite::init() )
	{
		CCLOG( "GameObject init" );
		screenSize = CCDirector::sharedDirector()->getWinSize();
		isActive = true;
		gameObjectType = kObjectTypeNone;
		pRet = true;
	}
	return pRet;
}

void GameObject::changeState( CharacterStates newState )
{
	CCLOG( "GameObject->changeState method should be overridden" );
}

void GameObject::updateStateWithDeltaTime( ccTime deltaTime, 
							  CCArray* listOfGameObjects )
{
    CCLOG( "updateStateWithDeltaTime method should be overridden" );
}

CCRect GameObject::adjustedBoundingBox()
{
    CCLOG( "GameObect adjustedBoundingBox should be overridden" );
	return this->boundingBox();
}



CCAnimation* GameObject::loadPlistForAnimationWithName( const char* animationName, const char* className )
{
	//1: Getting path
	CCAnimation *animationToReturn = NULL;
	const char*  plistPath = CCFileUtils::fullPathFromRelativePath( className );

	// 2: Read in the plist file
	CCDictionary<string, CCObject*> *plistDictionary = 
		CCFileUtils::dictionaryWithContentsOfFile( plistPath );

	// 3: If the plistDictionary was null, the file was not found.
	if ( plistDictionary == NULL ) {
		CCLOG( "Error reading plist: %s", className );
		return NULL; // No Plist Dictionary or file found
    }
	// 4: Get just the mini-dictionary for this animation

	CCDictionary<string, CCString*> *animationSettings = 
		( CCDictionary<string, CCString*>* ) plistDictionary->objectForKey( animationName );
	if ( animationSettings == NULL ) {
		CCLOG( "Could not locate AnimationWithName: %s",animationName );
		return NULL;
    }

	// 5: Get the delay value for the animation
	float animationDelay = animationSettings->objectForKey( "delay" )->toFloat();

    animationToReturn = CCAnimation::animation();
    animationToReturn->setDelay( animationDelay );

	// 6: Add the frames to the animation
	string animationFramePrefix = ( animationSettings->objectForKey( std::string( "filenamePrefix" ) ) )->toStdString();
	string animationFrames = ( animationSettings->objectForKey( "animationFrames" ) )->toStdString();
	
	vector<string> animationFrameNumbers;
	int startpos=0;
	for( int i = 0; i < ( int )animationFrames.length(); i++ )
	{
		if( animationFrames.at( i ) == ',' )
		{
			animationFrameNumbers.push_back( animationFrames.substr( startpos, i - startpos ) );
			startpos = i+1;
		}
		else if( i==animationFrames.length()-1 )
			animationFrameNumbers.push_back( animationFrames.substr( startpos, i - startpos + 1 ) );
	}
	

	string frameNumber;
	char frameName[100];
	for ( vector<string>::iterator iter = animationFrameNumbers.begin(); iter != animationFrameNumbers.end(); iter++ ) 
	{
		strcpy( frameName, "" );
		frameNumber = *iter;	
		sprintf( frameName, "%s%s.png", animationFramePrefix.c_str(), ( *iter ).c_str() );
        animationToReturn->addFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->
									spriteFrameByName( frameName ) );
    }
	return animationToReturn;

}