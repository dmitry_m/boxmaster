#ifndef GAMECHARACTER_H
#define GAMECHARACTER_H

#include "GameObjects\GameObject.h"

class GameCharacter : public GameObject {
	CC_SYNTHESIZE( int, characterHealth, CharacterHealth );
    CC_SYNTHESIZE( CharacterStates, characterState, CharacterState );

	~GameCharacter();
	virtual CorrectionTypes checkSpritePosition();
};

#endif