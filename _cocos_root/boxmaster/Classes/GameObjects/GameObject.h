#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <stdio.h>
#include <string.h>

#include "Constants\Constants.h"
#include "Constants\Commonprotocols.h"
#include "Singletons\GameManager.h"

using namespace cocos2d;

class GameObject: public CCSprite {	
public:
	CC_SYNTHESIZE( bool, isActive, IsActive );
	CC_SYNTHESIZE( bool, reactsToScreenBoundaries, ReactsToScreenBoundaries );
	CC_SYNTHESIZE( CCSize, screenSize, ScreenSize );
	CC_SYNTHESIZE( GameObjectType, gameObjectType, GameObjectType );
	
	virtual bool init();
	virtual void changeState( CharacterStates newState );
	virtual void updateStateWithDeltaTime( ccTime deltaTime, CCArray* listOfGameObjects );
	virtual CCRect adjustedBoundingBox();
	static CCAnimation* loadPlistForAnimationWithName( const char* animationName, const char* className );
};


#endif