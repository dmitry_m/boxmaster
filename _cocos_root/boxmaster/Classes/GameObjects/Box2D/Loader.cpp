#include "Loader.h"


Loader::Loader() {	
	walkSoundID = 0;
	jumpSoundID = 0;
	pushForwardSoundId = 0;
	pushBackwardSoundId = 0;

	leftSensorObjects = 0;
	bottomSensorObjects = 0;
	rightSensorObjects = 0;	
}

Loader::~Loader() {	
	lSensor->release();
	rSensor->release();
	bSensor->release();
	mbSensor->release();
}

bool Loader::initWithWorld( b2World *theWorld, CCPoint location, Scene1ActionLayer* sceneLayer ) {
	bool pRet = false;

	if( Box2DSprite::init() ) {
#if NEED_TR
		destroyBoxTrophy = false;
		lowGravityTrophy = false;
		teleportTrophy = false;
		shieldTrophy = false;
#endif

		layer = sceneLayer;
		world = theWorld;
		initAnimations();

		//landed = true;
		this->setDisplayFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( DEFAULT_TEXTURE_NAME ) );
		width = boundingBox().size.width;
		height = boundingBox().size.height;

		gameObjectType = kLoaderType;
		characterHealth = 101.0f;
		characterState = kIdle;
		this->createBodyAtLocation( location );
		this->createSensors();
		falling = false;
		pRet = true;

		ccBlendFunc ccBF;
		ccBF.dst = GL_ZERO;
		ccBF.dst = GL_ZERO;
		this->setBlendFunc( ccBF );
		//this->setIsVisible( false );
	}
	return pRet;
}

CorrectionTypes Loader::checkSpritePosition() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	// clamp if out of border
	CCPoint curPos = V2P( body->GetPosition() );

	if( curPos.y >= kMarginBottom + kAreaHeight + 1.0 *kBoxSide ) {
		curPos.y = kMarginBottom + kAreaHeight + 1.0 *kBoxSide;
		body->SetTransform( P2V( curPos ), 0.0f );
		body->SetLinearVelocity( b2Vec2( 0.0f, -0.2f ) );
	}

	if( curPos.x < kLeftBorder ) {
		curPos.x = kLeftBorder;
		body->SetTransform( P2V( curPos ), 0.0f );
	}
	else if( curPos.x > kRightBorder ) {
		curPos.x = kRightBorder;
		body->SetTransform( P2V( curPos ), 0.0f );
	}
	body->SetTransform( body->GetPosition(), 0.0f );
	
	leftIsEmpty = leftSensorObjects <= 0 ? true : false;
	bottomIsEmpty = bottomSensorObjects <= 0 ? true : false;
	rightIsEmpty = rightSensorObjects <= 0 ? true : false;
	
	return kNone;
}

void Loader::updateStateWithDeltaTime( ccTime deltaTime )
{
	/*
		WARNING: code was corrupted and partially lost!!!
	*/

	if (jumpTimeout <= 0.0f) {		
		flyMode = bottomIsEmpty;
		falling = flyMode;
	}
	else if (jumpTimeout > -1.0f) {
		jumpTimeout -= deltaTime;
	}


	if (jumpQuery) {
		tryJump();
	}

	applyDirection(deltaTime);
	
}

void Loader::changeState( CharacterStates newState )
{
	/*
	this->stopAllActions();
	this->setCharacterState( newState );

	switch ( newState ) {
		case kStateTakingDamage:
			{
//				this->playHitEffect();
				characterHealth = characterHealth - 10;
				break;
			}
		default:
			break;
	}
	*/
}

bool Loader::BoxAtSide( DirectionTypes side ) {
	float dir = side == kLeft ? -1.0f : 1.0f;

	Coordinates loaderCoordinates = P2C( getPosition() );	
	Coordinates boxCoordinates = Coordinates( loaderCoordinates.x + direction,  loaderCoordinates.y );
	Coordinates nextCoordinates = Coordinates( loaderCoordinates.x + 2 * direction,  loaderCoordinates.y );
	
	bool error = false;
	//if( ( ( int )nextCoordinates.x < 0 || ( int )nextCoordinates.x >= kBoxCanPlaced ) || layer->getBoxByCoordinates( nextCoordinates ) != NULL ) 
	if( layer->getBoxByCoordinates( boxCoordinates, error ) != NULL )  {
		return true;
	}
	else {
		return false;
	}
}

void Loader::PrepareButtons() {
	if( characterState == kMoving ) {
		return;
	}

	if( !BoxAtSide( kLeft ) ) {
		layer->setDirButtonTexture( kLeft, true );
	}
	else {
		layer->setDirButtonTexture( kLeft, leftIsEmpty );		
	}

	if( !BoxAtSide( kRight ) ) {
		layer->setDirButtonTexture( kRight, true );
	}
	else {
		layer->setDirButtonTexture( kRight, rightIsEmpty );		
	}

	bool backMoveVisible = false;
	if( BoxAtSide( kLeft ) && !leftIsEmpty && layer->getLastDirection() == kLeft && !flyMode ) {
		layer->setMoveButtonTexture( kRight );
		backMoveVisible = true;
	}
	else if( BoxAtSide( kRight ) && !rightIsEmpty && layer->getLastDirection() == kRight && !flyMode ) {
		layer->setMoveButtonTexture( kLeft );
		backMoveVisible = true;
	}
	layer->setMoveButtonVisible( backMoveVisible );
}

void Loader::applyDirection( ccTime deltaTime ) {
	
//	preparing ----------------------------------------------------------------

	if( characterState == kMoving ) {
		return;
	}

	PrepareButtons();
	// exit if no direction
	
	static float breathWaitingTime = 0.0f;
	if( currentDirection == kNoDirection ) {
		lastDirection = currentDirection;
		if( characterState != kBreathing ) {
			characterState = kIdle;
		}

		if( !flyMode ) {
			if( breathWaitingTime > 3.0f ) {
				this->setCharacterState( kBreathing );
				setAnim( randomBreathAnim(), true );
				breathWaitingTime = -3.5f;
			}
			else {
				if( getCharacterState() != kBreathing ) {
					setAnim( 0, true ); 
				}
				breathWaitingTime += deltaTime;
			}
		}
		return;
	}
	else {
		breathWaitingTime = 0.0f;
	}
	
	static double waitingTime = 0.0;
	// wait 100ms before moving boxes
	if( !flyMode && currentDirection == lastDirection && characterState == kWaiting ) {
		if( waitingTime > 0.1 ) {
			characterState = kMoving;
		}
		else {
			waitingTime += deltaTime;
			return;
		}
	}
	else {
		waitingTime = 0.0f; 
	}

	if( lastDirection != currentDirection ) {
		// setup flipX
		if( currentDirection == kLeft ) {
			this->setFlipX( DEFAULT_LOOKS_RIGHT );
		}
		else if( currentDirection == kRight ) {
			this->setFlipX( !DEFAULT_LOOKS_RIGHT );
		}
	}
	lastDirection = currentDirection; 
	
	// strenght and direction setup	
	direction = ( currentDirection == kLeft ) ? -1.0f : 1.0f;
	float delta = kDeltaWalk;
		

	// slow, if is in air
	if( flyMode ) {
		delta *= 0.5f;
	}

//	checking state ----------------------------------------------------------


	//static int count = 0;
	if( characterState != kMoving &&
		( ( currentDirection == kLeft && !leftIsEmpty ) || 
		  ( currentDirection == kRight && !rightIsEmpty ) ) ) {
			 
		// there are anything at left
		if( !flyMode ) {
			setAnim( 0, true );
		}

		if( !flyMode &&
		   ( ( currentDirection == kLeft &&  BoxAtSide( kLeft ) ) || 
			( currentDirection == kRight && BoxAtSide( kRight ) ) ) ) {

		//	if( count > 10 ) {
				characterState = kWaiting;
				waitingTime = 0.0f; 
		//		count = 0;
		//	}
		//	else {
		//		count++;
		//	}
		}
		return;
	}

//	applying -----------------------------------------------------------------

	// move himself

	if( characterState == kMoving ) {		
		StartMovingBox();			
	}
	else {
		// setup to walking
		if( !flyMode &&
			( characterState == kBreathing || characterState == kIdle ||
			( this->numberOfRunningActions() == 0 ) ) ) {

			setAnim( walkingAnim, true );
		}
		characterState = kWalking;		
		b2Vec2 deltaVec( direction * delta/PTM_RATIO, 0.0f );
		b2Vec2 oldPosition = body->GetPosition();
		b2Vec2 newPosition = oldPosition + deltaVec;
		lastPosition = V2P( body->GetPosition() );
		body->SetTransform( newPosition, 0.0f );
	} 
}


void Loader::StartMovingBox() {
	back = layer->getBackMoveButtonPressed() ? -1 : 1;
	Coordinates loaderCoordinates = P2C( getPosition() );
	Coordinates nextLoaderCoordinates = Coordinates( loaderCoordinates.x + back * direction,  loaderCoordinates.y );
	Coordinates boxCoordinates = Coordinates( loaderCoordinates.x + direction,  loaderCoordinates.y );
	Coordinates upperBoxCoordinates = Coordinates( boxCoordinates.x, boxCoordinates.y + 1 );
	
	bool error = false;
	boxToMove = layer->getBoxByCoordinates( boxCoordinates, error );

//	layer->alignToCenterOfCell( boxToMove->getBody() );
//	layer->alignToCenterOfCell( this->body );

	direction *= back;		
	float offset = ( kBoxSide + kBoxSeparator ) * direction;

	loaderStartPosition = getPosition();
	boxStartCoordinates = boxCoordinates;
	loaderEndPosition = ccp( loaderStartPosition.x + offset, loaderStartPosition.y );
	boxEndCoordinates = Coordinates( boxStartCoordinates.x + direction,  loaderCoordinates.y );

	loaderRelativeBoxOffset = ( loaderStartPosition.x - C2P( boxStartCoordinates ).x )/ PTM_RATIO;


	// if there are no free space
	if( !boxToMove ||
		boxToMove->getStrong() ||
		boxEndCoordinates.x < 0 || 
		boxEndCoordinates.x >= kBoxCanPlaced ||  
		nextLoaderCoordinates.x < 0 || 
		nextLoaderCoordinates.x >= kBoxCanPlaced ||
		layer->getBoxByCoordinates( upperBoxCoordinates ) || 
		!layer->isReallyEmptyCell( boxEndCoordinates ) ||  
		( back < 0 && layer->getBoxByCoordinates( nextLoaderCoordinates, error ) ) ) {              // there are box at upper position

		characterState = kIdle;
		boxToMove = 0;
	}
	else {	

		fallingMoving = false;

		
	
		// move and plug box to new position
		layer->unplugBoxByCoordinates( boxCoordinates );
		layer->plugBoxByCoordinates( boxToMove, boxEndCoordinates );	


		// special bodies
		CCPoint midPos = C2P( boxStartCoordinates );
		midPos.x += offset / 2;
		layer->getSpecialBody()->SetTransform( P2V( midPos ), 0.0f );
		if( back < 0.0f ) {
		//	mbSensor->getBody()->SetTransform( C2V( nextLoaderCoordinates ), 0.0f );
		}
		playPushForwardSound();

		// move sprites
		CCAnimation *pushAnim = back > 0 ? pushForwardAnim : pushBackwardAnim;
		this->runAction( CCAnimate::actionWithAnimation( pushAnim, true ) );

	}
}

void Loader::ProcessMoving( float dt ) {

	b2Body* lb = this->getBody();
	b2Body* bb = this->getBoxToMove()->getBody();

	b2Vec2 lPos = lb->GetPosition();
	b2Vec2 bPos = bb->GetPosition();

	b2Vec2 lTarget = P2V( loaderEndPosition );
	b2Vec2 bTarget = C2V( boxEndCoordinates );

	Coordinates loaderCoordinates = V2C( lPos );
	Coordinates nextLoaderCoordinates = V2C( lTarget );
	Coordinates boxCoordinates = V2C( bPos );
	Coordinates nextBoxCoordinates = V2C( bTarget );
	
	if( ( direction > 0 && bPos.x < bTarget.x ) ||
		( direction < 0 && bTarget.x < bPos.x ) ) {
		
		float delta = direction * kDeltaWalk / PTM_RATIO;
		delta *= back > 0 ? 0.6f : 0.5f;
		bPos.x += delta;

		lb->SetTransform( b2Vec2( bPos.x + loaderRelativeBoxOffset, lPos.y ), 0.0f );
		bb->SetTransform( b2Vec2( bPos.x, fallingMoving ? lPos.y : bPos.y ), 0.0f );

		layer->getSpecialBody()->SetTransform( b2Vec2( layer->getSpecialBody()->GetPosition().x,  fallingMoving ? lPos.y : bPos.y ), 0.0f );	

	
		if( !fallingMoving &&
			!layer->getBoxByCoordinates( Coordinates( loaderCoordinates.x, loaderCoordinates.y - 1 ) ) &&
			!layer->getBoxByCoordinates( Coordinates( nextLoaderCoordinates.x, nextLoaderCoordinates.y - 1 ) ) &&
			!layer->getBoxByCoordinates( Coordinates( boxCoordinates.x, boxCoordinates.y - 1 ) ) &&
			!layer->getBoxByCoordinates( Coordinates( nextBoxCoordinates.x, nextBoxCoordinates.y - 1 ) ) ) {

				fallingMoving = true;
		}
		
		if( back < 0.0f ) {
		//	mbSensor->getBody()->SetTransform( b2Vec2( mbSensor->getBody()->GetPosition().x, lPos.y ), 0.0f );	
			if( layer->getBoxByCoordinates( P2C( loaderEndPosition ) ) != 0 ) {
				bool sideIsEmpty = direction < 0 ? leftIsEmpty : rightIsEmpty;
				if( !sideIsEmpty ) {
					 back = 1.0f;
					 direction *= -1;

					 CCPoint tempPoint = loaderStartPosition;
					 loaderStartPosition = loaderEndPosition;
					 loaderEndPosition = tempPoint;

					 Coordinates tempCoordinates = boxStartCoordinates;
					 boxStartCoordinates = boxEndCoordinates;
					 boxEndCoordinates = tempCoordinates;

					 layer->unplugBoxByCoordinates( boxEndCoordinates );
					 layer->plugBoxByCoordinates( boxToMove, boxStartCoordinates );	
				}
									
			}
		}
	}
	else {
		bb->SetTransform( b2Vec2( bTarget.x, fallingMoving ? lPos.y : bPos.y ), 0.0f );
		lb->SetTransform( b2Vec2( bTarget.x + loaderRelativeBoxOffset, lPos.y ), 0.0f );
		EndMovingBox();
	}
}

void Loader::EndMovingBox() {
	layer->alignToCenterOfCell( boxToMove->getBody() );

	Coordinates boxCoordinates =  P2C( boxToMove->getPosition() );
	
	checkSpritePosition();

	layer->getSpecialBody()->SetTransform( b2Vec2( -100.0f/PTM_RATIO, -100.f/PTM_RATIO ), 0.0f );
	mbSensor->getBody()->SetTransform(  b2Vec2( -100.0f/PTM_RATIO, -200.0f ), 0.0f );


	// if loader is under box
	Coordinates coordinatesForCheck = P2C( getPosition() );
	coordinatesForCheck.y++;
	if( layer->getBoxByCoordinates( coordinatesForCheck ) ) {
		layer->checkForServive( layer->getBoxByCoordinates( coordinatesForCheck ) );
	}
	boxToMove = 0;
	characterState = kIdle;
}


/*
void Loader::StartMovingBox( int direction ) {
	back = layer->getBackMoveButtonPressed() ? -1 : 1;
	Coordinates loaderCoordinates = P2C( getPosition() );
	Coordinates nextLoaderCoordinates = Coordinates( loaderCoordinates.x + back * direction,  loaderCoordinates.y );
	Coordinates boxCoordinates = Coordinates( loaderCoordinates.x + direction,  loaderCoordinates.y );
	Coordinates upperBoxCoordinates = Coordinates( boxCoordinates.x, boxCoordinates.y + 1 );

	backupLoaderPosition = getPosition();
	backupBoxCoordinates = boxCoordinates;

	bool error = false;
	boxToMove = layer->getBoxByCoordinates( boxCoordinates, error );
	direction *= back;	
	Coordinates nextCoordinates = Coordinates( boxCoordinates.x + direction,  loaderCoordinates.y );
	
	float offset = ( kBoxSide + kBoxSeparator ) * direction;


	// there are free space
	if( !boxToMove ||
		nextCoordinates.x < 0 || 
		nextCoordinates.x >= kBoxCanPlaced ||  
		nextLoaderCoordinates.x < 0 || 
		nextLoaderCoordinates.x >= kBoxCanPlaced ||
		layer->getBoxByCoordinates( upperBoxCoordinates, error ) || 
		layer->getBoxByCoordinates( nextCoordinates, error ) ||  
		( back < 0 && layer->getBoxByCoordinates( nextLoaderCoordinates, error ) ) ) {              // there are box at upper position

		characterState = kIdle;
		boxToMove = 0;
	}
	else {		
		layer->alignToCenterOfCell( boxToMove->getBody() );
		layer->alignToCenterOfCell( this->body );

		b2Vec2 movingLoaderPosition = b2Vec2( body->GetPosition().x + back * direction * 4 / PTM_RATIO, body->GetPosition().y );
		body->SetTransform( movingLoaderPosition, 0.0f );

		CCAnimation *pushAnim;
		if( back > 0 ) {
			pushAnim = pushForwardAnim;
		} else {
			pushAnim = pushBackwardAnim;
		}

		// move and plug box to new position
		layer->unplugBoxByCoordinates( boxCoordinates );
		layer->plugBoxByCoordinates( boxToMove, nextCoordinates );	

		layer->getSpecialBody()->SetTransform( boxToMove->getBody()->GetPosition(), 0.0f );
		boxToMove->getBody()->SetTransform( C2V( nextCoordinates ), 0.0f );
		if( back < 0.0f ) {
			mbSensor->getBody()->SetTransform( C2V( nextLoaderCoordinates ), 0.0f );
		}

		// move sprites
		movingLoaderAction = CCSequence::actions( 
			CCSpawn::actions( 
				CCMoveTo::actionWithDuration( TIME_FOR_MOVE, ccp( getPosition().x + offset, getPosition().y ) ), 
				CCAnimate::actionWithAnimation( pushAnim, true ),
				CCSequence::actions( 
					CCActionInterval::actionWithDuration( TIME_FOR_MOVE / 2 ),
					CCCallFunc::actionWithTarget( this, callfunc_selector( Loader::CheckForContinueMovingBox ) ), 
					NULL ),					
				NULL ),				
			CCCallFunc::actionWithTarget( this, callfunc_selector( Loader::EndMovingBox ) ), 
			NULL );

		movingBoxAction = CCMoveTo::actionWithDuration( TIME_FOR_MOVE, C2P( nextCoordinates ) );
		
		dangerousMoving = false;
		secondPartOfMoving = false;
		//DirectionTypes dir = direction < 0 ? kLeft : kRight;
		//layer->setDirButtonIsVisible( dir, false );
		boxToMove->runAction( movingBoxAction );
		this->stopAllActions();
		this->runAction( movingLoaderAction );		
	}
}

void Loader::CheckForContinueMovingBox() {
	// interrupt moving and start moving back
	if( dangerousMoving ) {
		this->stopAllActions();
		boxToMove->stopAllActions();
		
		// invert animation
		CCAnimation *pushAnim;
		if( back < 0 ) {
			pushAnim = pushForwardAnim;
		} else {
			pushAnim = pushBackwardAnim;
		}
		
		movingLoaderAction = CCSequence::actions( 
			CCSpawn::actions( CCMoveTo::actionWithDuration( TIME_FOR_MOVE/2, backupLoaderPosition ), 
				CCAnimate::actionWithAnimation( pushAnim, true ),
				NULL ),				 
			CCCallFunc::actionWithTarget( this, callfunc_selector( Loader::EndMovingBox ) ), 
			NULL );

		movingBoxAction = CCMoveTo::actionWithDuration( TIME_FOR_MOVE/2, C2P( backupBoxCoordinates ) );
		
		layer->plugBoxByCoordinates( layer->unplugBoxByCoordinates( V2C( boxToMove->getBody()->GetPosition() ) ), backupBoxCoordinates );

		layer->getSpecialBody()->SetTransform( boxToMove->getBody()->GetPosition(), 0.0f );
		boxToMove->getBody()->SetTransform( C2V( backupBoxCoordinates ), 0.0f );
		mbSensor->getBody()->SetTransform( C2V( -5, 0 ), 0.0f );

		runAction( movingLoaderAction );
		boxToMove->runAction( movingBoxAction );
	}
	else {
		secondPartOfMoving = true;
	}
}

void Loader::movingBackSensorDetected() {
	if( !secondPartOfMoving ) {
		dangerousMoving = true;
	}
	else {
		bool error = false;
		layer->checkForServive( layer->getBoxByCoordinates( V2C( mbSensor->getBody()->GetPosition() ), error ) );
	}
}

void Loader::EndMovingBox() {
	layer->alignToCenterOfCell( boxToMove->getBody() );

	DirectionTypes dir = direction < 0 ? kLeft : kRight;
	Coordinates boxCoordinates =  P2C( boxToMove->getPosition() );
	
	checkSpritePosition();

	bool error = false;

	layer->getSpecialBody()->SetTransform( b2Vec2( -100.0f/PTM_RATIO, -100.f/PTM_RATIO ), 0.0f );
	mbSensor->getBody()->SetTransform(  b2Vec2( -100.0f/PTM_RATIO, -200.0f ), 0.0f );

	// if loader is in box
	if( layer->getBoxByCoordinates( P2C( getPosition() ), error ) ) {
		layer->checkForServive( layer->getBoxByCoordinates( P2C( getPosition() ), error ) );
	}

	// if loader is under box
	Coordinates coordinatesForCheck = P2C( getPosition() );
	coordinatesForCheck.y++;
	if( layer->getBoxByCoordinates( coordinatesForCheck, error ) ) {
		layer->checkForServive( layer->getBoxByCoordinates( coordinatesForCheck, error ) );
	}

	boxToMove = 0;
	characterState = kIdle;
}
*/

void Loader::createBodyAtLocation( CCPoint location ) {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	b2BodyDef bodyDef;
	//bodyDef.type = b2_staticBody;	
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = b2Vec2( location.x/PTM_RATIO, location.y/PTM_RATIO );
	bodyDef.allowSleep = false;
	body = world->CreateBody( &bodyDef );

	// connecting body with sprite
	body->SetUserData( this );

	// creating fixture as box
	b2FixtureDef fixtureDef;
	b2PolygonShape shape;
	
	shape.SetAsBox( ( width  )/2/PTM_RATIO, ( height - 2 )/2/PTM_RATIO );
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.5f;
	fixtureDef.friction = 1.0f;
	fixtureDef.restitution = 0.0f;
	
	fixtureDef.filter.categoryBits = LOADER_CATEGORY;
	fixtureDef.filter.maskBits = BOX_CATEGORY;
	fixtureDef.filter.groupIndex = -1;
	
	// connect fixture to body
	body->CreateFixture( &fixtureDef ); 
}

void Loader::createSensors() {
	CCSize sensorSize = CCSizeMake( 5, kModelHeight );
	b2BodyDef bodyDef;
	
	b2FixtureDef fixtureDef;
	fixtureDef.filter.categoryBits = SENSOR_CATEGORY;
	fixtureDef.filter.maskBits = BOX_CATEGORY;
	fixtureDef.filter.groupIndex = -1;
	
	// creating left sensor	---------------------------------------------------------
    bodyDef.type = b2_dynamicBody;	
	bodyDef.allowSleep = false;
	
	bodyDef.position = body->GetWorldPoint( b2Vec2( -( kModelWidth )/2/PTM_RATIO , 0/PTM_RATIO ) );
	b2Body* leftSensor = world->CreateBody( &bodyDef );
		
	// creating sensor looker
	lSensor = new Sensor();
	lSensor->init( kLeftSensorType );
	lSensor->setIsVisible( false );
	
	//lSensor->autorelease();
	//this->addChild( lSensor );

	// linking body with sprite
	leftSensor->SetUserData( lSensor );
	lSensor->setBody( leftSensor );

	b2PolygonShape shape;
	shape.SetAsBox( kSensorDepth/2/PTM_RATIO, ( height - 2*kSideSensorMargin )/2/PTM_RATIO );
	
    fixtureDef.shape = &shape;
    fixtureDef.isSensor = true;
	leftSensor->CreateFixture( &fixtureDef );
	
	// creating right sensor ---------------------------------------------------------
    bodyDef.type = b2_dynamicBody;	
	bodyDef.position = body->GetWorldPoint( b2Vec2( ( kModelWidth )/2/PTM_RATIO , 0/PTM_RATIO ) );
	
	b2Body* rightSensor = world->CreateBody( &bodyDef );
	

	// creating sensor looker
	rSensor = new Sensor();
	rSensor->init( kRightSensorType );
	rSensor->setIsVisible( false );
	//rSensor->autorelease();
	//this->addChild( rSensor );

	// linking body with sprite
	rightSensor->SetUserData( rSensor );
	rSensor->setBody( rightSensor );
	
    shape.SetAsBox( kSensorDepth/2/PTM_RATIO, ( height - 2*kSideSensorMargin )/2/PTM_RATIO );
    fixtureDef.shape = &shape;
    fixtureDef.isSensor = true;
	rightSensor->CreateFixture( &fixtureDef );
	
	// creating bottom sensor ---------------------------------------------------------
    bodyDef.type = b2_dynamicBody;	
	bodyDef.position = body->GetWorldPoint( b2Vec2( 0/PTM_RATIO, -( kModelHeight + kSensorDepth )/2/PTM_RATIO ) );
	b2Body* bottomSensor = world->CreateBody( &bodyDef );
	
	
	// creating sensor looker
	bSensor = new Sensor();
	bSensor->init( kBottomSensorType );
	bSensor->setIsVisible( false );
	//bSensor->autorelease();
	//this->addChild( bSensor );

	// linking body with sprite
	bottomSensor->SetUserData( bSensor );
	bSensor->setBody( bottomSensor );

    shape.SetAsBox( ( width/2 )/2/PTM_RATIO, kBottomSensorHeight/2/PTM_RATIO );
    fixtureDef.shape = &shape;
    fixtureDef.isSensor = true;
	bottomSensor->CreateFixture( &fixtureDef );
	
	// creating moving back sensor ------------------------------------------------------
    bodyDef.type = b2_staticBody;	
	bodyDef.position = body->GetWorldPoint( C2V( -3, 0 ) );
	b2Body* movingBackSensor = world->CreateBody( &bodyDef );
		
	// creating sensor looker
	mbSensor = new Sensor();
	mbSensor->init( kMovingBackSensorType );
	mbSensor->setIsVisible( false );
	//bSensor->autorelease();
	//this->addChild( mbSensor );

	// linking body with sprite
	movingBackSensor->SetUserData( mbSensor );
	mbSensor->setBody( movingBackSensor );

    shape.SetAsBox(kBoxSide/2/PTM_RATIO, kBoxSide/2/PTM_RATIO );
    fixtureDef.shape = &shape;
    fixtureDef.isSensor = true;
	movingBackSensor->CreateFixture( &fixtureDef );
}

void Loader::refreshSensors() {
	lSensor->getBody()->SetTransform( body->GetWorldPoint( b2Vec2( -( width + kSensorMargin + kSensorDepth )/2/PTM_RATIO, 0.0f ) ), 0.0f );
	rSensor->getBody()->SetTransform( body->GetWorldPoint( b2Vec2( ( width + kSensorMargin + kSensorDepth )/2/PTM_RATIO, 0.0f ) ), 0.0f );
	bSensor->getBody()->SetTransform( body->GetWorldPoint( b2Vec2( 0.0f, -( height + kBottomSensorHeight + 2 )/2/PTM_RATIO ) ), 0.0f );
}

bool Loader::tryJump() {
	if( !flyMode && characterState != kMoving ) {
		characterState = kJumping;   // conditiously
		b2Vec2 impulse = b2Vec2( 0.0, body->GetMass() * kJumpImpulse );
		b2Vec2 impulsePoint = 
			body->GetWorldPoint( b2Vec2( 0.0f / ( PTM_RATIO ), 0.0f / ( PTM_RATIO ) ) );
		body->SetLinearVelocity( b2Vec2( 0.0f, 0.0f ) );
		body->ApplyLinearImpulse( impulse, impulsePoint );

		jumpTimeout = 0.1f;
		flyMode = falling = true;

		// setup to jumping
		setAnim( jumpingStartAnim, false );
		
		playJumpSound();
		return true;
	} 
	return false;
}

void Loader::initAnimations() {
	
	this->setPushForwardAnim( this->loadPlistForAnimationWithName( "pushForwardAnim", LOADER_ANIM_PLIST ) );
	if( pushForwardAnim ) {
		pushForwardAnim->retain();
	}	
	this->setPushBackwardAnim( this->loadPlistForAnimationWithName( "pushBackwardAnim", LOADER_ANIM_PLIST ) );
	if( pushBackwardAnim ) {
		pushBackwardAnim->retain();
	}
	this->setWalkingAnim( this->loadPlistForAnimationWithName( "walkingAnim", LOADER_ANIM_PLIST ) );
	if( walkingAnim ) {
		walkingAnim->retain();
	}
	this->setJumpingStartAnim( this->loadPlistForAnimationWithName( "jumpingStartAnim", LOADER_ANIM_PLIST ) );
	if( jumpingStartAnim ) {
		jumpingStartAnim->retain();
	}
	this->setJumpingEndAnim( this->loadPlistForAnimationWithName( "jumpingEndAnim", LOADER_ANIM_PLIST ) );
	if( jumpingEndAnim ) {
		jumpingEndAnim->retain();
	}
	this->setBreathing1Anim( this->loadPlistForAnimationWithName( "breath1", LOADER_ANIM_PLIST ) );
	if( breathing1Anim ) {
		breathing1Anim->retain();
	}
	this->setBreathing2Anim( this->loadPlistForAnimationWithName( "breath2", LOADER_ANIM_PLIST ) );
	if( breathing2Anim ) {
		breathing2Anim->retain();
	}
	this->setBreathing3Anim( this->loadPlistForAnimationWithName( "breath3", LOADER_ANIM_PLIST ) );
	if( breathing3Anim ) {
		breathing3Anim->retain();
	}
}

void Loader::setAnim( CCAnimation *anim, bool restoreDefault ) {
	this->stopAllActions();

	// setup to default
	if( restoreDefault ) {
		this->setDisplayFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( DEFAULT_TEXTURE_NAME ) );
	}

	if( anim ) {
		this->runAction( CCAnimate::actionWithAnimation( anim, restoreDefault ) );
	}	
}

CCAnimation* Loader::randomBreathAnim() {
	int number = rand() % 3;
	switch( number ) {
	case 0:
		return breathing1Anim;
	case 1:
		return breathing2Anim;
	case 2:
		return breathing3Anim;
	}
	return 0;
}

#if NEED_TR
void Loader::setTrophy( TrophyTypes type, bool value ) {
	switch( type ) {
	case kDestroyBox:
		destroyBoxTrophy = value;
		layer->getDestroyBoxIndicator()->setIsVisible( value );
		break;
	case kLowGravity:
		lowGravityTrophy = value;
		layer->getLowGravityIndicator()->setIsVisible( value );
		break;
	case kTeleport:
		teleportTrophy = value;
		layer->getTeleportIndicator()->setIsVisible( value );
		break;
	case kShield:
		shieldTrophy = value;
		layer->getShieldIndicator()->setIsVisible( value );
		break;
	}
}

bool Loader::getTrophy( TrophyTypes type ) {
	switch( type ) {
	case kDestroyBox:
		return destroyBoxTrophy;
		break;
	case kLowGravity:
		return lowGravityTrophy;
		break;
	case kTeleport:
		return teleportTrophy;
		break;
	case kShield:
		return shieldTrophy;
		break;
	default:
		return false;
	}
}
#endif



// MUSIC

void Loader::playJumpSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( jumpSoundID );
	jumpSoundID = PLAYSOUNDEFFECT( JUMP1 );
#endif
}

void Loader::playWalkSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( walkSoundID );
	walkSoundID = PLAYSOUNDEFFECT( WALK1 );
#endif
}

void Loader::stopWalkSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( walkSoundID );
	walkSoundID = 0;
#endif
}



void Loader::playPushForwardSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( pushForwardSoundId );
	pushForwardSoundId = PLAYSOUNDEFFECT( MOVE_FORWARD1 );
#endif
}

void Loader::playPushBackwardSound() {
#ifdef ENABLE_MUSIC1
	STOPSOUNDEFFECT( pushBackwardSoundId );
	pushBackwardSoundId = PLAYSOUNDEFFECT( MOVE_BACK1 );
#endif
}

/*
void Loader::playJumpEffect()
{
	int soundToPlay = rand() % 4;
	if ( soundToPlay == 0 ) 
	{
		PLAYSOUNDEFFECT( VIKING_JUMPING_1 );
	} 
	else if ( soundToPlay == 1 ) 
	{
		PLAYSOUNDEFFECT( VIKING_JUMPING_2 );
	} 
	else if ( soundToPlay == 2 ) 
	{
		PLAYSOUNDEFFECT( VIKING_JUMPING_3 );
	} 
	else 
	{
		PLAYSOUNDEFFECT( VIKING_JUMPING_4 );
	}
}

void Loader::checkServivalVictory() {
	if( P2C( getPosition() ).y > ( kBoxPerCell - 0.5f ) &&
		bottomIsEmpty == false ) {
			layer->gameOverWithWin( true );
	}
}
*/

