#include "Trophy.h"
#include "Constants/Constants.h"

#if NEED_TR

using namespace cocos2d;

Trophy::Trophy() {
}

Trophy::~Trophy() {
}

bool Trophy::init() {
	bool pRet = false;
	if ( Box2DSprite::init() ) {
		type = kDestroyBox;		
	}

	return pRet;
}

void Trophy::setType( TrophyTypes type ) {
	if( type == kDestroyBox ) {
		this->setDisplayFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( IM_DESTROY_BOX ) );
	}
	else if( type == kLowGravity ) {
		this->setDisplayFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( IM_LOW_GRAVITY ) );
	}
	else if( type == kTeleport ) {
		this->setDisplayFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( IM_TELEPORT ) );
	} 
	else {
		this->setDisplayFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( IM_SHIELD ) );
	}
}

CCRect Trophy::trophyBoundingBox() {
	CCRect trophyRect = boundingBox();
	trophyRect.size.width *= 1.2;
	trophyRect.size.height *= 1.2;
	return trophyRect;
}



#endif