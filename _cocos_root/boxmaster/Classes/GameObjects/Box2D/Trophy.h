#ifndef TROPHY_H
#define TROPHY_H

#include "GameObjects\Box2D\Box2DSprite.h"

#if NEED_TR
	class Trophy : public Box2DSprite {	
		TrophyTypes type;
	CC_SYNTHESIZE( CCAnimation *, trophyFadeAnim, TrophyFadeAnim );
	public:
		Trophy();
		~Trophy();
		bool init();
		void setType( TrophyTypes type );
		TrophyTypes getType() {
			return type;
		}
		CCRect trophyBoundingBox(); 
	};
#endif
#endif