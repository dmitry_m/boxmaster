#ifndef SENSOR_H
#define SENSOR_H

#include "GameObjects\Box2D\Box2DSprite.h"

using namespace cocos2d;

class Sensor : public Box2DSprite
{
public:
	~Sensor();
	bool init( GameObjectType type );
	virtual bool mouseJointBegan();
};

#endif