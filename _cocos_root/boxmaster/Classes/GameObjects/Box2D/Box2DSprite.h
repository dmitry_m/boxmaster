#ifndef BOX2DSPRITE_H
#define BOX2DSPRITE_H

#include "Box2D.h"

#include "GameObjects\GameCharacter.h"

using namespace cocos2d;

class Box2DSprite : public GameCharacter
{	
	CC_SYNTHESIZE( bool, needProcess, NeedProcess );

public:
	CC_SYNTHESIZE( b2Body *, body, Body );

	bool init();
	// Public Methods
	// Return TRUE to accept the mouse joint
	// Return FALSE to reject the mouse joint
	virtual bool mouseJointBegan();
};

#endif