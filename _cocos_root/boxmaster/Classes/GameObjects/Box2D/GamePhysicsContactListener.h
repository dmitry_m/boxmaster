#ifndef GAME_PHYSICS_CONTACT_LISTENER_H
#define GAME_PHYSICS_CONTACT_LISTENER_H

#include "GameObjects\Box2D\Box2DSprite.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"

using namespace cocos2d;

class Scene1ActionLayer;

class GamePhysicsContactListener : public b2ContactListener {
	CC_SYNTHESIZE( Scene1ActionLayer*, layer, Layer );
	CC_SYNTHESIZE( bool, sensorsLocked, SensorsLocked );

public:
	GamePhysicsContactListener(void);
	~GamePhysicsContactListener(void);

    void* userData;
    void BeginContact(b2Contact* contact);
    void EndContact(b2Contact* contact);
};

#endif