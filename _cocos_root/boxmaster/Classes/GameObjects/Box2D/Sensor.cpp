#include "Sensor.h"

Sensor::~Sensor() {
}

bool Sensor::init( GameObjectType type ) {
	bool pRet = false;
	if ( Box2DSprite::init() ) {
		gameObjectType = type;
		needProcess = false;
		pRet = true;
	}
	return pRet;
}

bool Sensor::mouseJointBegan() {
	return false;
}
