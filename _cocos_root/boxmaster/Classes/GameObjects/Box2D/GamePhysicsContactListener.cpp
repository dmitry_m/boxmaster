#include "GamePhysicsContactListener.h"


GamePhysicsContactListener::GamePhysicsContactListener(void)
{
}

GamePhysicsContactListener::~GamePhysicsContactListener(void)
{
}

void GamePhysicsContactListener::BeginContact(b2Contact* contact) {	

	Box2DSprite *spriteA = ( Box2DSprite* ) contact->GetFixtureA()->GetBody()->GetUserData();
	Box2DSprite *spriteB = ( Box2DSprite* ) contact->GetFixtureB()->GetBody()->GetUserData();
	
	if(  spriteA != NULL && spriteA != NULL &&
		( ( spriteA->getGameObjectType() == kLeftSensorType && spriteB->getGameObjectType() == kBoxType ) || 
		  ( spriteA->getGameObjectType() == kBoxType && spriteB->getGameObjectType() == kLeftSensorType ) ) ) {
			  
		layer->getLoader()->setLeftSensorObjects( layer->getLoader()->getLeftSensorObjects() + 1 );
	}
	else if(  spriteA != NULL && spriteA != NULL &&
		( ( spriteA->getGameObjectType() == kBottomSensorType && spriteB->getGameObjectType() == kBoxType ) || 
		  ( spriteA->getGameObjectType() == kBoxType && spriteB->getGameObjectType() == kBottomSensorType ) ) ) {
			  
		layer->getLoader()->setBottomSensorObjects( layer->getLoader()->getBottomSensorObjects() + 1 );
	}
	else if(  spriteA != NULL && spriteA != NULL &&
		( ( spriteA->getGameObjectType() == kRightSensorType && spriteB->getGameObjectType() == kBoxType ) || 
		  ( spriteA->getGameObjectType() == kBoxType && spriteB->getGameObjectType() == kRightSensorType ) ) ) {
			  
		layer->getLoader()->setRightSensorObjects( layer->getLoader()->getRightSensorObjects() + 1 );
	}
}

void GamePhysicsContactListener::EndContact(b2Contact* contact) {
	Box2DSprite *spriteA = ( Box2DSprite* ) contact->GetFixtureA()->GetBody()->GetUserData();
	Box2DSprite *spriteB = ( Box2DSprite* ) contact->GetFixtureB()->GetBody()->GetUserData();
	if(  spriteA != NULL && spriteA != NULL &&
		( ( spriteA->getGameObjectType() == kLeftSensorType && spriteB->getGameObjectType() == kBoxType ) || 
		  ( spriteA->getGameObjectType() == kBoxType && spriteB->getGameObjectType() == kLeftSensorType ) ) ) {
			  
		layer->getLoader()->setLeftSensorObjects( layer->getLoader()->getLeftSensorObjects() - 1 );
	}
	else if(  spriteA != NULL && spriteA != NULL &&
		( ( spriteA->getGameObjectType() == kBottomSensorType && spriteB->getGameObjectType() == kBoxType ) || 
		  ( spriteA->getGameObjectType() == kBoxType && spriteB->getGameObjectType() == kBottomSensorType ) ) ) {
			  
		layer->getLoader()->setBottomSensorObjects( layer->getLoader()->getBottomSensorObjects() - 1 );
	}
	else if(  spriteA != NULL && spriteA != NULL &&
		( ( spriteA->getGameObjectType() == kRightSensorType && spriteB->getGameObjectType() == kBoxType ) || 
		  ( spriteA->getGameObjectType() == kBoxType && spriteB->getGameObjectType() == kRightSensorType ) ) ) {
			  
		layer->getLoader()->setRightSensorObjects( layer->getLoader()->getRightSensorObjects() - 1 );
	}
}