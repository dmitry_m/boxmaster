#include "Box.h"

using namespace cocos2d;

Box::Box() {
	needTexture = true;
	strong = false;
}

Box::~Box() {
}

bool Box::init() {
	bool pRet = false;

	if ( Box2DSprite::init() ) {
		char textureName[SMALL_BUF_LEN];
		if( needTexture ) {
			if( strong ) {
				strcpy( textureName, "box4.png" );				
			}
			else {
				sprintf( textureName, "box%d.png", rand() % 3 + 1 );
			}
			this->setDisplayFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( textureName ) );
		}
		bodyLastPosition = b2Vec2( 0.0f, 0.0f );
		this->firstFall = true;
		gameObjectType = kBoxType;
		pRet = true;
		border = false;
		plugged = false;
	}

	return pRet;
}

bool Box::mouseJointBegan() {
	return false;
}
