#ifndef BOX_H
#define BOX_H

#include "GameObjects\Box2D\Box2DSprite.h"

class Box : public Box2DSprite
{	
	CC_SYNTHESIZE( b2Vec2, bodyLastPosition, BodyLastPosition );
	CC_SYNTHESIZE( bool, firstFall, FirstFall );
	CC_SYNTHESIZE( bool, needTexture, NeedTexture );
	CC_SYNTHESIZE( bool, border, Border );
	CC_SYNTHESIZE( bool, plugged, Plugged );
	CC_SYNTHESIZE( bool, strong, Strong );
	CC_SYNTHESIZE( CCAnimation *, boxFadeAnim, BoxFadeAnim );
public:
	Box();
	~Box();
	bool init();
	virtual bool mouseJointBegan();
};

#endif