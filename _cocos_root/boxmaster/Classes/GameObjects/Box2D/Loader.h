#ifndef LOADER_H
#define LOADER_H

#include "GameObjects\Box2D\Box2DSprite.h"
#include "Service\Box2DHelpers.h"
#include "Service\ServiceInfo.h"
#include "GameObjects\Box2D\Sensor.h"
#include "Scenes\Scene1\Layers\Scene1ActionLayer.h"

using namespace cocos2d;

class Scene1ActionLayer;

class Loader : public Box2DSprite 
{
protected:
	b2World *world;

	double width;
	double height;
	unsigned int walkSoundID;
	unsigned int jumpSoundID;
	unsigned int pushForwardSoundId;
	unsigned int pushBackwardSoundId;

#if NEED_TR
	CC_SYNTHESIZE( bool, destroyBoxTrophy, DestroyBoxTrophy );
	CC_SYNTHESIZE( bool, lowGravityTrophy, LowGravityTrophy );
	CC_SYNTHESIZE( bool, teleportTrophy, TeleportTrophy );
	CC_SYNTHESIZE( bool, shieldTrophy, ShieldTrophy );
#endif
	CC_SYNTHESIZE( bool, fallingMoving, FallingMoving );

	//bool landed;
	CC_SYNTHESIZE( Scene1ActionLayer*, layer, Layer );
	CC_SYNTHESIZE( CCSpriteFrame*, standingFrame, StandingFrame );
	float loaderRelativeBoxOffset;
	
	// Walking, breathing, jumping and death animations.    
	CCAnimation* randomBreathAnim(); 
	CC_SYNTHESIZE( CCAnimation *, walkingAnim, WalkingAnim );
	CC_SYNTHESIZE( CCAnimation *, pushForwardAnim, PushForwardAnim );
	CC_SYNTHESIZE( CCAnimation *, pushBackwardAnim, PushBackwardAnim );
	CC_SYNTHESIZE( CCAnimation *, breathing1Anim, Breathing1Anim );
	CC_SYNTHESIZE( CCAnimation *, breathing2Anim, Breathing2Anim );
	CC_SYNTHESIZE( CCAnimation *, breathing3Anim, Breathing3Anim );
	CC_SYNTHESIZE( CCAnimation *, jumpingStartAnim, JumpingStartAnim );	
	CC_SYNTHESIZE( CCAnimation *, jumpingEndAnim, JumpingEndAnim );	
	CC_SYNTHESIZE( CCAnimation *, deathAnim, DeathAnim );
	
	//CC_SYNTHESIZE( ccTime, waitingTime, WaitingTime );
	
	CC_SYNTHESIZE( CCPoint, loaderStartPosition, LoaderStartPosition );
	CC_SYNTHESIZE( Coordinates, boxStartCoordinates, BoxStartCoordinates );
	CC_SYNTHESIZE( CCPoint, loaderEndPosition, LoaderEndPosition );
	CC_SYNTHESIZE( Coordinates, boxEndCoordinates, BoxEndCoordinates );
		
	float jumpTimeout;
	CC_SYNTHESIZE( int, back, Back );
	CC_SYNTHESIZE( bool, secondPartOfMoving, SecondPartOfMoving );
	CC_SYNTHESIZE( bool, dangerousMoving, DangeroutMoving );
	CC_SYNTHESIZE( bool, jumpQuery, JumpQuery );
	CC_SYNTHESIZE( DirectionTypes, currentDirection, CurrentDirection );
	CC_SYNTHESIZE( DirectionTypes, lastDirection, lastDirection );
	CC_SYNTHESIZE( bool, falling, Falling );
	CC_SYNTHESIZE( bool, flyMode, FlyMode );
	CC_SYNTHESIZE( bool, friction, Friction );
	CC_SYNTHESIZE( bool, leftIsEmpty, LeftIsEmpty );
	CC_SYNTHESIZE( bool, rightIsEmpty, RightIsEmpty );
	CC_SYNTHESIZE( bool, topIsEmpty, TopIsEmpty );
	CC_SYNTHESIZE( bool, bottomIsEmpty, BottomIsEmpty );
	CC_SYNTHESIZE( CCPoint, lastPosition, LastPosition );
	CC_SYNTHESIZE( CCAction*, movingLoaderAction, MovingLoaderAction );
	CC_SYNTHESIZE( CCAction*, movingBoxAction, MovingBoxAction );
	CC_SYNTHESIZE( int, distanceToBox, DistanceToBox );
	CC_SYNTHESIZE( float, direction, Direction );
	CC_SYNTHESIZE( Box*, boxToMove, BoxToMove );

	CC_SYNTHESIZE( Sensor*, lSensor, LSensor );
	CC_SYNTHESIZE( Sensor*, rSensor, RSensor );
	CC_SYNTHESIZE( Sensor*, tSensor, TSensor );
	CC_SYNTHESIZE( Sensor*, bSensor, BSensor );
	CC_SYNTHESIZE( Sensor*, mbSensor, MBSensor );
	
	CC_SYNTHESIZE( int, leftSensorObjects, LeftSensorObjects );
	CC_SYNTHESIZE( int, bottomSensorObjects, BottomSensorObjects );
	CC_SYNTHESIZE( int, rightSensorObjects, RightSensorObjects );

	void playJumpSound();
	void playWalkSound();
	void stopWalkSound();
	void playPushBackwardSound();
	void playPushForwardSound(); 

public:
	Loader();
	~Loader();
	bool initWithWorld( b2World *theWorld, CCPoint  location, Scene1ActionLayer* sceneLayer );
	void initAnimations();
	void setAnim( CCAnimation *anim, bool restoreDefault );
	void updateStateWithDeltaTime( ccTime deltaTime );
	virtual void changeState( CharacterStates newState );
	void createBodyAtLocation( CCPoint location );
	
	void applyDirection( ccTime deltaTime );
	void createSensors();
	CorrectionTypes checkSpritePosition();
	void MoveToValidPosition();
	bool tryJump();
	void refreshSensors();

	// ver 1
	//void StartMovingBox();
	//void CheckForContinueMovingBox();
	//void movingBackSensorDetected();
	//void EndMovingBox();
	
	//ver 2
	void StartMovingBox();
	void ProcessMoving( float dt );
	void EndMovingBox();

	bool BoxAtSide( DirectionTypes side );
	void PrepareButtons();
	CCRect heartBoundingBox();

	// check victory
	//void checkServivalVictory();
	//void playJumpEffect();
	
#if NEED_TR
	void setTrophy( TrophyTypes type, bool value );
	bool getTrophy( TrophyTypes type );
#endif
};

#endif