#include "Cloud.h"

bool Cloud::init()
{
	bool pRet = false;
	if( GameObject::init() )
    {
		CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

		// �������� �����������
		char textureName[SMALL_BUF_LEN];
		sprintf( textureName, "cloud%d.png", ( rand() % 5 ) + 1 );
		this->setDisplayFrame( CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName( textureName ) );

		// ��������� ������������ � ������ ��������
		AlignRandom();
		Fly();

		pRet = true;
	}
	return pRet;
}

void Cloud::AlignToBegin() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	int firstOffset = screenSize.width / SKY_FIRST_OFFSET;
	int altOffset = screenSize.height *( kSkyAltOffset );

	// ������������ � ������ ����. ������ ���������� ��������� �������
	double offsetY = ( rand() %  (int)( boundingBox().size.height * 2 ) ) - boundingBox().size.height;
	CCPoint beginPosition = ccp( firstOffset - 2 * boundingBox().size.width, screenSize.height - altOffset + offsetY );

	setPosition( beginPosition );
}


void Cloud::AlignRandom() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	int firstOffset = screenSize.width / SKY_FIRST_OFFSET;
	int altOffset = screenSize.height * kSkyAltOffset;
	
	// ������� ���������� ��������� �������
	double offsetX = ( rand() %  10 )/ 10.0f * screenSize.width;
	double offsetY = ( rand() %  (int)( boundingBox().size.height * 2 ) ) - boundingBox().size.height;
	CCPoint beginPosition = ccp( firstOffset + offsetX, screenSize.height - altOffset + offsetY );

	setPosition( beginPosition );
}

void Cloud::AlignToEnd() {	
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	int firstOffset = screenSize.width / SKY_FIRST_OFFSET;
	int nextOffset = screenSize.width / SKY_BETWEEN_WINDOWS;
	int altOffset = screenSize.height * kSkyAltOffset;
		
	// ������������ � �����
	CCPoint endPosition = ccp( firstOffset + 2 * nextOffset + 2 * boundingBox().size.width, screenSize.height - altOffset );
	
	setPosition( endPosition );
}

void Cloud::Fly() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	int firstOffset = screenSize.width / SKY_FIRST_OFFSET;
	int nextOffset = screenSize.width / SKY_BETWEEN_WINDOWS;
	CCPoint endPosition = ccp( firstOffset + 2 * nextOffset + 2 * boundingBox().size.width, getPosition().y );
	double timeForMove = ( double )( rand() % 10 ) + 25.0f;

	// ������ �������� ������ � ����� ����
	CCAction* action = CCMoveTo::actionWithDuration( timeForMove, endPosition );	
	this->runAction( action );
}


bool Cloud::NeedReposition() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	int firstOffset = screenSize.width / SKY_FIRST_OFFSET;
	int nextOffset = screenSize.width / SKY_BETWEEN_WINDOWS;	
	CCPoint endPosition = ccp( firstOffset + 2 * nextOffset + boundingBox().size.width, getPosition().y );

	// �������� �� ���������� ����� ����
	if( getPosition().x > endPosition.x ) {
		return true;
	}
	return false;
}

