#ifndef CLOUD_H
#define CLOUD_H

#include "GameObjects\GameObject.h"

using namespace cocos2d;

class Cloud : public GameObject
{
public:
	bool NeedReposition();
	void AlignToBegin();
	void AlignRandom();
	void AlignToEnd();
	void Fly();
	bool init();
};

#endif
