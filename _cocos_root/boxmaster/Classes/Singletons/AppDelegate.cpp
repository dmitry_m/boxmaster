#include "AppDelegate.h"

#include "Singletons\GameManager.h"


using namespace cocos2d;

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{ 
	CocosDenshion::SimpleAudioEngine::end();
}

bool AppDelegate::initInstance()
{
    bool bRet = false;
    do 
    {
#if ( CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 )
        // Initialize OpenGLView instance, that release by CCDirector when application terminate.
        // The HelloWorld is designed as HVGA.
        CCEGLView * pMainWnd = new CCEGLView();
        CC_BREAK_IF( ! pMainWnd
            || ! pMainWnd->Create( TEXT( "Loader" ), SOURCE_SCREEN_HEIGHT, SOURCE_SCREEN_WIDTH ) );
#endif  // CC_PLATFORM_WIN32

#if ( CC_TARGET_PLATFORM == CC_PLATFORM_IOS )

        // OpenGLView initialized in testsAppDelegate.mm on ios platform, nothing need to do here.

#endif  // CC_PLATFORM_IOS

#if ( CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID )

        // Android doesn't need to do anything.

#endif  // CC_PLATFORM_ANDROID

#if ( CC_TARGET_PLATFORM == CC_PLATFORM_WOPHONE )
        // Initialize OpenGLView instance, that release by CCDirector when application terminate.
        // The HelloWorld is designed as HVGA.
        // Use GetScreenWidth() and GetScreenHeight() get screen width and height.
        CCEGLView * pMainWnd = new CCEGLView( this );
        CC_BREAK_IF( ! pMainWnd
            || ! pMainWnd->Create( 320, 480 ) );

#if !defined( _TRANZDA_VM_ )
        // set the resource zip file
        // on wophone emulator, we copy resources files to Work7/TG3/APP/ folder instead of zip file
        CCFileUtils::setResource( "qwerty.zip" );
#endif

#endif  // CC_PLATFORM_WOPHONE

        bRet = true;
    } while ( 0 );
    return bRet;
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView( &CCEGLView::sharedOpenGLView() );

    // sets landscape mode
    #if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		pDirector->setDeviceOrientation(kCCDeviceOrientationLandscapeLeft);
	#endif

    // turn on display FPS
	bool displayFps = false;

#if ( DISPLAY_DEF_FPS == 1 )
	displayFps = true;
#endif

	GameManager::sharedGameManager()->setFps( 60.0f );
    pDirector->setDisplayFPS( displayFps );
    pDirector->setAnimationInterval( 1.0f / GameManager::sharedGameManager()->getFps() );
	
	// create a scene. it's an autorelease object
	CCSize screenSize = pDirector->getWinSize();
#if NEED_EM
	GameManager::sharedGameManager()->setupAudioEngine();
#endif

	if( screenSize.width == 480 && screenSize.height == 320 ) {
		kPathPrefix = kLQ;
	}
	else if( screenSize.width == 800 && screenSize.height == 480 ) {
		kPathPrefix = kMQ;
	}
	else if( screenSize.width == 854 && screenSize.height == 480 ) {
		kPathPrefix = kMQ;
	}
	else if( screenSize.width == 960 && screenSize.height == 540 ) {
		kPathPrefix = kHQ;
	}
	else if( screenSize.width == 1184 && screenSize.height == 720 ) {
		kPathPrefix = kHHQ;
	}
	else if( screenSize.width == 1280 && screenSize.height == 720 ) {
		kPathPrefix = kHHQ;
	}
	else if( screenSize.width == 1280 && screenSize.height == 800 ) {
		kPathPrefix = kHHQ;
	}
	else {
		kPathPrefix = kHHQ;
	}
	INIT_CONSTANTS

	//GameManager::sharedGameManager()->runSceneWithID( kMainMenuScene );
	GameManager::sharedGameManager()->runSceneWithID( kGameLevel1 );
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->pause();
	GameManager::sharedGameManager()->pauseBackgroundTrack();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->resume();
	GameManager::sharedGameManager()->resumeBackgroundTrack();
}
