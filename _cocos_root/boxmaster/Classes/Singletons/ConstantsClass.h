#ifndef CONSTANTS_CLASS_H
#define CONSTANTS_CLASS_H

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;


typedef enum {
    kLQ=0,
    kMQ=1,
	kHQ=2,
	kHHQ=3,
} PathPrefix;

class ConstantsClass : public CCObject { 

public:
	// constants
	PathPrefix _PathPrefix;	
	string _BigFont;
	string _Font;
#if NEED_RUS
	string _RusBigFont;
	string _RusFont;
#endif
	int _BoxCanPlaced;
	float _JoystickScale;
	float _BoxSide;
	float _MarginBottom;
	float _SensorDepth;
	float _JumpImpulse;
	float _DeltaWalk;
	float _Gravity;
	// joystick
	float _1stButtonOffset;	
	float _2dButtonOffset;
	float _3dButtonOffset;
	float _4thButtonOffset;

	ConstantsClass();
	~ConstantsClass();
	static ConstantsClass* sharedConstantsClass();
	bool init();
	void _initConstants();

	string getPath( string path );
};

#endif