#include "Singletons\GameManager.h"
#include "Scenes\MainMenu\MainMenuScene.h"
#include "Scenes\Options\OptionsScene.h"
#include "Scenes\Credits\CreditsScene.h"
#include "Scenes\Exit\ExitScene.h"
#include "Scenes\LevelComplete\LevelCompleteScene.h"
#include "Scenes\Greeting\GreetingScene.h"
#include "Scenes\Help\HelpScene.h"
#include "Scenes\Highscores\HighscoresScene.h"
#include "Scenes\Scene1\Scene1.h"


static GameManager* _sharedGameManager = NULL;	
static bool s_bFirstRun = true;

GameManager::GameManager() {

}

GameManager::~GameManager() {
	_sharedGameManager->release();	
}

GameManager* GameManager::sharedGameManager() {	
	if( s_bFirstRun ) {
		if( _sharedGameManager == NULL ) {
			_sharedGameManager = new GameManager();
			_sharedGameManager->init();
			s_bFirstRun = false;
		}
	}
	return _sharedGameManager;                             
}

bool GameManager::init() { 
	bool pRet = false;

	if( true ) {
		CCLOG( "Game Manager Singleton, init" );
		hasPlayerDied = false;
		currentScene = kNoSceneUninitialized;
		oldScene = kNoSceneUninitialized;
		hasAudioBeenInitialized = false;
		soundEngine = NULL;
		listOfSoundEffectFiles = NULL;
		soundEffectsState = NULL;
		managerSoundState = kAudioManagerUninitialized;

		_CONFIGMAN->init();

		pRet = true;
	}
	return pRet;
}

/*
	------------	-----------------------------------------------------------------
	------------	-----------------------------------------------------------------
*/

void GameManager::runSceneWithID( SceneTypes sceneID ) {
    oldScene = currentScene;
    currentScene = sceneID;
	CCScene* sceneToRun = NULL;

	switch ( sceneID ) {
	case kMainMenuScene: 
			sceneToRun = MainMenuScene::node();
			break;
	case kOptionsScene:
			sceneToRun = OptionsScene::node();
			break;
	case kCreditsScene:
			sceneToRun = CreditsScene::node();
			break;
	case kGreetingScene:
			sceneToRun = GreetingScene::node();
			break;
	case kHelpScene:
			sceneToRun = HelpScene::node();
			break;
	case kHighscoresScene:
			sceneToRun = HighscoresScene::node();
			break;
	case kLevelCompleteScene:
			sceneToRun = LevelCompleteScene::node();
			break;
	case kExitScene:
			sceneToRun = ExitScene::node();
			break;
	case kGameLevel1: 
			sceneToRun =Scene1::node();
			break;
	default:
			CCLOG( "Unknown ID, cannot switch scenes" );
			return;
			break;
	}

	if ( sceneToRun == NULL ) 
	{
		// Revert back, since no new scene was found
		currentScene = oldScene;
		oldScene = kNoSceneUninitialized;
		return;
	}
	
#if NEED_EM
	//load audio
	this->loadAudioForSceneWithID( currentScene );	
#endif

	if ( CCDirector::sharedDirector()->getRunningScene() == NULL ) {
		CCDirector::sharedDirector()->runWithScene( sceneToRun );
	}
	else {
		CCDirector::sharedDirector()->replaceScene( sceneToRun );	
	}
	
#if NEED_EM
	//unload old scene's audio
	this->unloadAudioForSceneWithID( oldScene );
#endif
	oldScene = currentScene;
	currentScene = sceneID;
}


string GameManager::formatSceneTypeToString( SceneTypes sceneID ) 
{

	string result = "";
	switch( sceneID ) 
	{
	case kNoSceneUninitialized:
		result = "kNoSceneUninitialized";
		break;
	case kMainMenuScene:
		result = "kMainMenuScene";
		break;
	case kOptionsScene:
		result = "kOptionsScene";
		break;
	case kCreditsScene:
		result = "kCreditsScene";
		break;
	case kGreetingScene:
		result = "kGreetingScene";
		break;
	case kLevelCompleteScene:
		result = "kLevelCompleteScene";
		break;
	case kHelpScene:
		result = "kHelpScene";
		break;
	case kHighscoresScene:
		result = "kHighscoresScene";
		break;
	case kGameLevel1:
		result = "kGameLevel1";
		break;
	default:
		CCLOG( "Unexpected SceneType." );
	}
	return result;
}


#if NEED_EM

void GameManager::setupAudioEngine() 
{
	if ( hasAudioBeenInitialized == true ) 
		return;
	else 
	{
		hasAudioBeenInitialized = true;
		/*
		In book in this place starts asynchronous sound load
		*/
		return;
    }
}

void GameManager::initAudioAsync()
{

}

CCDictionary<std::string, CCString*> * GameManager::getSoundEffectsListForSceneWithID( SceneTypes sceneID )
{
	const char *plistPath = CCFileUtils::fullPathFromRelativePath( SOUNDS_PLIST );

	CCDictionary<std::string, CCObject*> *plistDictionary = CCFileUtils::dictionaryWithContentsOfFile( plistPath );

	if ( plistDictionary == NULL ) 
	{
		CCLOG( "Error reading SoundEffects.plist" );
		return NULL; // No Plist Dictionary or file found
	}

	if ( listOfSoundEffectFiles == NULL || listOfSoundEffectFiles->count() < 1 )
	{
		CCLOG( "Before" );
		listOfSoundEffectFiles = new cocos2d::CCDictionary<std::string, CCString*>();
		CCLOG( "After" );
		plistDictionary->begin();

		std::string key = "";
		CCDictionary<std::string, CCString*> *val = NULL;

		while( ( val = ( CCDictionary<std::string, CCString*> * ) plistDictionary->next( &key ) ) )
		{
			val->begin();

			std::string valKey = "";
			CCString * valVal = NULL;

			while ( ( valVal = val->next( &valKey ) ) )
			{
				listOfSoundEffectFiles->setObject( valVal, valKey );
			}

			val->end();
		}
		////
		listOfSoundEffectFiles->retain();

		plistDictionary->end();

		CCLOG( "Number of SFX filenames: %d", listOfSoundEffectFiles->count() );
		listOfSoundEffectFiles->autorelease();
	}

	if ( soundEffectsState == NULL || soundEffectsState->size() < 1 )
	{
		soundEffectsState = new map<std::string, bool>();
		listOfSoundEffectFiles->begin();

		std::string key = "";
		CCString *val = NULL;

		while( ( val = listOfSoundEffectFiles->next( &key ) ) )
		{
			if ( soundEffectsState->insert( pair<std::string, bool> ( key, SFX_NOTLOADED ) ).second != true )
			{
				soundEffectsState->erase( key );
				soundEffectsState->insert( pair<std::string, bool> ( key, SFX_NOTLOADED ) );
			}
		}

		listOfSoundEffectFiles->end();
	}

	std::string sceneIDName = formatSceneTypeToString( sceneID );
	CCDictionary<std::string, CCString*> *soundEffectsList = 
		( CCDictionary<std::string, CCString*> * ) plistDictionary->
		objectForKey( sceneIDName );

	return soundEffectsList;
}

void GameManager::loadAudioForSceneWithID( SceneTypes sceneID )
{	
	
	CCDictionary<std::string, CCString*> *soundEffectsToLoad = this->getSoundEffectsListForSceneWithID( sceneID );
	
	if ( soundEffectsToLoad == NULL ) 
	{ 
		CCLOG( "Error reading SoundEffects.plist" );
		return;
	}

	soundEffectsToLoad->begin();

	std::string keyString = "";
	CCString *val = NULL;

	while ( ( val = soundEffectsToLoad->next( &keyString ) ) )
	{
		CCLOG( "\nLoading Audio Key: %s	File: %s", keyString.c_str(), val->toStdString().c_str() );
		char effectName[100] = {0};
		sprintf( effectName, "Sounds/SFX/%s", val->toStdString().c_str() );
		soundEngine->preloadEffect( effectName );
		if ( soundEffectsState->insert( pair<std::string, bool> ( keyString, SFX_LOADED ) ).second != true )
		{
			soundEffectsState->erase( keyString );
			soundEffectsState->insert( pair<std::string, bool> ( keyString, SFX_LOADED ) );
		}
	}

	soundEffectsToLoad->end();
}

void GameManager::unloadAudioForSceneWithID( SceneTypes sceneID )
{	
	CCDictionary<std::string, CCString*> *soundEffectsToUnload = this->getSoundEffectsListForSceneWithID( sceneID );

	if ( soundEffectsToUnload == NULL ) 
	{ 
		CCLOG( "Error reading SoundEffects.plist" );
		return;
	}

	soundEffectsToUnload->begin();

	std::string keyString = "";
	CCString *val = NULL;

	while ( ( val = soundEffectsToUnload->next( &keyString ) ) )
	{
		if ( soundEffectsState->insert( pair<std::string, bool> ( keyString, SFX_NOTLOADED ) ).second != true )
		{
			soundEffectsState->erase( keyString );
			soundEffectsState->insert( pair<std::string, bool> ( keyString, SFX_NOTLOADED ) );
		}
		CCLOG( "\nUnloading Audio Key: %s	File: %s", keyString.c_str(), val->toStdString().c_str() );
		char effectName[100] = {0};
		sprintf( effectName, "Sounds/SFX/%s", val->toStdString().c_str() );
		soundEngine->unloadEffect( effectName );	
	}

	soundEffectsToUnload->end();
}

void GameManager::stopBackgroundTrack() {
	if ( soundEngine->isBackgroundMusicPlaying() ) {
		soundEngine->stopBackgroundMusic();
	}
}
void GameManager::pauseBackgroundTrack() {
	soundEngine->pauseBackgroundMusic();
}
void GameManager::resumeBackgroundTrack() {
	soundEngine->resumeBackgroundMusic();
}

void GameManager::playBackgroundTrack( const char *trackFileName ) {
	if ( soundEngine->isBackgroundMusicPlaying() ) {
		soundEngine->stopBackgroundMusic();
	}
	
	if( !_CONFIG.getMusic() ) {
		return;
	}

	char filePath[100] = {0};
	sprintf( filePath, "Sounds/Music/%s", trackFileName );
	soundEngine = (CocosDenshion::SimpleAudioEngine *)5;
	soundEngine->preloadBackgroundMusic( filePath );
	if ( _CONFIG.getMusic() == true )
		soundEngine->playBackgroundMusic( filePath, true );
}

void GameManager::stopSoundEffect( int soundEffectID ) 
{
	if( soundEffectID != 0 )
		soundEngine->stopEffect( soundEffectID );
}

int GameManager::playSoundEffect( const char *soundEffectKey ) 
{
	if( !_CONFIG.getSound() )
		return 0;

	int soundID = 0;
	bool isSFXLoaded = false;
	map<std::string, bool>::iterator it = soundEffectsState->find( std::string( soundEffectKey ) );

	if ( it != soundEffectsState->end() ) {
		isSFXLoaded = it->second;
	}

	if ( isSFXLoaded == SFX_LOADED ) {
		char filePath[100] = {0};
		sprintf( filePath, "Sounds/SFX/%s", listOfSoundEffectFiles->objectForKey( std::string( soundEffectKey ) )->toStdString().c_str() );
		if ( _CONFIG.getSound() == true )
			soundID = soundEngine->playEffect( filePath );
	} 
	else {
		CCLOG( "GameMgr: SoundEffect % is not loaded.", soundEffectKey );
	}
	return soundID;
}

void GameManager::setIsMusicON( bool state ) {
	_CONFIG.setMusic( state );
	if( !state && soundEngine && soundEngine->isBackgroundMusicPlaying() ) {
		soundEngine->stopBackgroundMusic();
	}
}
	
bool GameManager::getIsMusicON() {
	return _CONFIG.getMusic();
}

void GameManager::setIsSoundEffectsON( bool state ) {
	_CONFIG.setSound( state );
	if( !state && soundEngine )
		soundEngine->stopAllEffects();
}
	
bool GameManager::getIsSoundEffectsON() {
	return _CONFIG.getSound();
}

#endif

void GameManager::setOrientation( bool orient ) {
	_CONFIG.setOrientation( orient );
}
	
bool GameManager::getOrientation() {
	return _CONFIG.getOrientation();
}


CCSize GameManager::getDimensionsOfCurrentScene() {
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
	CCSize levelSize;
	switch ( currentScene ) {
		case kMainMenuScene: 
		case kOptionsScene:
		case kCreditsScene:
		case kGreetingScene:
		case kLevelCompleteScene:
		case kGameLevel1: 
			levelSize = screenSize;
		break;
		default:
			CCLOG( "Unknown Scene ID, returning default size" );
			levelSize = screenSize;
		break;
	}
	return levelSize;
}


void GameManager::exitGame() {
	CCDirector::sharedDirector()->end();
}
