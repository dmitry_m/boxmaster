#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

//#include "cocos2d.h"
#include "Constants\Constants.h"
#include "SimpleAudioEngine.h"
#include "Service\FileOperation.h"
#include "Configuration\GameConfiguration.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	#include "RunJava.h"
#endif

using namespace cocos2d;
using namespace std;

class GameManager : public CCObject { 
	SceneTypes currentScene;
	CC_SYNTHESIZE( float, fps, Fps );
	CC_SYNTHESIZE( bool, isRecord, IsRecord );
	CC_SYNTHESIZE( bool, gamePaused, GamePaused );
	CC_SYNTHESIZE( GameComplexityType, gameComplexity, GameComplexity );
	CC_SYNTHESIZE( int, scores, Scores );
	CC_SYNTHESIZE( int, oldScores, OldScores );
	CC_SYNTHESIZE( bool, hasPlayerDied, HasPlayerDied );
    CC_SYNTHESIZE( GameManagerSoundState, managerSoundState, ManagerSoundState );
    CC_SYNTHESIZE( int, mmOffset, mmOffset );
	CC_SYNTHESIZE( SceneTypes, oldScene, OldScene );
	CC_SYNTHESIZE( bool, userLoggedIn, UserLoggedIn );	

#if NEED_EM
	bool hasAudioBeenInitialized;
    CocosDenshion::SimpleAudioEngine *soundEngine;
	CCDictionary<std::string, CCString *> *listOfSoundEffectFiles;
	map<std::string, bool> *soundEffectsState;
#endif

public:
	GameManager();
	~GameManager();
	static GameManager* sharedGameManager();
	bool init();
	void runSceneWithID( SceneTypes sceneID );
	string formatSceneTypeToString( SceneTypes sceneID );
	CCSize getDimensionsOfCurrentScene();
	void exitGame();	

	// �����
#if NEED_EM
	CCDictionary<std::string, CCString*> * getSoundEffectsListForSceneWithID( SceneTypes sceneID );
	void loadAudioForSceneWithID( SceneTypes sceneID );
	void unloadAudioForSceneWithID( SceneTypes sceneID );
	void setupAudioEngine();
	void initAudioAsync();
	void pauseBackgroundTrack();
	void resumeBackgroundTrack();
	void playBackgroundTrack( const char *trackFileName );
	void stopBackgroundTrack();
	void stopSoundEffect( int soundEffectID );
	int playSoundEffect( const char *soundEffectKey );
	void setIsMusicON( bool state );
	bool getIsMusicON();
	void setIsSoundEffectsON( bool state );
	bool getIsSoundEffectsON();	

#endif
	void setOrientation( bool pos );
	bool getOrientation();
};

#endif