#include "Constants\Constants.h"
#include "ConstantsClass.h"

static ConstantsClass* _sharedConstantsClass = NULL;	
static bool s_bFirstRun = true;

ConstantsClass::ConstantsClass() {
}

ConstantsClass::~ConstantsClass() {
	_sharedConstantsClass->release();	
}

ConstantsClass* ConstantsClass::sharedConstantsClass() {	
	if( s_bFirstRun ) {
		if( _sharedConstantsClass == NULL ) {
			_sharedConstantsClass = new ConstantsClass();
			_sharedConstantsClass->init();
			s_bFirstRun = false;
		}
	}
	return _sharedConstantsClass;                             
}

bool ConstantsClass::init() { 
	bool pRet = false;

	if( true ) {
		pRet = true;
	}
	return pRet;
}

/*
	-----------------------------------------------------------------------------
	-----------------------------------------------------------------------------
*/


void ConstantsClass::_initConstants() {
	switch( _PathPrefix ) {
		case kLQ:
			_BigFont = "Fonts/eng/LoaderFont_54.fnt";
			_Font = "Fonts/eng/LoaderFont_27.fnt";
#if NEED_RUS
			_RusBigFont = "Fonts/rus/LoaderFont_54.fnt";
			_RusFont = "Fonts/rus/LoaderFont_27.fnt";
#endif
			_BoxCanPlaced = 14;
			_BoxSide = 32.0f;
			_MarginBottom = 78;
			_JoystickScale = 1.17f;
			_SensorDepth = 2.7f;
			_JumpImpulse = 5.5f;
			_DeltaWalk = 2.2f;
			_Gravity = 10.0f;

			_1stButtonOffset = 0.09f;	
			_2dButtonOffset = 0.24f;
			_3dButtonOffset = 0.76f;
			_4thButtonOffset = 0.91f;
			break;

		case kMQ:
			_BigFont = "Fonts/eng/LoaderFont_81.fnt";
			_Font = "Fonts/eng/LoaderFont_40.fnt";
#if NEED_RUS
			_RusBigFont = "Fonts/rus/LoaderFont_81.fnt";
			_RusFont = "Fonts/rus/LoaderFont_40.fnt";
#endif
			_BoxCanPlaced = 15;
			_BoxSide = 48.0f;
			_MarginBottom = _BoxSide * 2.0f;
			_JoystickScale = 1.0f;
			_SensorDepth = 3.7f;
			_JumpImpulse = 9.5f;
			_DeltaWalk = 3.2f;
			_Gravity = 16.0f;
			_3dButtonOffset = 0.76f;
			_1stButtonOffset = 0.1f;	
			_2dButtonOffset = 0.24f;
			_4thButtonOffset = 0.90f;
			break;

		case kHQ:
			_BigFont = "Fonts/eng/LoaderFont_108.fnt";
			_Font = "Fonts/eng/LoaderFont_54.fnt";
#if NEED_RUS
			_RusBigFont = "Fonts/rus/LoaderFont_108.fnt";
			_RusFont = "Fonts/rus/LoaderFont_54.fnt";
#endif
			_BoxCanPlaced = 15;
			_BoxSide = 56.0f;
			_MarginBottom = _BoxSide * 2.0f;
			_JoystickScale = 1.0f;
			_SensorDepth = 4.0f;
			_JumpImpulse = 11.0f;
			_DeltaWalk = 3.4f;
			_Gravity = 20.0f;
			_3dButtonOffset = 0.76f;
			_1stButtonOffset = 0.1f;	
			_2dButtonOffset = 0.24f;
			_4thButtonOffset = 0.90f;
			break;

		case kHHQ:
			_BigFont = "Fonts/eng/LoaderFont_136.fnt";
			_Font = "Fonts/eng/LoaderFont_68.fnt";
#if NEED_RUS
			_RusBigFont = "Fonts/rus/LoaderFont_136.fnt";
			_RusFont = "Fonts/rus/LoaderFont_68.fnt";
#endif
			_BoxCanPlaced = 15;
			_BoxSide = 72.0f;
			_MarginBottom = _BoxSide * 2.0f;
			_JoystickScale = 1.0f;
			_SensorDepth = 5.6f;
			_JumpImpulse = 14.0f;
			_DeltaWalk = 4.7f;
			_Gravity = 26.0f;
			_3dButtonOffset = 0.76f;
			_1stButtonOffset = 0.1f;	
			_2dButtonOffset = 0.24f;
			_4thButtonOffset = 0.90f;
			break;
	}
}



string ConstantsClass::getPath( string path ) {
	string fullPath = "Res/";
	switch( _PathPrefix ) {
		case kLQ:
			fullPath += "0_lq/";
			break;
		case kMQ:
			fullPath += "1_mq/";
			break;
		case kHQ:
			fullPath += "2_hq/";
			break;
		case kHHQ:
			fullPath += "3_hhq/";
			break;
	}
	
	fullPath += path;
	return fullPath;
}