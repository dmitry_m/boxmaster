#include "Configuration\GameConfiguration.h"

static GameConfiguration* _sharedConfManager = NULL;	
static bool _firstRun = true;

GameConfiguration* GameConfiguration::sharedConfigurationManager() {	
	if( _firstRun ) {
		if( _sharedConfManager == NULL ) {
			_sharedConfManager = new GameConfiguration();
			_firstRun = false;
		}
	}
	return _sharedConfManager;                             
}

bool GameConfiguration::dispatchScores( int scores, GameComplexityType complexity ) {
	int currentScores = getMaxScoresForComplexity( complexity );
#if NEEDS_OF
	RunJava::getScores();
#endif
	if( currentScores < scores ) {
		setMaxScoresForComplexity( scores, complexity );
		saveConfiguration( true, true );	
		return true;
	}
	else {
		return false;
	}
}

void GameConfiguration::init() {
	openConfiguration();
#if ( CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID )
	RunJava::getScores();
#endif
}

void GameConfiguration::settingsUpdated() {
	saveConfiguration( true, false );
}

int GameConfiguration::getMaxScoresForComplexity( GameComplexityType complexity ) {
	if( complexity == kEasy ) {
		return Conf.getScoresEasy();
	}
	else if( complexity == kMedium ) {
		return Conf.getScoresMedium();
	}
	else if( complexity == kHard ) {
		return Conf.getScoresHard();
	}
	return 0;
}

#if NEEDS_OF
	void GameConfiguration::syncOpenfeintScoresWithLocal( int openfeintScores, int complexity ) {
		GameComplexityType cmplx = complexityFromInt( complexity );
		
		int localScores = getMaxScoresForComplexity( cmplx );
		// ���������� ��������� ����������
		if( localScores < openfeintScores ) {
			setMaxScoresForComplexity( openfeintScores, cmplx );
			saveConfiguration( true, false );
		}	
		// ���������� ���������� openfeint
		else if( localScores > openfeintScores ) {
			saveConfiguration( false, true );
		}
	}
#endif

// #######################################################################################

void GameConfiguration::saveConfiguration( bool local, bool openfeint ) {
	if( local ) {
		ConfigFile::writeConfiguration( Conf.getConf() );
	}
#if NEEDS_OF
	if( openfeint ) {
		RunJava::submitScores( Conf.getScoresEasy() >= 0 ? Conf.getScoresEasy() : 0, 0 );
		RunJava::submitScores( Conf.getScoresMedium() >= 0 ? Conf.getScoresMedium() : 0, 1 );
		RunJava::submitScores( Conf.getScoresHard() >= 0 ? Conf.getScoresHard() : 0, 2 );
	}
#endif
}

void GameConfiguration::openConfiguration() {
	Conf.setConf( ConfigFile::readConfiguration() );
}

void GameConfiguration::setMaxScoresForComplexity( int scores, GameComplexityType complexity ) {
	if( complexity == kEasy ) {
		Conf.setScoresEasy( scores );
	}
	else if( complexity == kMedium ) {
		Conf.setScoresMedium( scores );
	}
	else if( complexity == kHard ) {
		Conf.setScoresHard( scores );
	}
}

GameComplexityType GameConfiguration::complexityFromInt( int complexity ) {
	if( complexity == 0 ) {
		return kEasy;
	}
	else if( complexity == 1 ) {
		return kMedium;
	}
	else if( complexity == 2 ) {
		return kHard;
	}
}