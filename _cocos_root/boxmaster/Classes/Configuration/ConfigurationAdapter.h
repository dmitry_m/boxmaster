#ifndef CONFIGURATION_ADAPTER_H
#define CONFIGURATION_ADAPTER_H


struct Configuration {
	bool music;
	bool sound;
	bool orientation;
	int maxScoresEasy;
	int maxScoresMedium;
	int maxScoresHard;
};

class ConfigurationAdapter {
	Configuration conf;

public:	
	void setConf( Configuration newConf ) { conf = newConf; }
	Configuration getConf() { return conf; }

	void setMusic( bool on ) {	conf.music = on; }
	bool getMusic() {	return conf.music; }

	void setSound( bool on ) {	conf.sound = on; }
	bool getSound() {	return conf.sound; }

	void setOrientation( bool on ) {	conf.orientation = on; }
	bool  getOrientation() {	return conf.orientation; }
	
	void setScoresEasy( int scores ) {	conf.maxScoresEasy = scores; }
	int getScoresEasy() {	return conf.maxScoresEasy; }

	void setScoresMedium( int scores ) {	conf.maxScoresMedium = scores; }
	int getScoresMedium() {	return conf.maxScoresMedium; }

	void setScoresHard( int scores ) {	conf.maxScoresHard = scores; }
	int getScoresHard() {	return conf.maxScoresHard; }
};

#endif
