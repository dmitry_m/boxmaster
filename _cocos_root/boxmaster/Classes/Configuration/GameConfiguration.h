#ifndef GAME_CONFIGURATION_H
#define GAME_CONFIGURATION_H

#include "Constants\Constants.h"
#include "Service\FileOperation.h"
#include "Configuration\ConfigurationAdapter.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	#include "RunJava.h"
#endif


class GameConfiguration {
	void saveConfiguration( bool local, bool openfeint );	
	void openConfiguration();	
	void setMaxScoresForComplexity( int scores, GameComplexityType complexity );
	GameComplexityType complexityFromInt( int complexity );

public:	
	ConfigurationAdapter Conf;

	// ������ ��������� �� ������ (���� �����, �� �������)
	static GameConfiguration* sharedConfigurationManager();
	
	// ������ �� ���������� �����
	void init();

	// ���������� ��������� ����. ������ true, ���� ���������� ����� ������.
	// ������� ������ � ��������, ���� ����������
	bool dispatchScores( int scores, GameComplexityType complexity );

#if NEEDS_OF
	// ������������� ����������� openfeint � ���������
	void syncOpenfeintScoresWithLocal( int scores, int complexity );
#endif
	// �������� ���������
	void settingsUpdated();

	// ���������� ������� ������ ��������� ��� ���������� ������ ���������
	int getMaxScoresForComplexity( GameComplexityType complexity );
};

#endif