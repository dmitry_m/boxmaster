#include "HelloWorldScene.h"

USING_NS_CC;

void HelloWorld::resetSprite( CCNode* spr ) {
	if( !spr ) {
		return;
	}
	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

	float targetAlt = spr->getPosition().y < screenSize.height/2 ? 0.8f * screenSize.height : 0.2f * screenSize.height ;

	spr->runAction( CCSequence::actions ( 
		CCMoveTo::actionWithDuration( 1.5f, ccp( spr->getPosition().x, targetAlt ) ),
		CCCallFuncN::actionWithTarget( this, callfuncN_selector( HelloWorld::resetSprite ) ),
		NULL ) );
}




CCScene* HelloWorld::scene()
{
	// 'scene' is an autorelease object
	CCScene *scene = CCScene::node();
	
	// 'layer' is an autorelease object
	HelloWorld *layer = HelloWorld::node();

	CCSize screenSize = CCDirector::sharedDirector()->getWinSize();

//	CCTexture2D::setDefaultAlphaPixelFormat( kCCTexture2DPixelFormat_RGB5A1 );
	for( int i = 0; i < 400; i++ ) {
		CCSprite* sprite;
		switch( i % 5 ) {
		case 0:
			sprite = CCSprite::spriteWithFile("loader.png");
			break;
		case 1:
			sprite = CCSprite::spriteWithFile("square.png");
			break;
		case 2:
			sprite = CCSprite::spriteWithFile("ball.png");
			break;
		case 3:
			sprite = CCSprite::spriteWithFile("ball.png");
			break;
		case 4:
			sprite = CCSprite::spriteWithFile("ball_hq.png");
			break;
		}



		sprite->setPosition( ccp( 50 + i, 0.0f ) );
		layer->addChild( sprite, 100 );

		sprite->runAction( CCSequence::actions ( 
			CCMoveTo::actionWithDuration( 1.5f, ccp( sprite->getPosition().x, 0.8*screenSize.height ) ),
			CCCallFuncN::actionWithTarget( NULL, callfuncN_selector( HelloWorld::resetSprite ) ),
			NULL ) );
	}
//	CCTexture2D::setDefaultAlphaPixelFormat( kCCTexture2DPixelFormat_Default );


	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !CCLayer::init() )
	{
		return false;
	}

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	CCMenuItemImage *pCloseItem = CCMenuItemImage::itemFromNormalImage(
										"CloseNormal.png",
										"CloseSelected.png",
										this,
										menu_selector(HelloWorld::menuCloseCallback) );
	pCloseItem->setPosition( ccp(CCDirector::sharedDirector()->getWinSize().width - 20, 20) );

	// create menu, it's an autorelease object
	CCMenu* pMenu = CCMenu::menuWithItems(pCloseItem, NULL);
	pMenu->setPosition( CCPointZero );
	this->addChild(pMenu, 1);

	/////////////////////////////
	// 3. add your codes below...

	// add a label shows "Hello World"
	// create and initialize a label
    CCLabelTTF* pLabel = CCLabelTTF::labelWithString("Hello World", "Arial", 24);
	// ask director the window size
	CCSize size = CCDirector::sharedDirector()->getWinSize();

	// position the label on the center of the screen
	pLabel->setPosition( ccp(size.width / 2, size.height - 50) );

	// add the label as a child to this layer
	this->addChild(pLabel, 1);

	// add "HelloWorld" splash screen"
	//CCSprite* pSprite = CCSprite::spriteWithFile("HelloWorld.png");

	// position the sprite on the center of the screen
	//pSprite->setPosition( ccp(size.width/2, size.height/2) );

	// add the sprite as a child to this layer
	//this->addChild(pSprite, 0);
	
	return true;
}

void HelloWorld::menuCloseCallback(CCObject* pSender)
{
	CCDirector::sharedDirector()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}
