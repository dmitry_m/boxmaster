package com.google.ads.example;

import android.app.Activity;
import android.os.Bundle;
import com.google.ads.*;

public class BannerXML extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.main);

      // Look up the AdView as a resource and load a request.
      AdView adView = (AdView)this.findViewById(R.id.adView);
      adView.loadAd(new AdRequest());
    }
}