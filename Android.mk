LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared

LOCAL_MODULE_FILENAME := libgame

LOCAL_SRC_FILES := helloworld/main.cpp \
../../Classes/GameObjects/GameObject.cpp \
../../Classes/GameObjects/GameCharacter.cpp \
../../Classes/GameObjects/Cloud.cpp \
../../Classes/GameObjects/Box2D/Sensor.cpp \
../../Classes/GameObjects/Box2D/Loader.cpp \
../../Classes/GameObjects/Box2D/Trophy.cpp \
../../Classes/GameObjects/Box2D/Box2DSprite.cpp \
../../Classes/GameObjects/Box2D/Box.cpp \
../../Classes/GameObjects/Box2D/GamePhysicsContactListener.cpp \
../../Classes/Scenes/Credits/CreditsScene.cpp \
../../Classes/Scenes/Credits/CreditsLayer.cpp \
../../Classes/Scenes/Greeting/GreetingScene.cpp \
../../Classes/Scenes/Greeting/GreetingLayer.cpp \
../../Classes/Scenes/Exit/ExitScene.cpp \
../../Classes/Scenes/Exit/ExitLayer.cpp \
../../Classes/Scenes/LevelComplete/LevelCompleteScene.cpp \
../../Classes/Scenes/LevelComplete/LevelCompleteLayer.cpp \
../../Classes/Scenes/MainMenu/MainMenuScene.cpp \
../../Classes/Scenes/MainMenu/MainMenuLayer.cpp \
../../Classes/Scenes/Options/OptionsScene.cpp \
../../Classes/Scenes/Options/OptionsLayer.cpp \
../../Classes/Scenes/Scene1/Scene1.cpp \
../../Classes/Scenes/Scene1/Layers/Scene1UILayer.cpp \
../../Classes/Scenes/Scene1/Layers/Scene1ActionLayer.cpp \
../../Classes/Service/ServiceInfo.cpp \
../../Classes/Service/Box2DHelpers.cpp \
../../Classes/Service/GLES-Render.cpp \
../../Classes/Service/SimpleQueryCallback.cpp \
../../Classes/Singletons/AppDelegate.cpp \
../../Classes/Singletons/GameManager.cpp \
../../Classes/Singletons/ConstantsClass.cpp \
../../Classes/SneakyJoystick/SneakyJoystickSkinnedBase.cpp \
../../Classes/SneakyJoystick/SneakyJoystick.cpp \
../../Classes/SneakyJoystick/SneakyButtonSkinnedBase.cpp \
../../Classes/SneakyJoystick/SneakyButton.cpp \
../../Classes/RunJava.cpp
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
                   $(LOCAL_PATH)/../../Classes/Singletons             

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static box2d_static            
include $(BUILD_SHARED_LIBRARY)

$(call import-module,CocosDenshion/android)
$(call import-module,cocos2dx) 
$(call import-module,Box2D)